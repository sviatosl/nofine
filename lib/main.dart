import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:no_fine/bloc/auth/main_screen_bloc/main_screen_bloc.dart';
import 'package:no_fine/bloc/auth/sign_in_bloc/sign_in_bloc.dart';
import 'package:no_fine/bloc/fines_bloc/fines_bloc.dart';
import 'package:no_fine/bloc/news_bloc/news_bloc.dart';
import 'package:no_fine/bloc/notificatios_bloc/notifications_bloc.dart';
import 'package:no_fine/bloc/packages_bloc/packages_bloc.dart';
import 'package:no_fine/bloc/settings_bloc/settings_bloc.dart';
import 'package:no_fine/data/repository/user_repo.dart';
import 'package:no_fine/ui/base_screen.dart';
import 'package:no_fine/ui/main_screen/main_screen.dart';

import 'package:no_fine/ui/onboarding/onboarding_screen.dart';
import 'package:no_fine/ui/pages/%20coin_index_screen.dart';
import 'package:no_fine/ui/pages/%20withdrawing_coins_screen.dart';
import 'package:no_fine/ui/pages/add_fine_screen.dart';
import 'package:no_fine/ui/pages/adding_bonuses_screen.dart';
import 'package:no_fine/ui/pages/agreement_creen.dart';
import 'package:no_fine/ui/pages/info_page.dart';
import 'package:no_fine/ui/pages/my_bonuses_screen.dart';
import 'package:no_fine/ui/pages/my_fines_screen.dart';
import 'package:no_fine/ui/pages/my_pakeges_scren.dart';
import 'package:no_fine/ui/pages/my_trans.dart';
import 'package:no_fine/ui/pages/news_detail_screen.dart';
import 'package:no_fine/ui/pages/notification_screen.dart';
import 'package:no_fine/ui/pages/oferta_screen.dart';
import 'package:no_fine/ui/pages/payment_screen.dart';
import 'package:no_fine/ui/pages/policy_screen.dart';
import 'package:no_fine/ui/pages/profile_page.dart';

import 'package:no_fine/ui/pages/transfer_of_coins_screen_to_person.dart';
import 'package:no_fine/ui/pages/write_us.dart';
import 'package:no_fine/ui/sign_in/sign_in.dart';
import 'package:no_fine/ui/sign_up/sign_up.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'bloc/auth/sign_up_bloc/sign_up_bloc.dart';
import 'bloc/auth/translations_bloc/translations_bloc.dart';
import 'data/api_provider.dart';
import 'data/repository/auth_repo.dart';
import 'data/repository/transfer_repo.dart';
import 'bloc/bonuses_bloc/bonuses_bloc.dart';

String token;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  token = prefs.getString('token');
  print(token);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return MaterialApp(
      initialRoute: token == null ? OnboardingScreen.route : BaseScreen.route,
      onGenerateRoute: _onGenerateRoute,
    );
  }

  Route<dynamic> _onGenerateRoute(RouteSettings settings) {
    Widget page;
    switch (settings.name) {
      case BaseScreen.route:
        page = BaseScreen();
        break;

      case OnboardingScreen.route:
        page = BlocProvider(
            create: (_) => SignInBloc(AuthRepositoryImpl(ApiProvider(Dio()))),
            child: OnboardingScreen());
        break;

      case SignUpScreen.route:
        page = BlocProvider(
            create: (_) => SignUpBloc(AuthRepositoryImpl(ApiProvider(Dio()))),
            child: SignUpScreen());
        break;

      case SignInScreen.route:
        page = BlocProvider(
            create: (_) => SignInBloc(AuthRepositoryImpl(ApiProvider(Dio()))),
            child: SignInScreen());
        break;

      case MainScreen.route:
        page = BlocProvider(
            create: (_) => MainScreenBloc(
                  AuthRepositoryImpl(ApiProvider(Dio())),
                  UserRepositoryImpl(ApiProvider(Dio())),
                ),
            child: MainScreen());
        break;

      case MyBonusesScreen.route:
        page = BlocProvider(
            create: (_) => BonusesBloc(
                  TransferRepositoryImpl(ApiProvider(Dio())),
                ),
            child: MyBonusesScreen());
        break;

      case CoinIndexScreen.route:
        page = BlocProvider(
          create: (_) => SettingsBloc(
            TransferRepositoryImpl(ApiProvider(Dio())),
          ),
          child: CoinIndexScreen(),
        );
        break;

      case MyPakagesScreen.route:
        page = BlocProvider(
            create: (_) => PackagesBloc(
                  TransferRepositoryImpl(ApiProvider(Dio())),
                ),
            child: MyPakagesScreen());
        break;

      case MyTransScreen.route:
        page = BlocProvider(
            create: (_) => TransferBloc(
                  TransferRepositoryImpl(ApiProvider(Dio())),
                  UserRepositoryImpl(ApiProvider(Dio())),
                ),
            child: MyTransScreen());
        break;

      case WithdrawingCoinsScreen.route:
        page = BlocProvider(
            create: (_) => TransferBloc(
                  TransferRepositoryImpl(ApiProvider(Dio())),
                  UserRepositoryImpl(ApiProvider(Dio())),
                ),
            child: WithdrawingCoinsScreen());
        break;

      case TransferCoinsToPersonScreen.route:
        page = BlocProvider(
            create: (_) => TransferBloc(
                  TransferRepositoryImpl(ApiProvider(Dio())),
                  UserRepositoryImpl(ApiProvider(Dio())),
                ),
            child: TransferCoinsToPersonScreen());
        break;
      case AddBonusesScreen.route:
        page = BlocProvider(
            create: (_) =>
                BonusesBloc(TransferRepositoryImpl(ApiProvider(Dio()))),
            child: AddBonusesScreen());
        break;

      case MyFinesScreen.route:
        page = BlocProvider(
          create: (_) => FinesBloc(TransferRepositoryImpl(ApiProvider(Dio()))),
          child: MyFinesScreen(),
        );
        break;

      case AddFineScreen.route:
        page = BlocProvider(
          create: (_) => FinesBloc(TransferRepositoryImpl(ApiProvider(Dio()))),
          child: AddFineScreen(),
        );
        break;

      case ProfileScreen.route:
        page = BlocProvider(
            create: (_) => MainScreenBloc(
                  AuthRepositoryImpl(ApiProvider(Dio())),
                  UserRepositoryImpl(ApiProvider(Dio())),
                ),
            child: ProfileScreen(isVisibleAppBar: true,));
        break;

      case InfoScreen.route:
        page = InfoScreen();
        break;
      case WriteUsScreen.route:
        page = BlocProvider(
            create: (_) => SettingsBloc(
              TransferRepositoryImpl(ApiProvider(Dio())),
            ),
            child: WriteUsScreen());
        break;
      case NotificationScreen.route:
        page = BlocProvider(
            create: (_) => NotificationsBloc(
                  TransferRepositoryImpl(ApiProvider(Dio())),
                ),
            child: NotificationScreen());
        break;

      case NewsDetailScreen.route:
        page = BlocProvider(
            create: (_) => NewsBloc(
              TransferRepositoryImpl(ApiProvider(Dio())),
            ),
            child: NewsDetailScreen());
        break;


      case PolicyScreen.route:
        page = PolicyScreen();
        break;
      case OfertaScreen.route:
        page = OfertaScreen();
        break;
      case AgreementScreen.route:
        page = AgreementScreen();
        break;
      case PaymantScreen.route:
        page = PaymantScreen();
        break;

    }

    return MaterialPageRoute(
      settings: settings,
      builder: (context) => page,
    );
  }
}
