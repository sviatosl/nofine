import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:no_fine/bloc/packages_bloc/packages_event.dart';
import 'package:no_fine/bloc/packages_bloc/packages_state.dart';
import 'package:no_fine/data/api_models/packages_response.dart';
import 'package:no_fine/data/repository/transfer_repo.dart';

class PackagesBloc extends Bloc<PackageEvent, PackagesState> {
  static const TAG = "packageBloc";
  final TransferRepository _repository;

  PackagesBloc(
    this._repository,
  ) : super(PackagesInitState()) {
    //add(UpdateTransScreenEvent());
  }

  @override
  Stream<PackagesState> mapEventToState(PackageEvent event) async* {

    if (event is UpdatePackagesEvent) {
      yield* _mapUpdateBonuseToState(event);
    }
  }



  Stream<PackagesState> _mapUpdateBonuseToState(
      UpdatePackagesEvent event) async* {
    try {
      List<PackagesResponse> packagesResponse = await _repository.getAllPackages();
      yield PackagesLoadedState(packagesResponse);


    } catch (error, stacktrace) {
       yield* _handleException(error);
      print("[$TAG]: $error");
      print(error);
      print(stacktrace);

    }
  }

  Stream<PackagesState> _handleException(error) async* {
    if (error is PlatformException) {
      yield PackagesFailureState();
    } else {
      yield PackagesFailureState();
    }
  }

}
