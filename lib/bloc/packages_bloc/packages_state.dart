

import 'package:no_fine/data/api_models/packages_response.dart';

abstract class PackagesState {}

class PackagesInitState extends PackagesState {}

class PackagesLoadedState extends PackagesState {
  List<PackagesResponse> packagesResponse;

  PackagesLoadedState(this.packagesResponse);
}

class PackagesFailureState extends PackagesState {}
