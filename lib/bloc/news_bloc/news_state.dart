
import 'package:no_fine/data/api_models/news_response.dart';


abstract class NewsState {}

class NewsInitState extends NewsState {}

class NewsLoadedState extends NewsState {
  List<NewsResponse> newsResponse;

  NewsLoadedState(this.newsResponse);

}

class NewsFailureState extends NewsState {}
