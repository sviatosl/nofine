import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:no_fine/bloc/news_bloc/news_state.dart';
import 'package:no_fine/bloc/notificatios_bloc/notifications_event.dart';
import 'package:no_fine/bloc/notificatios_bloc/notifications_state.dart';
import 'package:no_fine/data/api_models/news_response.dart';
import 'package:no_fine/data/api_models/notifications_response.dart';

import 'package:no_fine/data/repository/transfer_repo.dart';

import 'news_event.dart';

class NewsBloc extends Bloc<NewsEvent, NewsState> {
  final TransferRepository _repository;

  NewsBloc(
    this._repository,
  ) : super(NewsInitState()) {
    //add(UpdateTransScreenEvent());
  }

  @override
  Stream<NewsState> mapEventToState(NewsEvent event) async* {
    if (event is GetAllNewsEvent) {
      yield* _mapGetAllNewsToState(event);
    }

  }

  Stream<NewsState> _mapGetAllNewsToState(
      GetAllNewsEvent event) async* {
    try {
      List<NewsResponse> list =
          await _repository.getAllNews();
      yield NewsLoadedState(list);
    } catch (error, stacktrace) {
      yield* _handleException(error);

      print(error);
      print(stacktrace);
    }
  }

  Stream<NewsState> _handleException(error) async* {
      yield NewsFailureState();
  }
}
