import 'package:flutter/material.dart';

abstract class SettingsEvent {}

class UpdateSettingsEvent extends SettingsEvent {}

class AddFitbackEvent extends SettingsEvent {

  final String description;
  AddFitbackEvent({
    @required this.description,

  });
}
