
import 'package:no_fine/data/api_models/settings_respomse.dart';


abstract class SettingsState {}

class SettingsInitState extends SettingsState {}

class SettingsLoadedState extends SettingsState {
  SettingsResponse2 settingsResponse;

  SettingsLoadedState(this.settingsResponse);
}

class SettingsFailureState extends SettingsState {}

class FitbackSuccessState extends SettingsState {

  String fitbackResponse;
  FitbackSuccessState(this.fitbackResponse);

}
