import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:no_fine/bloc/packages_bloc/packages_event.dart';
import 'package:no_fine/bloc/packages_bloc/packages_state.dart';
import 'package:no_fine/bloc/settings_bloc/settings_event.dart';
import 'package:no_fine/bloc/settings_bloc/settings_state.dart';
import 'package:no_fine/data/api_models/packages_response.dart';
import 'package:no_fine/data/api_models/server_error.dart';
import 'package:no_fine/data/api_models/settings_respomse.dart';
import 'package:no_fine/data/repository/settings_repo.dart';
import 'package:no_fine/data/repository/transfer_repo.dart';

class SettingsBloc extends Bloc<SettingsEvent, SettingsState> {
  final TransferRepository _repository;

  SettingsBloc(
    this._repository,
  ) : super(SettingsInitState()) {
    //add(UpdateTransScreenEvent());
  }

  @override
  Stream<SettingsState> mapEventToState(SettingsEvent event) async* {
    if (event is UpdateSettingsEvent) {
      yield* _mapUpdateBonuseToState(event);
    }
    if (event is AddFitbackEvent) {
      yield* _mapAddFitbackState(event);
    }
  }

  Stream<SettingsState> _mapUpdateBonuseToState(
      UpdateSettingsEvent event) async* {
    try {
      SettingsResponse2 settingsResponse = await _repository.getSettings();
      print('end get settings ; ');
      yield SettingsLoadedState(settingsResponse);
    } catch (error, stacktrace) {
      yield* _handleException(error);

      print(error);
      print(stacktrace);
    }
  }


  Stream<SettingsState> _mapAddFitbackState(
      AddFitbackEvent event) async* {
    try {
      ServerMassage res = await _repository.addFitback(event.description);
     // print('end get settings ; ');
      yield FitbackSuccessState(res.message);
    } catch (error, stacktrace) {
      yield* _handleException(error);

      print(error);
      print(stacktrace);
    }
  }


  Stream<SettingsState> _handleException(error) async* {
    yield SettingsFailureState();
  }
}
