import 'package:no_fine/data/api_models/fines_response.dart';

abstract class FinesState {}

class FinesInitState extends FinesState {}

class FinesLoadedState extends FinesState {
  List<FineResponse> fineResponse;

  FinesLoadedState(this.fineResponse);
}

class FinesFailureState extends FinesState {
  String fineResponse;

  FinesFailureState(this.fineResponse);
}

class FinesAddSuccessState extends FinesState {

  String fineResponse;

  FinesAddSuccessState(this.fineResponse);


}
