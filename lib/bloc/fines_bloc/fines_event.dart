import 'package:flutter/material.dart';

abstract class FinesEvent {}

class AddFineEvent extends FinesEvent {
  final String uin;
  final int type;

  AddFineEvent({
    @required this.uin,
    @required this.type,
  });
}

class UpdateFinesEvent extends FinesEvent {}
