import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:no_fine/bloc/fines_bloc/fines_event.dart';
import 'package:no_fine/bloc/fines_bloc/fines_state.dart';
import 'package:no_fine/data/api_models/fines_response.dart';
import 'package:no_fine/data/api_models/server_error.dart';
import 'package:no_fine/data/repository/transfer_repo.dart';

class FinesBloc extends Bloc<FinesEvent, FinesState> {
  static const TAG = "finesBloc";
  final TransferRepository _repository;

  FinesBloc(
    this._repository,
  ) : super(FinesInitState()) {
    //add(UpdateTransScreenEvent());
  }

  @override
  Stream<FinesState> mapEventToState(FinesEvent event) async* {
    if (event is AddFineEvent) {
      yield* _mapAddFineToState(event);
    }
    if (event is UpdateFinesEvent) {
      yield* _mapUpdateBonuseToState(event);
    }
  }

  Stream<FinesState> _mapAddFineToState(AddFineEvent event) async* {
    try {
    ServerMassage res =   await _repository.addFine(event.uin, event.type);
      // await add(UpdateTransScreenEvent());
      yield FinesAddSuccessState(res.message);
    } catch (error, stacktrace) {

      yield* _handleException(error);
    }
  }

  Stream<FinesState> _mapUpdateBonuseToState(UpdateFinesEvent event) async* {
    try {
      List<FineResponse> fineResponse = await _repository.getAllFines();
      yield FinesLoadedState(fineResponse);
    } catch (error, stacktrace) {
      yield* _handleException(error);
      print("[$TAG]: $error");
      yield* _handleException(error);
    }
  }

  Stream<FinesState> _handleException(error) async* {
    yield FinesFailureState(error.toString());
  }
}
