import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:no_fine/bloc/notificatios_bloc/notifications_event.dart';
import 'package:no_fine/bloc/notificatios_bloc/notifications_state.dart';
import 'package:no_fine/bloc/packages_bloc/packages_event.dart';
import 'package:no_fine/bloc/packages_bloc/packages_state.dart';
import 'package:no_fine/data/api_models/notifications_response.dart';
import 'package:no_fine/data/api_models/packages_response.dart';
import 'package:no_fine/data/repository/transfer_repo.dart';

class NotificationsBloc extends Bloc<NotificationsEvent, NotificationsState> {
  final TransferRepository _repository;

  NotificationsBloc(
    this._repository,
  ) : super(NotificationsInitState()) {
    //add(UpdateTransScreenEvent());
  }

  @override
  Stream<NotificationsState> mapEventToState(NotificationsEvent event) async* {
    if (event is UpdateNotificationsEvent) {
      yield* _mapUpdateBonuseToState(event);
    }
    if (event is ReadNotificationsEvent) {
      yield* _mapReadNotificationToState(event);
    }
    if (event is ReadAllNotificationsEvent) {
      yield* _mapAllReadNotificationToState(event);
    }
  }

  Stream<NotificationsState> _mapUpdateBonuseToState(
      UpdateNotificationsEvent event) async* {
    try {
      List<NotificationsResponse> notificationsResponse =
          await _repository.getNotifications();
      yield NotificationsLoadedState(notificationsResponse);
    } catch (error, stacktrace) {
      yield* _handleException(error);

      print(error);
      print(stacktrace);
    }
  }

  Stream<NotificationsState> _mapReadNotificationToState(
      ReadNotificationsEvent event) async* {
    try {
      await _repository.readOneNotification(event.id);
      //  List<NotificationsResponse> notificationsResponse = await _repository.getNotifications();
      //  yield NotificationsLoadedState(notificationsResponse);

    } catch (error, stacktrace) {
      yield* _handleException(error);

      print(error);
      print(stacktrace);
    }
  }


  Stream<NotificationsState> _mapAllReadNotificationToState(
      ReadAllNotificationsEvent event) async* {
    try {
      await _repository.readAllNotification();
      //  List<NotificationsResponse> notificationsResponse = await _repository.getNotifications();
      //  yield NotificationsLoadedState(notificationsResponse);

    } catch (error, stacktrace) {
      yield* _handleException(error);

      print(error);
      print(stacktrace);
    }
  }

  Stream<NotificationsState> _handleException(error) async* {
    if (error is PlatformException) {
      yield NotificationsFailureState();
    } else {
      yield NotificationsFailureState();
    }
  }
}
