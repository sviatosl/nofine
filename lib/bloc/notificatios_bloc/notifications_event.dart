import 'package:flutter/material.dart';

abstract class NotificationsEvent {}

class UpdateNotificationsEvent extends NotificationsEvent {}

class ReadNotificationsEvent extends NotificationsEvent {
  final String id;

  ReadNotificationsEvent({
    @required this.id,
  });

}

class ReadAllNotificationsEvent extends NotificationsEvent {}