

import 'package:no_fine/data/api_models/notifications_response.dart';
import 'package:no_fine/data/api_models/packages_response.dart';

abstract class NotificationsState {}

class NotificationsInitState extends NotificationsState {}

class NotificationsLoadedState extends NotificationsState {
  List<NotificationsResponse> notificationsResponse;

  NotificationsLoadedState(this.notificationsResponse);
}

class NotificationsFailureState extends NotificationsState {}
