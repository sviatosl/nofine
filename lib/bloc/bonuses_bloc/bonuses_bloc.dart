import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:no_fine/bloc/bonuses_bloc/bonuses_event.dart';
import 'package:no_fine/data/api_models/bonuse_response.dart';
import 'package:no_fine/data/api_models/server_error.dart';
import 'package:no_fine/data/repository/transfer_repo.dart';
import 'package:no_fine/bloc/bonuses_bloc/bonuses_state.dart';

class BonusesBloc extends Bloc<BonusesEvent, BonusesState> {
  static const TAG = "SignInBloc";
  final TransferRepository _repository;

  BonusesBloc(
    this._repository,
  ) : super(BonusesInitState()) {
    //add(UpdateTransScreenEvent());
  }

  @override
  Stream<BonusesState> mapEventToState(BonusesEvent event) async* {
    if (event is AddBonuseEvent) {
      yield* _mapAddBonuseToState(event);
    }
    if (event is UpdateBonusesEvent) {
      yield* _mapUpdateBonuseToState(event);
    }
  }

  Stream<BonusesState> _mapAddBonuseToState(AddBonuseEvent event) async* {
    try {
      ServerMassage res = await _repository.addBonuse(
          event.autoVin, event.autoNumber, event.autoLicense);
      // await add(UpdateTransScreenEvent());
      yield BonusesAddSuccess(res.message);
    } catch (error, stacktrace) {
      print("[$TAG]: $error");
      print(error);
      print(stacktrace);
      yield* _handleException(error);
    }
  }

  Stream<BonusesState> _mapUpdateBonuseToState(
      UpdateBonusesEvent event) async* {
    try {
      List<BonuseResponce> bonuseResponce = await _repository.getAllBonuses();

      yield BonusesLoadedState(bonuseResponce);
    } catch (error, stacktrace) {
      // var errorData = json.decode(error.response.toString());
      //   yield* _errorToState();
      // print("[$TAG]: $error");
      /* yield* _handleException(error);*/
      print("[$TAG]: $error");
      print(error);
      print(stacktrace);
      yield* _handleException(error);
    }
  }

  Stream<BonusesState> _handleException(error) async* {
    yield BonusesFailureState(error.toString());
  }
}
