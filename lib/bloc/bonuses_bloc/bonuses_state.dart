import 'package:no_fine/data/api_models/bonuse_response.dart';

abstract class BonusesState {}

class BonusesInitState extends BonusesState {}

class BonusesLoadedState extends BonusesState {
  List<BonuseResponce> bonuseResponce;

  BonusesLoadedState(this.bonuseResponce);
}

class BonusesFailureState extends BonusesState {
  String bonusesResponse;

  BonusesFailureState(this.bonusesResponse);

}

class BonusesAddSuccess extends BonusesState {

  String bonusesResponse;

  BonusesAddSuccess(this.bonusesResponse);


}
