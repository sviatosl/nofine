

import 'package:flutter/material.dart';

abstract class BonusesEvent {}

class AddBonuseEvent extends BonusesEvent {
  final String autoVin;
  final String autoNumber;
  final String autoLicense;

  AddBonuseEvent({
    @required this.autoVin,
    @required this.autoNumber,
    @required this.autoLicense,
  });
}

class UpdateBonusesEvent extends BonusesEvent {}
