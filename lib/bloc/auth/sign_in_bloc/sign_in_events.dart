import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

abstract class SignInEvent {}

class SignInLoginEvent extends SignInEvent {
  final String phone;
  final String password;

  SignInLoginEvent({
    @required this.phone,
    @required this.password,
  });
}

class SignInLoginGoogleEvent extends SignInEvent {}

class SignInLoginFacebookEvent extends SignInEvent {}