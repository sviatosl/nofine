import 'package:flutter/material.dart';

abstract class SignInState {}

class SignInInitState extends SignInState {}

class SignInLoadingState extends SignInState {}

class SignInFailureState extends SignInState {
  final String errorMessage;

  SignInFailureState({@required this.errorMessage});
}

class SuccessSignInState extends SignInState {}

class SignInLoginGoogleState extends SignInState {}

class SignInLoginFacebookState extends SignInState {}
