import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:flutter/services.dart';

import 'package:no_fine/bloc/auth/sign_in_bloc/sign_in_events.dart';
import 'package:no_fine/bloc/auth/sign_in_bloc/sign_in_state.dart';

import 'package:no_fine/data/repository/auth_repo.dart';

class SignInBloc extends Bloc<SignInEvent, SignInState> {

  static const TAG = "SignInBloc";
  final AuthRepository _repository;
  SignInBloc(this._repository) : super(SignInInitState());



  @override
  Stream<SignInState> mapEventToState(SignInEvent event) async* {
    if (event is SignInLoginEvent) {
      yield* _mapSignInLoginToState(event);
    } else if (event is SignInLoginGoogleEvent) {
      yield* _mapSignInLoginGoogleToState( event);
    } else if (event is SignInLoginFacebookEvent) {
      yield* _mapSignInLoginFacebookToState(event);
    }
  }


  Stream<SignInState> _mapSignInLoginToState(SignInLoginEvent event) async* {
    try {
       await _repository.login(event.phone, event.password);
      yield SuccessSignInState();
    } catch (error,stacktrace) {
      yield* _handleException(error);
    }

  }



  Stream<SignInState> _mapSignInLoginGoogleToState(SignInLoginGoogleEvent event) async* {
    yield SignInLoadingState();
    try {
      //await _repository.login(event.phone, event.password);

      await _repository.googleLogIn();

      yield SuccessSignInState();
    } catch (error,stacktrace) {

      print("[$TAG]: $error");
      print(error);
      print(stacktrace);
    //  yield* _handleException(error);
    }
  }

  Stream<SignInState> _mapSignInLoginFacebookToState(SignInLoginFacebookEvent event) async* {
    yield SignInLoadingState();

    try {
     // await _repository.login(event.phone, event.password);
      await _repository.facebookLogIn();

      yield SuccessSignInState();
    } catch (error,stacktrace) {

      print("[$TAG]: $error");
      print(error);
      print(stacktrace);
    //  yield* _handleException(error);
    }

  }

  Stream<SignInState> _errorToState() async* {
    yield SignInFailureState();
    //yield InitialAuthState();
  }


  Stream<SignInState>  _handleException(error) async* {
    if (error is PlatformException) {
      yield SignInFailureState(errorMessage: error.message);
    } if (error is DioError) {
      yield SignInFailureState(errorMessage: error.message);
    }else {
      yield SignInFailureState(errorMessage: error.toString());
    }
  }


}
