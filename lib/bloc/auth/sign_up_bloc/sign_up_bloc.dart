import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:no_fine/bloc/auth/sign_up_bloc/sign_up_events.dart';
import 'package:no_fine/bloc/auth/sign_up_bloc/sign_up_state.dart';
import 'package:no_fine/data/repository/auth_repo.dart';

class SignUpBloc extends Bloc<SignUpEvent, SignUpState> {
  static const TAG = "SignUpBloc";
  final AuthRepository _repository;

  SignUpBloc(this._repository) : super(SignUpInitState());

  @override
  Stream<SignUpState> mapEventToState(SignUpEvent event) async* {
    if (event is SignUpRegisterEvent) {
      yield* _mapSingUpToState(event);
    }
  }

  Stream<SignUpState> _mapSingUpToState(SignUpRegisterEvent event) async* {
    yield SignUpLoadingState();

    try {
      await _repository.register(event.name, event.surname, event.phone,
          event.email, event.password, event.confirm_password);
      yield SuccessSignUpState();
    } catch (error, stacktrace) {
      yield* _handleException(error);
    }
  }

  Stream<SignUpState> _handleException(error) async* {
    yield SignUpFailureState(errorMessage: error.message.toString());
  }
}
