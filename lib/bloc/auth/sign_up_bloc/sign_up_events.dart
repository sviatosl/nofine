import 'package:flutter/material.dart';

abstract class SignUpEvent {}

class SignUpRegisterEvent extends SignUpEvent {
  final String name;
  final String surname;
  final String phone;
  final String email;
  final String password;
  final String confirm_password;

  SignUpRegisterEvent({
    @required this.name,
    @required this.surname,
    @required this.phone,
    @required this.email,
    @required this.password,
    @required this.confirm_password,
  });
}

