import 'package:flutter/material.dart';

abstract class SignUpState {}

class SignUpInitState extends SignUpState {}

class SignUpFailureState extends SignUpState {
  final String errorMessage;

  SignUpFailureState({@required this.errorMessage});
}

class SignUpLoadingState extends SignUpState {}

class SignUpCompleteState extends SignUpState {}

class SuccessSignUpState extends SignUpState {}
