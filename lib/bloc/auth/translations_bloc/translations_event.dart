import 'package:flutter/material.dart';

abstract class TransferEvent {}

class TransferCoinsTOUserEvent extends TransferEvent {
  final String recipient_bill;
  final int coin;

  TransferCoinsTOUserEvent({
    @required this.recipient_bill,
    @required this.coin,
  });
}

class TransferCoinsToStockExchangeEvent extends TransferEvent {

  final int coin;

  TransferCoinsToStockExchangeEvent({

    @required this.coin,
  });
}

class UpdateTransScreenEvent extends TransferEvent {}


class UpdateAllTransfersEvent extends TransferEvent {}

