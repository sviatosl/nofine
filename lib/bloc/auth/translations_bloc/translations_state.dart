import 'package:no_fine/data/api_models/transfers_response.dart';
import 'package:no_fine/data/api_models/login_user_response.dart';
import 'package:no_fine/data/api_models/user_response.dart';

abstract class TransferState {}

class TransferInitState extends TransferState {}



class TransferToUserSucessfful extends TransferState {
  String transferResponse;

  TransferToUserSucessfful(this.transferResponse);
}

class TransferToStockExchangeSucessfful extends TransferState {
  String transferResponse;

  TransferToStockExchangeSucessfful(this.transferResponse);
}

class TransferFailureState extends TransferState {

  String transferResponse;

  TransferFailureState(this.transferResponse);
}


class TransferLoadedState extends TransferState {
  UserResponse userResponse;
  TransferLoadedState(this.userResponse);
}


class TransferAllLoadedState extends TransferState {
  TransferResponse transferResponse;
  TransferAllLoadedState(this.transferResponse);
}


class SuccessfullTransferToUserInitState extends TransferState {}

class BlockTransferState extends TransferState {}