import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:no_fine/bloc/auth/main_screen_bloc/main_screen_event.dart';
import 'package:no_fine/bloc/auth/main_screen_bloc/main_screen_state.dart';
import 'package:no_fine/bloc/auth/translations_bloc/translations_event.dart';
import 'package:no_fine/bloc/auth/translations_bloc/translations_state.dart';
import 'package:no_fine/data/api_models/server_error.dart';
import 'package:no_fine/data/api_models/transfers_response.dart';
import 'package:no_fine/data/api_models/login_user_response.dart';
import 'package:no_fine/data/api_models/user_response.dart';
import 'package:no_fine/data/repository/auth_repo.dart';
import 'package:no_fine/data/repository/transfer_repo.dart';
import 'package:no_fine/data/repository/user_repo.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TransferBloc extends Bloc<TransferEvent, TransferState> {
  static const TAG = "SignInBloc";
  final TransferRepository _transferRepository;
  final UserRepository _userRepository;

  TransferBloc(this._transferRepository, this._userRepository)
      : super(TransferInitState()) {
    //add(UpdateTransScreenEvent());
  }

  @override
  Stream<TransferState> mapEventToState(TransferEvent event) async* {
    if (event is TransferCoinsTOUserEvent) {
      yield* _mapTransferCoinsToUserToState(event);
    }
    if (event is TransferCoinsToStockExchangeEvent) {
      yield* _mapTransferCoinsToStockExchangeToState(event);
    }
    if (event is UpdateTransScreenEvent) {
      yield* _mapUpdateTransScreenToState(event);
    }
    if (event is UpdateAllTransfersEvent) {
      yield* _mapUpdateAllTransfersToState(event);
    }
  }

  Stream<TransferState> _mapTransferCoinsToUserToState(
      TransferCoinsTOUserEvent event) async* {
    try {
      ServerMassage res = await _transferRepository.transCoinToUser(
          event.recipient_bill, event.coin);
      yield TransferToUserSucessfful(res.message);
      // await add(UpdateTransScreenEvent());
    } catch (error, stacktrace) {
      print("[$TAG]: $error");
      print(error);
      print(stacktrace);
      yield* _handleException(error);
    }
  }

  Stream<TransferState> _mapTransferCoinsToStockExchangeToState(
      TransferCoinsToStockExchangeEvent event) async* {
    try {
      ServerMassage res =
          await _transferRepository.transCoinToStockExchange(event.coin);
      yield TransferToStockExchangeSucessfful(res.message);
    } catch (error, stacktrace) {
      yield* _handleException(error);
    }
  }

  _getPackageId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //print('packageID' + prefs.getInt('packageId').toString());
    //  packageID = prefs.getInt('packageId');
    //print('000000' + packageID.toString());
    return prefs.getInt('packageId');
  }

  Stream<TransferState> _mapUpdateTransScreenToState(
      UpdateTransScreenEvent event) async* {
    try {

        UserResponse userResponse = await _userRepository.getUser();
        yield TransferLoadedState(userResponse);

    } catch (error, stacktrace) {
      print("[$TAG]: $error");
      print(error);
      print(stacktrace);
      yield* _handleException(error);
    }
  }

  Stream<TransferState> _mapUpdateAllTransfersToState(
      UpdateAllTransfersEvent event) async* {
    try {
      TransferResponse transferResponse =
          await _transferRepository.getAllTransfers();
      yield TransferAllLoadedState(transferResponse);
    } catch (error, stacktrace) {
      print("[$TAG]: $error");
      print(error);
      print(stacktrace);
      yield* _handleException(error);
    }
  }

  Stream<TransferState> _handleException(error) async* {
    yield TransferFailureState(error.toString());
  }
}
