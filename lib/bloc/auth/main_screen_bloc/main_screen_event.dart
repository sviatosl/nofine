
import 'dart:io';

import 'package:flutter/material.dart';

abstract class MainScreenEvent {}

class LogOutEvent extends MainScreenEvent {}

class GetCurrentUserEvent extends MainScreenEvent {}

class UpdatePasswordUserEvent extends MainScreenEvent {
  final String pass;
  final String confirmPass;

  UpdatePasswordUserEvent({
    @required this.pass,
    @required this.confirmPass,
  });
}

class UpdateUserEvent extends MainScreenEvent {
  final String name;
  final String surname;
  final String email;
  final String phone;

  UpdateUserEvent({
    @required this.name,
    @required this.surname,
    @required this.email,
    @required this.phone,
  });
}

class UploadProfileImageEvent extends MainScreenEvent {
  final File image;

  UploadProfileImageEvent(this.image);


}
