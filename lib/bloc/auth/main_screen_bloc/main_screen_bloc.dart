import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:no_fine/bloc/auth/main_screen_bloc/main_screen_event.dart';
import 'package:no_fine/bloc/auth/main_screen_bloc/main_screen_state.dart';
import 'package:no_fine/data/api_models/login_user_response.dart';
import 'package:no_fine/data/api_models/server_error.dart';
import 'package:no_fine/data/api_models/user_response.dart';
import 'package:no_fine/data/repository/auth_repo.dart';
import 'package:no_fine/data/repository/user_repo.dart';

class MainScreenBloc extends Bloc<MainScreenEvent, MainScreenState> {
  static const TAG = "MainScreenBloc";
  final AuthRepository _repository;
  final UserRepository _repositoryUser;

  MainScreenBloc(this._repository, this._repositoryUser)
      : super(MainScreenInitState());

  @override
  Stream<MainScreenState> mapEventToState(MainScreenEvent event) async* {
    if (event is LogOutEvent) {
      yield* _mapSignInLoginToState(event);
    }
    if (event is GetCurrentUserEvent) {
      yield* _mapGetUserToState(event);
    }
    if (event is UpdatePasswordUserEvent) {
      yield* _mapUpdatePass(event);
    }
    if (event is UpdateUserEvent) {
      yield* _mapUpdateUser(event);
    }

    if (event is UploadProfileImageEvent) {
      yield* _mapUploadProfileImage(event);
    }

  }

  Stream<MainScreenState> _mapSignInLoginToState(LogOutEvent event) async* {
    try {
      await _repository.logout();
      yield LogOutState();
    } catch (error, stacktrace) {
      print("[$TAG]: $error");
      print(error);
      print(stacktrace);
      yield* _handleException(error);
    }
  }

  Stream<MainScreenState> _mapGetUserToState(GetCurrentUserEvent event) async* {
    try {
      UserResponse userResponse = await _repositoryUser.getUser();
      yield GetUserSucessfful(userResponse);
    } catch (error, stacktrace) {
      print("[$TAG]: $error");
      print(error);
      print(stacktrace);
      yield* _handleException(error);
    }
  }

  Stream<MainScreenState> _mapUpdatePass(UpdatePasswordUserEvent event) async* {
    try {
      ServerMassage res =
          await _repositoryUser.updatePass(event.pass, event.confirmPass);
      add(GetCurrentUserEvent());
      yield SucessffulUpdatePassState(res.message);
    } catch (error, stacktrace) {
      print("[$TAG]: $error");
      print(error);
      print(stacktrace);
      yield* _handleUpdatePassException(error);
    }
  }

  Stream<MainScreenState> _mapUpdateUser(UpdateUserEvent event) async* {
    try {
      //  ServerMassage res =
      await _repositoryUser.updateUser(
          event.name, event.surname, event.email, event.phone);
      yield SucessffulUpdateUserState();
    } catch (error, stacktrace) {
      print("[$TAG]: $error");
      print(error);
      print(stacktrace);
      yield* _handleUpdatePassException(error);
    }
  }


  Stream<MainScreenState> _mapUploadProfileImage(UploadProfileImageEvent event) async* {
    try {
      //  ServerMassage res =
      await _repositoryUser.uploadProfileImage(event.image);

    } catch (error, stacktrace) {
      print("[$TAG]: $error");
      print(error);
      print(stacktrace);
      yield* _handleUpdatePassException(error);
    }
  }

  Stream<MainScreenState> _handleUpdatePassException(error) async* {
    yield UpdatePassFailureState(error.toString());
  }

  Stream<MainScreenState> _handleException(error) async* {
    if (error is PlatformException) {
      yield LogOutFailureState();
    } else {
      yield LogOutFailureState();
    }
  }
}
