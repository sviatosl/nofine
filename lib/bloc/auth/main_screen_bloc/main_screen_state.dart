

import 'package:no_fine/data/api_models/login_user_response.dart';
import 'package:no_fine/data/api_models/user_response.dart';

abstract class MainScreenState {}

class MainScreenInitState extends MainScreenState {}

class LogOutState extends MainScreenState {}

class LogOutFailureState extends MainScreenState {}

class GetUserSucessfful extends MainScreenState {

  UserResponse userResponse;
  GetUserSucessfful(this.userResponse);

}

class SucessffulUpdatePassState extends MainScreenState {

  String updatePassResponse;

  SucessffulUpdatePassState(this.updatePassResponse);

}
class UpdatePassFailureState extends MainScreenState {

  String failureResponse;

  UpdatePassFailureState(this.failureResponse);

}

class SucessffulUpdateUserState extends MainScreenState {

 /* String updateUserResponse;

  SucessffulUpdateUserState(this.updateUserResponse);*/

}