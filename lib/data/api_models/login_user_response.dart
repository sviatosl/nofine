
class LoginUserResponse {
  int id;
  String name;
  String surname;
  String phone;
  String email;
  String bill;
  String privateKey;
  String seedBill;
  String balance;
  int packageId;
  int countryId;
  String apiToken;
  Null provider;
  Null providerId;
  int block;
  String affiliateId;
  String response;


  LoginUserResponse(
      {this.id,
        this.name,
        this.surname,
        this.phone,
        this.email,
        this.bill,
        this.privateKey,
        this.seedBill,
        this.balance,
        this.packageId,
        this.countryId,
        this.apiToken,
        this.provider,
        this.providerId,
        this.block,
        this.affiliateId,
        this.response});

  LoginUserResponse.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    surname = json['surname'];
    phone = json['phone'];
    email = json['email'];
    bill = json['bill'];
    privateKey = json['privateKey'];
    seedBill = json['seed_bill'];
    balance = json['balance'];
    packageId = json['package_id'];
    countryId = json['country_id'];
    apiToken = json['api_token'];
    provider = json['provider'];
    providerId = json['provider_id'];
    block = json['block'];
    affiliateId = json['affiliate_id'];
    response = json['response'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['surname'] = this.surname;
    data['phone'] = this.phone;
    data['email'] = this.email;
    data['bill'] = this.bill;
    data['privateKey'] = this.privateKey;
    data['seed_bill'] = this.seedBill;
    data['balance'] = this.balance;
    data['package_id'] = this.packageId;
    data['country_id'] = this.countryId;
    data['api_token'] = this.apiToken;
    data['provider'] = this.provider;
    data['provider_id'] = this.providerId;
    data['block'] = this.block;
    data['affiliate_id'] = this.affiliateId;
    data['response'] = this.response;
    return data;
  }
}


