class PackagesResponse {
  int id;
  String name;
  String price;
  int limit;
  String description;

  PackagesResponse({this.id, this.name, this.price, this.limit, this.description});

  PackagesResponse.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    price = json['price'];
    limit = json['limit'];
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['price'] = this.price;
    data['limit'] = this.limit;
    data['description'] = this.description;
    return data;
  }
}