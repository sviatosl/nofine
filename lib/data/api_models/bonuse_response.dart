class BonuseResponce {
  String autoVin;
  String autoNumber;
  String autoLicense;

  BonuseResponce({this.autoVin, this.autoNumber, this.autoLicense});

  BonuseResponce.fromJson(Map<String, dynamic> json) {
    autoVin = json['auto_vin'];
    autoNumber = json['auto_number'];
    autoLicense = json['auto_license'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['auto_vin'] = this.autoVin;
    data['auto_number'] = this.autoNumber;
    data['auto_license'] = this.autoLicense;
    return data;
  }
}