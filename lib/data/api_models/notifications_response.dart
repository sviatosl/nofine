class NotificationsResponse {
  NotificationsResponse({
    this.id,
    this.type,
    this.notifiableType,
    this.userId,
    this.name,
    this.status,
    this.readAt,
    this.createdAt,
  });

  String id;
  String type;
  String notifiableType;
  int userId;
  String name;
  String status;
  dynamic readAt;
  DateTime createdAt;

  factory NotificationsResponse.fromJson(Map<String, dynamic> json) => NotificationsResponse(
    id: json["id"],
    type: json["type"],
    notifiableType: json["notifiable_type"],
    userId: json["user_id"],
    name: json["name"],
    status: json["status"],
    readAt: json["read_at"],
    createdAt: DateTime.parse(json["created_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "type": type,
    "notifiable_type": notifiableType,
    "user_id": userId,
    "name": name,
    "status": status,
    "read_at": readAt,
    "created_at": createdAt.toIso8601String(),
  };
}
