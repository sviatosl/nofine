
class NewsResp{

  List<NewsResponse> listNews;
  NewsResp({
    this.listNews,

  });

  NewsResp.fromJson(Map<String, dynamic> json) {

    if (json['seasonBean'] != null) {
      listNews = new List<NewsResponse>();
      json['seasonBean'].forEach((v) {
        listNews.add(new NewsResponse.fromJson(v));
      });
    }
  }
}


class NewsResponse {
  NewsResponse({
    this.id,
    this.text,
    this.subject,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  String text;
  String subject;
  DateTime createdAt;
  String updatedAt;

  factory NewsResponse.fromJson(Map<String, dynamic> json) => NewsResponse(
    id: json["id"],
    text: json["text"],
    subject: json["subject"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "text": text,
    "subject": subject,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt,
  };
}
/*class NewsResponse {
  NewsResponse({
    this.id,
    this.text,
    this.subject,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  String text;
  String subject;
  DateTime createdAt;
  DateTime updatedAt;

  factory NewsResponse.fromJson(Map<String, dynamic> json) => NewsResponse(
    id: json["id"],
    text: json["text"],
    subject: json["subject"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "text": text,
    "subject": subject,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
  };
}*/
