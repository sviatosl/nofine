/*
class FinesListResponse {
  final List<FineResponse> results;

  FinesListResponse(this.results);

  FinesListResponse.fromJsonArray(List json)
      : results = json.map((i) => FineResponse.fromJson(i)).toList();
}*/


class FineResponse {
  int id;
  String uin;
  int userId;
  String comment;
  String addCoin;
  int statusId;
  int type;
  DateTime createdAt;
  DateTime updatedAt;

  FineResponse(
      {this.id,
        this.uin,
        this.userId,
        this.comment,
        this.addCoin,
        this.statusId,
        this.type,
        this.createdAt,
        this.updatedAt});

  FineResponse.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    uin = json['uin'];
    userId = json['user_id'];
    comment = json['comment'];
    addCoin = json['add_coin'];
    statusId = json['status_id'];
    type = json['type'];
    createdAt =DateTime.parse(json['created_at']);
    updatedAt = DateTime.parse(json['updated_at']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['uin'] = this.uin;
    data['user_id'] = this.userId;
    data['comment'] = this.comment;
    data['add_coin'] = this.addCoin;
    data['status_id'] = this.statusId;
    data['type'] = this.type;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}