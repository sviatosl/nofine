class SettingsResponse2 {
  int id;
  String coin;
  String coinReferred;
  String indexCoin;
  String wavesCoin;
  int accounts;
  int login;
  int register;
  int fines;
  int payments;
  int transfer;
  int packages;
  int transferCoins;
  String apiKey;

  SettingsResponse2(
      {this.id,
        this.coin,
        this.coinReferred,
        this.indexCoin,
        this.wavesCoin,
        this.accounts,
        this.login,
        this.register,
        this.fines,
        this.payments,
        this.transfer,
        this.packages,
        this.transferCoins,
        this.apiKey});

  SettingsResponse2.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    coin = json['coin'];
    coinReferred = json['coin_referred'];
    indexCoin = json['index_coin'];
    wavesCoin = json['waves_coin'];
    accounts = json['accounts'];
    login = json['login'];
    register = json['register'];
    fines = json['fines'];
    payments = json['payments'];
    transfer = json['transfer'];
    packages = json['packages'];
    transferCoins = json['transfer_coins'];
    apiKey = json['api_key'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['coin'] = this.coin;
    data['coin_referred'] = this.coinReferred;
    data['index_coin'] = this.indexCoin;
    data['waves_coin'] = this.wavesCoin;
    data['accounts'] = this.accounts;
    data['login'] = this.login;
    data['register'] = this.register;
    data['fines'] = this.fines;
    data['payments'] = this.payments;
    data['transfer'] = this.transfer;
    data['packages'] = this.packages;
    data['transfer_coins'] = this.transferCoins;
    data['api_key'] = this.apiKey;
    return data;
  }
}