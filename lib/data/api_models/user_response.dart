class UserResponse {
  UserResponse({
    this.id,
    this.name,
    this.surname,
    this.phone,
    this.email,
    this.bill,
    this.privateKey,
    this.seedBill,
    this.balance,
    this.image,
    this.inn,
    this.ogpn,
    this.address,
    this.packageId,
    this.countryId,
    this.block,
    this.referredBy,
    this.affiliateId,
    this.partnerBlock,
    this.provider,
    this.providerId,
    this.emailVerifiedAt,
    this.apiToken,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  String name;
  String surname;
  String phone;
  String email;
  String bill;
  String privateKey;
  String seedBill;
  String balance;
  String image;
  dynamic inn;
  dynamic ogpn;
  dynamic address;
  int packageId;
  int countryId;
  int block;
  dynamic referredBy;
  String affiliateId;
  int partnerBlock;
  String provider;
  String providerId;
  dynamic emailVerifiedAt;
  String apiToken;
  DateTime createdAt;
  DateTime updatedAt;

  factory UserResponse.fromJson(Map<String, dynamic> json) => UserResponse(
    id: json["id"],
    name: json["name"],
    surname: json["surname"],
    phone: json["phone"],
    email: json["email"],
    bill: json["bill"],
    privateKey: json["privateKey"],
    seedBill: json["seed_bill"],
    balance: json["balance"],
    image: json["image"],
    inn: json["inn"],
    ogpn: json["ogpn"],
    address: json["address"],
    packageId: json["package_id"],
    countryId: json["country_id"],
    block: json["block"],
    referredBy: json["referred_by"],
    affiliateId: json["affiliate_id"],
    partnerBlock: json["partner_block"],
    provider: json["provider"],
    providerId: json["provider_id"],
    emailVerifiedAt: json["email_verified_at"],
    apiToken: json["api_token"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "surname": surname,
    "phone": phone,
    "email": email,
    "bill": bill,
    "privateKey": privateKey,
    "seed_bill": seedBill,
    "balance": balance,
    "image": image,
    "inn": inn,
    "ogpn": ogpn,
    "address": address,
    "package_id": packageId,
    "country_id": countryId,
    "block": block,
    "referred_by": referredBy,
    "affiliate_id": affiliateId,
    "partner_block": partnerBlock,
    "provider": provider,
    "provider_id": providerId,
    "email_verified_at": emailVerifiedAt,
    "api_token": apiToken,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
  };
}
