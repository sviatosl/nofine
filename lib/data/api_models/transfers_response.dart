/*class TransferResponse {
  List<SenderUser> senderUser;
  List<RecipientUser> recipientUser;

  TransferResponse({this.senderUser, this.recipientUser});

  TransferResponse.fromJson(Map<String, dynamic> json) {
    if (json['sender_user'] != null) {
      senderUser = new List<SenderUser>();
      json['sender_user'].forEach((v) {
        senderUser.add(new SenderUser.fromJson(v));
      });
    }
    if (json['recipient_user'] != null) {
      recipientUser = new List<RecipientUser>();
      json['recipient_user'].forEach((v) {
        recipientUser.add(new RecipientUser.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.senderUser != null) {
      data['sender_user'] = this.senderUser.map((v) => v.toJson()).toList();
    }
    if (this.recipientUser != null) {
      data['recipient_user'] =
          this.recipientUser.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class SenderUser {
  int id;
  int recipientUser;
  int senderUser;
  int coin;
  String bill;
  int status;
  String createdAt;
  String updatedAt;

  SenderUser(
      {this.id,
        this.recipientUser,
        this.senderUser,
        this.coin,
        this.bill,
        this.status,
        this.createdAt,
        this.updatedAt});

  SenderUser.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    recipientUser = json['recipient_user'];
    senderUser = json['sender_user'];
    coin = json['coin'];
    bill = json['bill'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['recipient_user'] = this.recipientUser;
    data['sender_user'] = this.senderUser;
    data['coin'] = this.coin;
    data['bill'] = this.bill;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class RecipientUser {
  int id;
  int recipientUser;
  int senderUser;
  int coin;
  String bill;
  int status;
  String createdAt;
  String updatedAt;

  RecipientUser(
      {this.id,
        this.recipientUser,
        this.senderUser,
        this.coin,
        this.bill,
        this.status,
        this.createdAt,
        this.updatedAt});

  RecipientUser.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    recipientUser = json['recipient_user'];
    senderUser = json['sender_user'];
    coin = json['coin'];
    bill = json['bill'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['recipient_user'] = this.recipientUser;
    data['sender_user'] = this.senderUser;
    data['coin'] = this.coin;
    data['bill'] = this.bill;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}*/


class TransferResponse {
  TransferResponse({
    this.senderUser,
    this.recipientUser,
  });

  final List<User> senderUser;
  final List<User> recipientUser;

  factory TransferResponse.fromJson(Map<String, dynamic> json) => TransferResponse(
    senderUser: List<User>.from(json["sender_user"].map((x) => User.fromJson(x))),
    recipientUser: List<User>.from(json["recipient_user"].map((x) => User.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "sender_user": List<dynamic>.from(senderUser.map((x) => x.toJson())),
    "recipient_user": List<dynamic>.from(recipientUser.map((x) => x.toJson())),
  };
}

class User {
  User({
    this.id,
    this.recipientUser,
    this.senderUser,
    this.coin,
    this.bill,
    this.status,
    this.createdAt,
    this.updatedAt,
  });

  final int id;
  final String recipientUser;
  final String senderUser;
  final int coin;
  final String bill;
  final String status;
  final DateTime createdAt;
  final DateTime updatedAt;

  factory User.fromJson(Map<String, dynamic> json) => User(
    id: json["id"],
    recipientUser: json["recipient_user"],
    senderUser: json["sender_user"],
    coin: json["coin"],
    bill: json["bill"],
    status: json["status"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "recipient_user": recipientUser,
    "sender_user": senderUser,
    "coin": coin,
    "bill": bill,
    "status": status,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
  };
}


