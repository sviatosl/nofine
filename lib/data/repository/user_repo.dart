import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:no_fine/data/api_models/login_user_response.dart';
import 'package:no_fine/data/api_models/server_error.dart';
import 'package:no_fine/data/api_models/user_response.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class UserRepository {
  Future<UserResponse> getUser();

  Future<ServerMassage> updatePass(String pass, String updatePass);

  Future updateUser(String name, String surname, String email, String phone);

  Future uploadProfileImage(File image);
}

class UserRepositoryImpl extends UserRepository {
  final provider;

  UserRepositoryImpl(this.provider);

  @override
  Future<UserResponse> getUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    int idUser = prefs.getInt('userId');
    print('get6 user respo');
    print(token);
    print(idUser);
    UserResponse userResponse = await provider.getUser(token, idUser);
    return userResponse;
  }

  @override
  Future<ServerMassage> updatePass(String pass, String updatePass) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    return await provider.updatePassword(token, pass, updatePass);
  }

  @override
  Future updateUser(
      String name, String surname, String email, String phone) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    int idUser = prefs.getInt('userId');
    return await provider.updateUser(
        token, idUser, name, surname, email, phone);
  }

  @override
  Future uploadProfileImage(File image) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    return await provider.uploadProfileImage(token, image);
  }
}
