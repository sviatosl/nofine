import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:no_fine/data/api_models/login_user_response.dart';

import 'package:shared_preferences/shared_preferences.dart';

abstract class AuthRepository {
  Future login(String phone, String password);

  Future register(String name, String surname, String phone, String email,
      String password, String confirm_password);

  Future logout();

  Future googleLogIn();

  Future facebookLogIn();
}

class AuthRepositoryImpl extends AuthRepository {
  final provider;
  SharedPreferences _sharedPreferences;
  static const tokenKey = "apiToken";

  AuthRepositoryImpl(this.provider);

  GoogleSignIn _googleSignIn = GoogleSignIn(
      clientId:
          '800251787897-pogiuveum0f7lquvc8oe03geqvqg25h4.apps.googleusercontent.com');

//GoogleSignInAuthentication _googleSignInAuthentication ;
//GoogleSignInAccount googleSignInAccount;

  // AuthRepositoryImpl(this.provider, this._sharedPreferences);

  /*@override
  Future authUser(String login, String password) async {
    AuthResponse response = await provider.authUser(login, password);
    _saveToken(response.token);
  }*/

  @override
  Future register(String name, String surname, String phone, String email,
      String password, String confirm_password) async {
    LoginUserResponse userResponse = await provider.signUp(
        name, surname, phone, email, password, confirm_password);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('token', userResponse.apiToken);
    prefs.setInt('userId', userResponse.id);
    prefs.setString('name', userResponse.name);
    prefs.setString('surname', userResponse.surname);
    prefs.setInt('packageId', userResponse.packageId);
    return userResponse;
  }

  @override
  Future login(String phone, String password) async {
    LoginUserResponse userResponse = await provider.signIn(phone, password);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print('login api token');
    print(userResponse.apiToken);
    prefs.setString('token', userResponse.apiToken);
    prefs.setString('name', userResponse.name);
    prefs.setString('surname', userResponse.surname);
    prefs.setInt('userId', userResponse.id);
    prefs.setInt('packageId', userResponse.packageId);
  }

  @override
  Future logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await provider.logOut(prefs.getString('token'));
    await prefs.clear();
  }

  @override
  Future facebookLogIn() async {
    final facebookLogin = FacebookLogin();
    final FacebookLoginResult res = await facebookLogin.logIn(['email']);

    Response response = await Dio().get(
        'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,picture,email&access_token=${res.accessToken.token}');
    final data = jsonDecode(response.data) as Map<String, dynamic>;
    print( data['first_name']);
    print( data['picture']['data']['url']);
    print( data['id']);
    LoginUserResponse userResponse = await provider.signInWithSocialNetworks(
        data['first_name'],
        data['last_name'],
        data['email'],
        data['picture']['data']['url'],
        'facebook',
        data['id']);
    /*LoginUserResponse userResponse = await provider.signInWithSocialNetworks(
        'first',
        'last2',
        'emqwdail@gmail.com',
        'trt7tdydaad',
        'facebook',
        '1231231231231');*/

    // print('facebbok api token');

    //  SharedPreferences prefs = await SharedPreferences.getInstance();

    //  print( userResponse.apiToken);
    // await  prefs.setString('token', userResponse.apiToken);
    //  prefs.setInt('userId', userResponse.id);
    // prefs.setInt('packageId', userResponse.packageId);
  }

  @override
  Future googleLogIn() async {
    // await _googleSignIn.signIn();
    //  GoogleSignIn().signIn();

    final result = await _googleSignIn.signIn();
    final GoogleSignInAuthentication googleAuth = await result.authentication;
    print('google current user');
    print(_googleSignIn.currentUser);
    print('accessToken : ' + googleAuth.accessToken);
    print(_googleSignIn.currentUser.displayName.split(' ')[0]);
    print(_googleSignIn.currentUser.displayName.split(' ')[1]);
    print('urr image !!!');
    print('urr image !!!');
    if(_googleSignIn.currentUser.photoUrl==null){print('google image is null!!!!!!!!!!!!!!!!!');}
    if(_googleSignIn.currentUser.photoUrl.toString() is String){print('google image is string!!!!!!!!!!!!!!!!!');}

    print(_googleSignIn.currentUser.photoUrl);



    LoginUserResponse userResponse = await provider.signInWithSocialNetworks(
        _googleSignIn.currentUser.displayName.split(' ')[0],
        _googleSignIn.currentUser.displayName.split(' ')[1],
        _googleSignIn.currentUser.email,
        _googleSignIn.currentUser.photoUrl,
        'google',
        _googleSignIn.currentUser.id);

    /* SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('token', userResponse.apiToken);
    prefs.setInt('userId', userResponse.id);
    prefs.setInt('packageId', userResponse.packageId);*/
  }
}
