import 'package:no_fine/data/api_models/bonuse_response.dart';
import 'package:no_fine/data/api_models/fines_response.dart';
import 'package:no_fine/data/api_models/news_response.dart';
import 'package:no_fine/data/api_models/notifications_response.dart';
import 'package:no_fine/data/api_models/packages_response.dart';
import 'package:no_fine/data/api_models/server_error.dart';
import 'package:no_fine/data/api_models/settings_respomse.dart';

import 'package:no_fine/data/api_models/transfers_response.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class TransferRepository {
  Future<ServerMassage> transCoinToUser(String recipientBill, int coin);

  Future<ServerMassage> transCoinToStockExchange(int coin);

  Future<ServerMassage> addBonuse(
      String autoVin, String autoNumber, String autoLicense);

  Future<List<BonuseResponce>> getAllBonuses();

  Future<ServerMassage> addFine(String uin, int type);

  Future<List<FineResponse>> getAllFines();

  Future<List<PackagesResponse>> getAllPackages();

  Future<TransferResponse> getAllTransfers();

  Future<List<NotificationsResponse>> getNotifications();

  Future readOneNotification(String idNoti);

  Future readAllNotification();

  Future<SettingsResponse2> getSettings();

  Future<ServerMassage> addFitback(String description);

  Future<List<NewsResponse>> getAllNews();
}

class TransferRepositoryImpl extends TransferRepository {
  final provider;

  TransferRepositoryImpl(this.provider);

  @override
  Future<ServerMassage>  transCoinToUser(String recipientBill, int coin) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
   return await provider.transCoinToUser(token, recipientBill, coin);
  }

  @override
  Future<ServerMassage> transCoinToStockExchange(int coin) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
   return await provider.transCoinToStockExchange(token, coin);
  }

  @override
  Future<ServerMassage> addBonuse(
      String autoVin, String autoNumber, String autoLicense) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    return await provider.addBonuse(token, autoVin, autoNumber, autoLicense);
  }

  @override
  Future<List<BonuseResponce>> getAllBonuses() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    return await provider.getAllBonusesByCurrentUser(token);
  }

  @override
  Future<ServerMassage> addFine(String uin, int type) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    return await provider.addFine(token, uin, type);
  }

  @override
  Future<List<FineResponse>> getAllFines() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    return await provider.getAllFinesByCurrentUser(token);
  }

  @override
  Future<List<PackagesResponse>> getAllPackages() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    return await provider.getAllPackages(token);
  }

  @override
  Future<TransferResponse> getAllTransfers() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    return await provider.getTransfers(token);
  }

  @override
  Future<List<NotificationsResponse>> getNotifications() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    return await provider.getNotifications(token);
  }

  @override
  Future readOneNotification(String idNoti) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    return await provider.readOneNotification(token, idNoti);
  }

  @override
  Future readAllNotification() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    return await provider.readAllNotification(token);
  }

  @override
  Future<SettingsResponse2> getSettings() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    /*  SettingsResponse settingsResponse = await provider.getSettings(token);
    print('settings repo : ');
    print(settingsResponse.);*/
    print('repo start get settings');
    return await provider.getSettings(token);
  }

  @override
  Future<ServerMassage> addFitback(String description) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    return await provider.sentFitback(token, description);
  }

  @override
  Future<List<NewsResponse>> getAllNews() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    return await provider.getAllNews(token);
  }



}
