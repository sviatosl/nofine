
import 'package:no_fine/data/api_models/settings_respomse.dart';

import 'package:shared_preferences/shared_preferences.dart';

abstract class SettingsRepository {
  Future<SettingsResponse2> getSettings();


}

class SettingsRepositoryImpl extends SettingsRepository {
  final provider;

  SettingsRepositoryImpl(this.provider);

  @override
  Future<SettingsResponse2> getSettings() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
   /* SettingsResponse settingsResponse = await provider.getSettings(token);
    print('settings repo : ');
    print(settingsResponse.coin);*/
    return await provider.getSettings(token);
  }

}