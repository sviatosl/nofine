import 'package:shared_preferences/shared_preferences.dart';

class StoreToken {
 static SharedPreferences _sharedPreferences;
  static const tokenKey = "apiToken";



  @override
 static Future<String> getToken() {
    return Future.value(_sharedPreferences.getString(tokenKey));
  }

 static void saveToken(String token) {
    _sharedPreferences.setString(tokenKey, token);
  }





}