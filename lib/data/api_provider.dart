import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:no_fine/data/api_models/news_response.dart';
import 'package:no_fine/data/api_models/notifications_response.dart';
import 'package:no_fine/data/api_models/packages_response.dart';
import 'package:no_fine/data/api_models/server_error.dart';
import 'package:no_fine/data/api_models/login_user_response.dart';
import 'package:no_fine/data/api_models/settings_respomse.dart';
import 'package:no_fine/data/api_models/user_response.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:no_fine/data/api_models/bonuse_response.dart';

import 'api_models/fines_response.dart';

import 'api_models/transfers_response.dart';

class ApiProvider {
  static const BASE_URL = "https://api-fines.profdecor.com.ua";
  static const String apiEndpoint = BASE_URL + "/api/v1/";
  final Dio _dio;

  ApiProvider(this._dio);

  Future<LoginUserResponse> signUp(String name, String surname, String phone,
      String email, String password, String confirm_password) async {
    try {
      Response response = await _dio.post(apiEndpoint + "register", data: {
        "name": name,
        "surname": surname,
        "phone": phone,
        "email": email,
        "password": password,
        "confirm_password": confirm_password
      });
      return LoginUserResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      print(error.response.data);
      print(error.response.statusCode);
      return throw ServerError.fromJson(error.response.data).message;
    }
  }

  Future<LoginUserResponse> signIn(String phone, String password) async {
    try {
      Response response = await _dio.post(apiEndpoint + "login",
          data: {"phone": phone, "password": password});

      print('response login');
      print(response.data);
      print(response.statusMessage);
      print(response.statusCode);

      return LoginUserResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      print(error.response.data);
      print(error.response.statusCode);
      return throw ServerError.fromJson(error.response.data).message;
    }
  }

  Future<LoginUserResponse> signInWithSocialNetworks(
      String name,
      String surname,
      String email,
      dynamic urlimage,
      String provider,
      String providerid) async {
    try {
      Response response =
          await _dio.post(apiEndpoint + "register-social", data: {
        "name": name,
        "surname": surname,
        "email": email,
        "urlimage": urlimage ?? 'null',
        "provider": provider,
        "providerid": providerid,
      });

      print('response social login');
      print(response.data);
      print(response.statusMessage);
      print(response.statusCode);
      SharedPreferences prefs = await SharedPreferences.getInstance();
      print('api token signInWithSocialNetworks : ');
      print(response.data['api_token']);
      print(response.data['image']);
      prefs.setString('token', response.data['api_token']);
      prefs.setInt('userId', response.data['id']);
      prefs.setInt('packageId', response.data['package_id']);
      prefs.setString('name', response.data['name']);
      prefs.setString('surname', response.data['surname']);
      // return LoginUserResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      print('error !!1');
      print(error.response.data);
      return null;
    }
  }

  Future logOut(String apiToken) async {
    try {
      Response response = await _dio.post(apiEndpoint + "logout",
          //  data: {"api_token": apiToken},
          options: Options(headers: {"Authorization": apiToken}));
      print('response log out');
      print(response.data);
      print(response.statusMessage);
      print(response.statusCode);
      return LoginUserResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      //debugPrint(error);
      return null;
    }
  }

  Future<UserResponse> getUser(String apiToken, int id) async {
    try {
      Response response = await _dio.get(
        apiEndpoint + "users/$id",
        //  data: {"api_token": apiToken},
        /* queryParameters: {
          "id": id,
        },*/
        options: Options(headers: {"Authorization": apiToken}),
      );
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString('token');
      int idUser = prefs.getInt('userId');
      print('response getUser');
      print(response.data);
      print(response.statusMessage);
      print(response.statusCode);
      return UserResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      //debugPrint(error);

      return null;
    }
  }

  Future<ServerMassage> transCoinToUser(
      String apiToken, String recipientBill, int coin) async {
    Response response;
    try {
      response = await _dio.post(apiEndpoint + "bills/transfer",
          options: Options(headers: {"Authorization": apiToken}),
          data: {"recipient_bill": recipientBill, "coin": coin});

      // return UserResponse.fromJson(response.data);
      return ServerMassage.fromJson(response.data);
    } on DioError catch (error) {
      return throw ServerError.fromJson(error.response.data).message;
    }
  }

  Future<ServerMassage> transCoinToStockExchange(
      String apiToken, int coin) async {
    try {
      Response response = await _dio.post(apiEndpoint + "bills/exchange",
          //  data: {"api_token": apiToken},
          options: Options(headers: {"Authorization": apiToken}),
          data: {"coin": coin});

      // return UserResponse.fromJson(response.data);
      return ServerMassage.fromJson(response.data);
    } catch (error, stacktrace) {
      return throw ServerError.fromJson(error.response.data).message;
    }
  }

  Future<ServerMassage> addBonuse(String apiToken, String autoVin,
      String autoNumber, String autoLicense) async {
    try {
      Response response = await _dio.post(apiEndpoint + "stimulations",
          options: Options(headers: {"Authorization": apiToken}),
          data: {
            "auto_vin": autoVin,
            "auto_number": autoNumber,
            "auto_license": autoLicense
          });
      return ServerMassage.fromJson(response.data);
    } catch (error, stacktrace) {
      return throw ServerError.fromJson(error.response.data).message;
    }
  }

  Future<List<BonuseResponce>> getAllBonusesByCurrentUser(
      String apiToken) async {
    try {
      Response response = await _dio.get(
        apiEndpoint + "stimulations",
        options: Options(headers: {"Authorization": apiToken}),
      );

      return response.data
          .map<BonuseResponce>((el) => BonuseResponce.fromJson(el))
          .toList();
    } catch (error, stacktrace) {
      return null;
    }
  }

  Future<ServerMassage> addFine(String apiToken, String uin, int type) async {
    Response response;
    try {
      response = await _dio.post(apiEndpoint + "fines",
          options: Options(headers: {"Authorization": apiToken}),
          data: {"uin": uin, "type": type});

      return ServerMassage.fromJson(response.data);
    } catch (error, stacktrace) {
      return throw ServerError.fromJson(error.response.data).message;
    }
  }

  Future<List<FineResponse>> getAllFinesByCurrentUser(String apiToken) async {
    try {
      Response response = await _dio.get(
        apiEndpoint + "fines",
        options: Options(headers: {"Authorization": apiToken}),
      );

      return response.data
          .map<FineResponse>((el) => FineResponse.fromJson(el))
          .toList();
    } catch (error, stacktrace) {
      return null;
    }
  }

  Future<List<PackagesResponse>> getAllPackages(String apiToken) async {
    try {
      Response response = await _dio.get(
        apiEndpoint + "packages",
        options: Options(headers: {"Authorization": apiToken}),
      );

      return response.data
          .map<PackagesResponse>((el) => PackagesResponse.fromJson(el))
          .toList();
    } catch (error, stacktrace) {
      return null;
    }
  }

  Future<TransferResponse> getTransfers(String apiToken) async {
    try {
      Response response = await _dio.get(
        apiEndpoint + "transfers",
        options: Options(headers: {"Authorization": apiToken}),
      );

      return TransferResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      //debugPrint(error);
      print(error);
      print(stacktrace);
      return null;
    }
  }

  Future<List<NotificationsResponse>> getNotifications(String apiToken) async {
    try {
      Response response = await _dio.get(
        apiEndpoint + "notifications",
        options: Options(headers: {"Authorization": apiToken}),
      );
      print(response.data);
      print(response.statusMessage);
      print(response.statusCode);
      return response.data
          .map<NotificationsResponse>(
              (el) => NotificationsResponse.fromJson(el))
          .toList();
    } catch (error, stacktrace) {
      return null;
    }
  }

  Future<List<NotificationsResponse>> readOneNotification(
      String apiToken, String id) async {
    try {
      Response response = await _dio.post(
        apiEndpoint + "notifications/read",
        options: Options(headers: {"Authorization": apiToken}),
        data: {"id": id},
      );
    } catch (error, stacktrace) {
      //debugPrint(error);
      print(error);
      print(stacktrace);
      return null;
    }
  }

  Future<List<NotificationsResponse>> readAllNotification(
      String apiToken) async {
    try {
      Response response = await _dio.get(
        apiEndpoint + "notifications/reading",
        options: Options(headers: {"Authorization": apiToken}),
      );
    } catch (error, stacktrace) {
      return null;
    }
  }

  Future<ServerMassage> updatePassword(
      String apiToken, String pass, String confirmPass) async {
    try {
      Response response = await _dio.post(apiEndpoint + "users/password",
          options: Options(headers: {"Authorization": apiToken}),
          data: {"password": pass, "confirm_password": confirmPass});
      print('update pass: ');
      print(response.data);
      print(response.statusMessage);
      print(response.statusCode);
      return ServerMassage.fromJson(response.data);
    } catch (error, stacktrace) {
      return throw ServerError.fromJson(error.response.data).message;
    }
  }

  Future<SettingsResponse2> getSettings(String apiToken) async {
    try {
      Response response = await _dio.get(apiEndpoint + "settings",
          options: Options(headers: {"Authorization": apiToken}));
      print('settings pass: ');
      print(response.data);
      print(response.statusMessage);
      print(response.statusCode);
      // return SettingsResponse2.fromJson(response.data);
      return response.data
          .map<SettingsResponse2>((el) => SettingsResponse2.fromJson(el))
          .toList()[0];
    } catch (error, stacktrace) {
      print('get setting : ');
      print(error.data.response);
      return null;
      // return throw ServerError.fromJson(error.response.data).message;
    }
  }

  Future updateUser(String apiToken, int id, String name, String surname,
      String email, String phone) async {
    try {
      Response response = await _dio.put(apiEndpoint + "users/$id",
          //  data: {"api_token": apiToken},
          /* queryParameters: {
          "id": id,
        },*/
          options: Options(headers: {"Authorization": apiToken}),
          data: {
            "name": name,
            "surname": surname,
            "email": email,
            "phone": phone
          });

      return UserResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      //debugPrint(error);

      return throw ServerError.fromJson(error.response.data).message;
    }
  }

  Future uploadProfileImage(String apiToken, File image) async {
    try {
      String fileName = image.path.split('/').last;

      FormData data = FormData.fromMap({
        "image": await MultipartFile.fromFile(
          image.path,
          filename: fileName,
        ),
      });

      await _dio.post(apiEndpoint + "users/avatar",
          options: Options(headers: {"Authorization": apiToken}), data: data);
      // return ServerMassage.fromJson(response.data);
    } catch (error, stacktrace) {
      return null;
    }
  }

  Future<ServerMassage> sentFitback(String apiToken, String description) async {
    try {
      Response response = await _dio.post(apiEndpoint + "appeals",
          options: Options(headers: {"Authorization": apiToken}),
          data: {
            "description": description,
          });
      return ServerMassage.fromJson(response.data);
    } catch (error, stacktrace) {
      return throw ServerError.fromJson(error.response.data).message;
    }
  }

  Future <List<NewsResponse>> getAllNews(String apiToken) async {
    try {
      Response response = await _dio.get(
        apiEndpoint + "news",
        options: Options(headers: {"Authorization": apiToken}),
      );
      print('news data : ');
      print(response.data);
        return response.data
          .map<NewsResponse>((el) => NewsResponse.fromJson(el))
          .toList();
    //  print('end resp');
      //return NewsResp.fromJson(response.data);
    } catch (error, stacktrace) {
        print('error news :');
      print(error.response.data);
      throw(error);
      return null;
    }
  }
}
