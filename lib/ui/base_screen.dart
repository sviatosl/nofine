import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:no_fine/bloc/auth/main_screen_bloc/main_screen_bloc.dart';
import 'package:no_fine/bloc/news_bloc/news_bloc.dart';
import 'package:no_fine/bloc/notificatios_bloc/notifications_bloc.dart';
import 'package:no_fine/bloc/settings_bloc/settings_bloc.dart';
import 'package:no_fine/data/api_provider.dart';
import 'package:no_fine/data/repository/auth_repo.dart';
import 'package:no_fine/data/repository/transfer_repo.dart';
import 'package:no_fine/data/repository/user_repo.dart';
import 'package:no_fine/ui/main_screen/main_screen.dart';
import 'package:no_fine/ui/pages/mane_news_screen.dart';
import 'package:no_fine/ui/pages/notification_screen.dart';
import 'package:no_fine/ui/pages/profile_page.dart';
import 'package:no_fine/ui/resoursese/app_colors.dart';
import 'package:no_fine/ui/widgets/custom_alert.dart';
import 'package:no_fine/ui/widgets/submit_button.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BaseScreen extends StatefulWidget {
  static const String route = "/base";

  @override
  _BaseScreenState createState() => _BaseScreenState();
}

class _BaseScreenState extends State<BaseScreen> {
  PageController _pageController;
  int _page = 0;

  _showDialog() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String name = prefs.getString('name');
   String surname = prefs.getString('surname');
    Future.delayed(Duration.zero, () {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return CustomAlert(
              SubmitButton(
                buttonColor: AppColors.primary,
                text: 'Продолжить',
                textColor: AppColors.white,
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              image: 'assets/images/success.png',
              //title: 'Успех',
              description: 'Добро пожаловать ' + name + ' ' + surname + ' !',
            );
          });
    });
  }

  @override
  void initState() {
    super.initState();

    _showDialog();

    _pageController = PageController(initialPage: 0);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: PageView(
          physics: NeverScrollableScrollPhysics(),
          controller: _pageController,
          onPageChanged: onPageChanged,
          children: <Widget>[
            MultiBlocProvider(providers: [
              BlocProvider<NewsBloc>(
                create: (BuildContext context) => NewsBloc(
                  TransferRepositoryImpl(ApiProvider(Dio())),
                ),
              ),
              BlocProvider<SettingsBloc>(
                create: (BuildContext context) => SettingsBloc(
                  TransferRepositoryImpl(ApiProvider(Dio())),
                ),
              ),
            ], child: NewsScreen()),
            BlocProvider(
                create: (_) => NotificationsBloc(
                      TransferRepositoryImpl(ApiProvider(Dio())),
                    ),
                child: NotificationScreen(isVisibleAppBar: false)),
            BlocProvider(
                create: (_) => MainScreenBloc(
                      AuthRepositoryImpl(ApiProvider(Dio())),
                      UserRepositoryImpl(ApiProvider(Dio())),
                    ),
                child: ProfileScreen(isVisibleAppBar: false)),
            BlocProvider(
                create: (_) => MainScreenBloc(
                      AuthRepositoryImpl(ApiProvider(Dio())),
                      UserRepositoryImpl(ApiProvider(Dio())),
                    ),
                child: MainScreen()),
          ],
        ),
        bottomNavigationBar: ClipRRect(
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(22.0),
            topRight: Radius.circular(22.0),
          ),
          child: Container(
            /* decoration: const BoxDecoration(
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(22.0),
                topRight: Radius.circular(22.0),
              ),
            ),*/
            child: BottomNavigationBar(
              backgroundColor: AppColors.primary,
              showSelectedLabels: false,
              showUnselectedLabels: false,
              type: BottomNavigationBarType.fixed,
              selectedItemColor: Colors.green,
              items: [
                /* BottomNavigationBarItem(
                    icon: Image.asset('assets/icons/my_fines_icon.png',
                        // color: AppColors.red,

                        scale: 2),
                    activeIcon: Image.asset('assets/icons/my_fines_icon.png',
                         color: Colors.amber,

                        scale: 2),
                    title: Text('Personal')),
                BottomNavigationBarItem(
                  icon: Icon(Icons.notifications),
                  title: Text('Notifications'),
                ),*/

                _buildBottomNavigationBarItem(
                    iconPath: 'assets/icons/bottom_menu/home.png',
                    activeIconPath:
                        'assets/icons/bottom_menu/selected_home.png',
                    titleText: 'Главная'),
                _buildBottomNavigationBarItem(
                    iconPath: 'assets/icons/bottom_menu/notifications.png',
                    activeIconPath:
                        'assets/icons/bottom_menu/selected_notifications.png',
                    titleText: 'Главная2'),
                _buildBottomNavigationBarItem(
                    iconPath: 'assets/icons/bottom_menu/user.png',
                    activeIconPath:
                        'assets/icons/bottom_menu/selected_profile.png',
                    titleText: 'Главная3'),
                _buildBottomNavigationBarItem(
                    iconPath: 'assets/icons/bottom_menu/grid.png',
                    activeIconPath:
                        'assets/icons/bottom_menu/selected_grid.png',
                    titleText: 'Главная4'),
              ],
              onTap: navigationTapped,
              currentIndex: _page,
            ),
          ),
        )
        /*   bottomNavigationBar: BottomNavigationBar(
        showSelectedLabels: false,
        backgroundColor: AppColors.primary,
        selectedItemColor: AppColors.white,
        unselectedItemColor: AppColors.gray,
        elevation: 20,
        type: BottomNavigationBarType.fixed,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Image.asset(
                'assets/icons/my_fines_icon.png',
                color: AppColors.white,
                scale: 2
            ),
            title: Padding(
              padding: const EdgeInsets.all(4.0),
              child: Text(
                'Сериалы',
                style: TextStyle(fontSize: 10, fontFamily: 'Bold'),
              ),
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              //Icons.home
              Feather.getIconData('grid'),
              size: 22,
            ),
            title: Padding(
              padding: const EdgeInsets.all(4.0),
              child: Text(
                'Сериалы',
                style: TextStyle(fontSize: 10, fontFamily: 'Bold'),
              ),
            ),
          ),
        //  _buildBottomNavigationBarItem(iconPath:'assets/icons/my_fines_icon.png',titleText: '' ),
         // _buildBottomNavigationBarItem(iconPath:'assets/icons/my_fines_icon.png' ,titleText: ''),

        ],
        onTap: navigationTapped,
        currentIndex: _page,
      ),*/
        );
  }

  BottomNavigationBarItem _buildBottomNavigationBarItem({
    @required String titleText,
    @required String iconPath,
    @required String activeIconPath,
  }) {
    return BottomNavigationBarItem(
      icon: Image.asset(
        iconPath,
        color: AppColors.white,
        scale: 2,
      ),
      activeIcon: Image.asset(
        activeIconPath,
        color: AppColors.white,
        scale: 2,
      ),
      title: Text(
        titleText,
      ),
    );
  }

  void navigationTapped(int page) {
    _pageController.jumpToPage(page);
  }

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }

  void onPageChanged(int page) {
    setState(() {
      this._page = page;
    });
  }
}
