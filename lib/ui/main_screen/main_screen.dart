import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:no_fine/bloc/auth/main_screen_bloc/main_screen_bloc.dart';
import 'package:no_fine/bloc/auth/main_screen_bloc/main_screen_event.dart';
import 'package:no_fine/bloc/auth/main_screen_bloc/main_screen_state.dart';

import 'package:no_fine/ui/onboarding/onboarding_screen.dart';
import 'package:no_fine/ui/pages/%20coin_index_screen.dart';
import 'package:no_fine/ui/pages/%20withdrawing_coins_screen.dart';
import 'package:no_fine/ui/pages/agreement_creen.dart';
import 'package:no_fine/ui/pages/info_page.dart';
import 'package:no_fine/ui/pages/my_bonuses_screen.dart';
import 'package:no_fine/ui/pages/my_fines_screen.dart';
import 'package:no_fine/ui/pages/my_pakeges_scren.dart';
import 'package:no_fine/ui/pages/my_trans.dart';
import 'package:no_fine/ui/pages/notification_screen.dart';
import 'package:no_fine/ui/pages/oferta_screen.dart';
import 'package:no_fine/ui/pages/policy_screen.dart';
import 'package:no_fine/ui/pages/profile_page.dart';
import 'package:no_fine/ui/pages/transfer_of_coins_screen_to_person.dart';
import 'package:no_fine/ui/pages/write_us.dart';

import 'package:no_fine/ui/resoursese/app_colors.dart';
import 'package:no_fine/ui/resoursese/app_styles.dart';
import 'package:no_fine/ui/widgets/custom_alert.dart';
import 'package:no_fine/ui/widgets/menu_drop_down_item.dart';
import 'package:no_fine/ui/widgets/menu_item.dart';
import 'package:no_fine/ui/widgets/submit_button.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MainScreen extends StatefulWidget {
  static const String route = "/main";

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  int index = 1;
  int indexColorButton = 1;
  int isSelectediconIndex = 0;

  MainScreenBloc _bloc;
  bool _isOpen = false;

  @override
  void initState() {
    _bloc = BlocProvider.of<MainScreenBloc>(context);
    _bloc.add(GetCurrentUserEvent());
    super.initState();
  }

  _getPackageId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print('packageID' + prefs.getInt('packageId').toString());
    // packageID =await prefs.getInt('packageId');
    return prefs.getInt('packageId');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: SafeArea(
        top: true,
        child: BlocBuilder<MainScreenBloc, MainScreenState>(
            builder: (context, state) {
          if (state is LogOutState) {
            WidgetsBinding.instance.addPostFrameCallback(
                (_) => Navigator.of(context).pushReplacementNamed(
                      OnboardingScreen.route,
                    ));
          }
          if (state is GetUserSucessfful) {
            return Column(
              children: [
                Container(
                  decoration: BoxDecoration(
                    color: AppColors.primary,
                    borderRadius: const BorderRadius.only(
                      bottomRight: Radius.circular(22.0),
                      bottomLeft: Radius.circular(22.0),
                    ),
                  ),
                  height: 80.0,
                  //color: AppColors.primary,
                  child: Row(
                    children: [
                      const SizedBox(
                        width: 20.0,
                      ),
                      InkWell(
                        onTap: () async {
                          setState(() {
                            isSelectediconIndex = 6;
                          });
                          await Navigator.of(context).pushNamed(
                            ProfileScreen.route,
                          );
                          _bloc.add(GetCurrentUserEvent());
                        },
                        child: Center(
                          child: state.userResponse.image != null
                              ? ClipOval(
                                  child: Image.network(
                                  'https://api-fines.profdecor.com.ua/public/upload/' +
                                      state.userResponse.image,
                                  fit: BoxFit.cover,
                                  height: 66,
                                  width: 66,
                                ))
                              : Image.asset('assets/icons/profile_icon.png',
                                  scale: 2),
                        ),
                      ),
                      /* Image.network(
                        state.userResponse.,
                        fit: BoxFit.cover,
                        //width: double.infinity,
                        // fit: BoxFit.cover,
                        //image: movies[index].Image,
                      ),*/

                      const SizedBox(
                        width: 20.0,
                      ),
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              (state.userResponse.name +
                                      " " +
                                      state.userResponse.surname) ??
                                  '',
                              style: AppStyles.Roboto500(),
                            ),
                            const SizedBox(
                              height: 4,
                            ),
                            Text(
                              'Баланс : ' + state.userResponse.balance,
                              style: AppStyles.Roboto500(fontSize: 12),
                            ),
                            const SizedBox(
                              height: 4,
                            ),
                            Text('Статус: Бронзовый',
                                style: AppStyles.Roboto500(fontSize: 12)),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                const SizedBox(
                  height: 12,
                ),
                Expanded(
                  child: ListView(
                    children: [
                      SelectableMenuItemWidget(
                        index: 1,
                        isSelected: isSelectediconIndex,
                        //icon: 'assets/icons/my_fines_icon.png',Vector (28).png
                        icon: 'assets/icons/my_fines_icon.png',
                        title: 'Мои штрафы',
                        onTap: () {
                          setState(() {
                            isSelectediconIndex = 1;
                            _isOpen = false;
                          });
                          Navigator.of(context).pushNamed(
                            MyFinesScreen.route,
                          );
                        },
                      ),
                      SelectableMenuItemWidget(
                        index: 2,
                        isSelected: isSelectediconIndex,
                        icon: 'assets/icons/my_bonuses_icon.png',
                        title: 'Мои бонусы',
                        onTap: () {
                          setState(() {
                            isSelectediconIndex = 2;
                            _isOpen = false;
                          });
                          Navigator.of(context).pushNamed(
                            MyBonusesScreen.route,
                          );
                        },
                      ),
                      /*   SelectableMenuItemWidget(
                            index: 3,
                            isSelected: isSelectediconIndex,
                            icon: 'assets/icons/messages_icon.png',
                            title: 'Уведомления',
                            onTap: () {
                              setState(() {
                                isSelectediconIndex = 3;
                                _isOpen = false;
                              });
                              Navigator.of(context).pushNamed(
                                NotificationScreen.route,
                              );
                            },
                          ),*/

                      InkWell(
                        onTap: () {
                          setState(() {
                            isSelectediconIndex = 4;
                            _isOpen == true ? _isOpen = false : _isOpen = true;
                          });
                        },
                        child: Row(
                          children: [
                            SelectableMenuItemWidget(
                              index: 4,
                              isSelected: isSelectediconIndex,
                              icon: 'assets/icons/my_operations_icon.png',
                              title: 'Мои операции',
                              /* onTap: () {
                               */ /* setState(() {
                                  isSelectediconIndex = 4;
                                });*/ /*
                                */ /* Navigator.of(context).pushNamed(
                                  MyTransScreen.route,
                                );*/ /*
                              },*/
                            ),
                            /*Icon(
                          'assets/icons/arrow_right.png',
                          color: isSelectediconIndex ==4?AppColors.primary:AppColors.gray,
                          size: 12.0,
                        ),*/
                            const SizedBox(width: 60.0),
                            Image.asset(
                                isSelectediconIndex == 4
                                    ? 'assets/icons/arrow_down.png'
                                    : 'assets/icons/arrow_right.png',
                                color: isSelectediconIndex == 4
                                    ? AppColors.primary
                                    : AppColors.gray,
                                scale: 2)
                          ],
                        ),
                      ),
                      Visibility(
                          visible: _isOpen == true,
                          child: Column(
                            children: [
                              DropDownMenuItemWidget(
                                //index: 4,
                                //  isSelected: isSelectediconIndex,
                                //   icon: 'assets/icons/translation_to_user.png',
                                title: 'Мои операции',
                                onTap: () async {
                                  await Navigator.of(context).pushNamed(
                                    MyTransScreen.route,
                                  );
                                  _bloc.add(GetCurrentUserEvent());
                                },
                              ),
                              DropDownMenuItemWidget(
                                //index: 4,
                                //  isSelected: isSelectediconIndex,
                                //  icon: 'assets/icons/translation_to_user.png',
                                title: 'Перевод пользователю',
                                onTap: () async {

                                  await Navigator.of(context).pushNamed(
                                    TransferCoinsToPersonScreen.route,
                                  );
                                  _bloc.add(GetCurrentUserEvent());
                                },
                              ),
                              DropDownMenuItemWidget(
                                //  index: 4,
                                //  isSelected: isSelectediconIndex,
                                //  icon: 'assets/icons/transfer_to_exchange.png',
                                title: 'Перевод на биржу',
                                onTap: () async {
                                  await Navigator.of(context).pushNamed(
                                    WithdrawingCoinsScreen.route,
                                  );
                                  _bloc.add(GetCurrentUserEvent());
                                },
                              ),
                            ],
                          )),
                      SelectableMenuItemWidget(
                        index: 5,
                        isSelected: isSelectediconIndex,
                        icon: 'assets/icons/packages_icon.png',
                        title: 'Пакеты',
                        onTap: () {
                          setState(() {
                            isSelectediconIndex = 5;
                            _isOpen = false;
                          });
                          Navigator.of(context).pushNamed(
                            MyPakagesScreen.route,
                          );
                        },
                      ),
                      SelectableMenuItemWidget(
                        index: 6,
                        isSelected: isSelectediconIndex,
                        icon: 'assets/icons/profile.png',
                        title: 'Профиль',
                        onTap: () async {
                          setState(() {
                            isSelectediconIndex = 6;
                            _isOpen = false;
                          });
                          await Navigator.of(context).pushNamed(
                            ProfileScreen.route,
                          );
                          _bloc.add(GetCurrentUserEvent());
                        },
                      ),
                      /* SelectableMenuItemWidget(
                        index: 7,
                        isSelected: isSelectediconIndex,
                        icon: 'assets/icons/index_icon.png',
                        title: 'Индекс монеты',
                        onTap: () {
                          setState(() {
                            isSelectediconIndex = 7;
                          });

                          Navigator.of(context).pushNamed(
                            CoinIndexScreen.route,
                          );
                        },
                      ),*/
                      SelectableMenuItemWidget(
                        index: 8,
                        isSelected: isSelectediconIndex,
                        icon: 'assets/icons/info_icon.png',
                        title: 'О сервисе',
                        onTap: () {
                          setState(() {
                            isSelectediconIndex = 8;
                            _isOpen = false;
                          });
                          Navigator.of(context).pushNamed(
                            InfoScreen.route,
                          );
                        },
                      ),
                      SelectableMenuItemWidget(
                        index: 9,
                        isSelected: isSelectediconIndex,
                        icon: 'assets/icons/help_icon.png',
                        title: 'Помощь',
                        onTap: () {
                          setState(() {
                            isSelectediconIndex = 9;
                            _isOpen = false;
                          });

                          Navigator.of(context).pushNamed(
                            WriteUsScreen.route,
                          );
                        },
                      ),
                      SelectableMenuItemWidget(
                        index: 10,
                        isSelected: isSelectediconIndex,
                        icon: 'assets/icons/log_out_icon.png',
                        title: 'Выход',
                        onTap: () {
                          setState(() {
                            isSelectediconIndex = 10;
                          });

                          showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return CustomAlert(
                                  Row(
                                    children: [
                                      Expanded(
                                        child: SubmitButton(
                                          buttonColor: AppColors.primary,
                                          text: 'Да',
                                          textColor: AppColors.white,
                                          onPressed: () {
                                            _onLogOutClick();
                                          },
                                        ),
                                      ),
                                      const SizedBox(
                                        width: 10,
                                      ),
                                      Expanded(
                                        child: SubmitButton(
                                          buttonColor:
                                              AppColors.backgroundTransparent,
                                          borderColor: AppColors.primary,
                                          text: 'Нет',
                                          textColor: AppColors.primary,
                                          onPressed: () {
                                            Navigator.pop(context);
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                  image: 'assets/images/success.png',
                                  //title: 'Успех',
                                  description: 'Вы точно хотите выйти?',
                                );
                              });
                        },
                      ),
                      const SizedBox(
                        height: 60,
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.of(context).pushNamed(
                            PolicyScreen.route,
                          );
                        },
                        child: Container(
                            height: 30,
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 16.0),
                              child: Center(
                                  child: Text(
                                'Политика конфиденциальности',
                                style: AppStyles.Roboto400(
                                    color: AppColors.primary),
                              )),
                            )),
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.of(context).pushNamed(
                            OfertaScreen.route,
                          );
                        },
                        child: Container(
                            height: 30,
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 16.0),
                              child: Center(
                                  child: Text(
                                'Договор оферты',
                                style: AppStyles.Roboto400(
                                    color: AppColors.primary),
                              )),
                            )),
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.of(context).pushNamed(
                            AgreementScreen.route,
                          );
                        },
                        child: Container(
                            height: 30,
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 16.0),
                              child: Center(
                                  child: Text(
                                'Соглашение',
                                style: AppStyles.Roboto400(
                                    color: AppColors.primary),
                              )),
                            )),
                      )
                    ],
                  ),
                )
              ],
            );
          }
          return Center(
              child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(AppColors.primary),
          ));
        }),
      ),
    );
  }

  void _onLogOutClick() {
    _bloc.add(LogOutEvent());
  }
}
