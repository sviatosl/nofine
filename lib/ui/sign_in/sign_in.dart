import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:no_fine/bloc/auth/sign_in_bloc/sign_in_bloc.dart';
import 'package:no_fine/bloc/auth/sign_in_bloc/sign_in_events.dart';
import 'package:no_fine/bloc/auth/sign_in_bloc/sign_in_state.dart';
import 'package:no_fine/ui/base_screen.dart';
import 'package:no_fine/ui/main_screen/main_screen.dart';
import 'package:no_fine/ui/resoursese/app_colors.dart';

import 'package:no_fine/ui/sign_up/sign_up.dart';
import 'package:no_fine/ui/widgets/app_bar.dart';
import 'package:no_fine/ui/widgets/social_button.dart';
import 'package:no_fine/ui/widgets/submit_button.dart';
import 'package:no_fine/ui/widgets/text_fild.dart';

class SignInScreen extends StatefulWidget {
  static const String route = "/signIn";

  @override
  _SignInScreenState createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  SignInBloc _bloc;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  // NumberEditingController _phoneController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  String phone;
  String password;
  final controller = MaskedTextController(mask: "+7(000) 000-00-00");
  //static final FacebookLogin facebookSignIn = new FacebookLogin();

  @override
  void initState() {
    _bloc = BlocProvider.of<SignInBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: CustomAppBar(
        titleText: 'Войти',
        context: context,
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child:
            BlocConsumer<SignInBloc, SignInState>(listener: (context, state) {
          if (state is SignInFailureState) {
            _scaffoldKey.currentState.showSnackBar(
              new SnackBar(
                backgroundColor: Colors.red,
                duration: new Duration(seconds: 6),
                behavior: SnackBarBehavior.floating,
                elevation: 6.0,
                content: new Text(state.errorMessage),
              ),
            );
          }
        }, builder: (context, state) {
          if (state is SuccessSignInState) {
            WidgetsBinding.instance.addPostFrameCallback(
                (_) => Navigator.of(context).pushReplacementNamed(
                      BaseScreen.route,
                    ));
          }
          return _buildBodyWidget();
        }),
      ),
    );
  }

  Widget _buildBodyWidget() {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          MyTextField(
            //limitNum: 11,
            labelText: 'Телефон',
            controller: controller,
            keyboardTypeIsInt: true,
            fieldValidator: (String value) {
              if (value.length < 10) {
                return 'Номер телефона от 10 цыфр';
              }
              return null;
            },
            onSaved: (String value) {
              phone = value;
            },
          ),
          const SizedBox(height: 10.0),
          MyTextField(
            labelText: 'Пароль',
            controller: _passwordController,
            keyboardTypeIsInt: false,
            fieldValidator: (String value) {
              if (value.length < 9) {
                return 'Пароль от 9 цыфр';
              }
              return null;
            },
            onSaved: (String value) {
              password = value;
            },
          ),
          const SizedBox(height: 20.0),
          SubmitButton(
            onPressed: () => _onSingUpClick(),
            text: 'Войти',
            textColor: AppColors.white,
            buttonColor: AppColors.primary,
          ),
          const SizedBox(
            height: 16.0,
          ),
          SubmitButton(
            onPressed: () {
              Navigator.of(context).pushNamed(
                SignUpScreen.route,
              );
            },
            text: 'У Вас еще нет аккаунта?',
            textColor: AppColors.primary,
            buttonColor: AppColors.backgroundTransparent,
            borderColor: AppColors.primary,
          ),
          Expanded(
            child: const SizedBox(),
          ),
          SocialButton(
            onPressed: () async {
              _onLogInFaceBookClick();

            },
            text: 'Войти с помощью Facebook',
            textColor: AppColors.black,
            image: 'assets/icons/facebook.png',
          ),
          const SizedBox(
            height: 16.0,
          ),
          SocialButton(
            onPressed: () {
                _onLogInGoogleClick();
              // GoogleSignIn().signIn();
            },
            text: 'Войти с помощью Google',
            textColor: AppColors.black,
            image: 'assets/icons/google.png',
          ),
          const SizedBox(
            height: 30.0,
          ),
        ],
      ),
    );
  }

  void _onSingUpClick() {
    if (_formKey.currentState.validate()) {
      final String phone = controller.value.text;
      String phoneMask = controller.value.text
          .replaceAll(new RegExp(r'[^0-9]'), '');
      final String password = _passwordController.value.text;
      _bloc.add(SignInLoginEvent(phone: phoneMask, password: password));
    }

    /*   final String phone = _phoneController.value.text;
    final String password = _passwordController.value.text;
    _bloc.add(SignInLoginEvent(phone: phone, password: password));*/
  }

  void _onLogInGoogleClick() {
    _bloc.add(SignInLoginGoogleEvent());
  }
  void _onLogInFaceBookClick() {
    _bloc.add(SignInLoginFacebookEvent());
  }

}
