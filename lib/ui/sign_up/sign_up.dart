import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:no_fine/bloc/auth/sign_up_bloc/sign_up_bloc.dart';
import 'package:no_fine/bloc/auth/sign_up_bloc/sign_up_events.dart';
import 'package:no_fine/bloc/auth/sign_up_bloc/sign_up_state.dart';
import 'package:no_fine/ui/base_screen.dart';
import 'package:no_fine/ui/main_screen/main_screen.dart';
import 'package:no_fine/ui/resoursese/app_colors.dart';
import 'package:no_fine/ui/resoursese/app_styles.dart';
import 'package:no_fine/ui/sign_in/sign_in.dart';
import 'package:no_fine/ui/widgets/app_bar.dart';
import 'package:no_fine/ui/widgets/privacy_policy.dart';
import 'package:no_fine/ui/widgets/submit_button.dart';
import 'package:no_fine/ui/widgets/text_fild.dart';

class SignUpScreen extends StatefulWidget {
  static const String route = "/signUp";

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  SignUpBloc _bloc;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _nameController = TextEditingController();
  TextEditingController _surnameController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _confirm_passwordController = TextEditingController();

  String name;
  String surname;
  String phone;
  String email;
  String password;
  String confirmPassword;
  bool _isObscurePass = true;

  bool _isObscureConfirmPass = true;
  final controller = MaskedTextController(mask: "+7(000) 000-00-00");
  @override
  void initState() {
    _bloc = BlocProvider.of<SignUpBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: CustomAppBar(
        titleText: 'Создать аккаунт',
        context: context,
      ),
      body: Padding(
          padding: const EdgeInsets.all(16.0),
          child:
              BlocConsumer<SignUpBloc, SignUpState>(
                  listener: (context, state) {
                    if (state is SignUpFailureState) {
                      _scaffoldKey.currentState.showSnackBar(
                        new SnackBar(
                          backgroundColor: Colors.red,
                          duration: new Duration(seconds: 6),
                          behavior: SnackBarBehavior.floating,
                          elevation: 6.0,
                          content: new Text(""),
                        ),
                      );
                    }
                  },

                  builder: (context, state) {
            if (state is SuccessSignUpState) {
              WidgetsBinding.instance.addPostFrameCallback(
                  (_) => Navigator.of(context).pushReplacementNamed(
                    BaseScreen.route,
                      ));
            }
            return _buildBodyWidget();
          })),
    );
  }

  Widget _buildBodyWidget() {
    return SingleChildScrollView(
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            MyTextField(
              labelText: 'Имя',
              controller: _nameController,
              keyboardTypeIsInt: false,
              fieldValidator: (String value) {
                if (value.length < 3) {
                  return 'Имя от 3 букв';
                }
                return null;
              },
              onSaved: (String value) {
                name = value;
              },
            ),
            const SizedBox(height: 10.0),
            MyTextField(
              labelText: 'Фамилия',
              controller: _surnameController,
              keyboardTypeIsInt: false,
              fieldValidator: (String value) {
                if (value.length < 3) {
                  return 'Фамилия от 3 букв';
                }
                return null;
              },
              onSaved: (String value) {
                name = value;
              },
            ),
            const SizedBox(height: 10.0),
            MyTextField(
              labelText: 'Телефон',
              controller: controller,
              keyboardTypeIsInt: true,
              fieldValidator: (String value) {
                if (value.length < 10) {
                  return 'Номер телефона от 10 цыфр';
                }
                return null;
              },
              onSaved: (String value) {
                phone = value;
              },
            ),
            const SizedBox(height: 10.0),
            MyTextField(
              labelText: 'Email',
              controller: _emailController,
              keyboardTypeIsInt: false,
              fieldValidator: (String value) {
                Pattern pattern =
                    r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                RegExp regex = new RegExp(pattern);

                if (!regex.hasMatch(value)) {
                  return 'Неверный фомат e-mail';
                }
                return null;
              },
              onSaved: (String value) {
                email = value;
              },
            ),
            const SizedBox(height: 10.0),


            Container(
              //constraints: BoxConstraints(maxHeight: height),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6),
                //  color: AppColors.primary.withOpacity(0.05)
              ),
              child: Row(
                children: [
                  Expanded(
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(6),
                      child: Container(
                        child: TextFormField(
                          validator: (String value) {
                            if (value.length < 9) {
                              return 'Пароль от 9 цыфр';
                            }

                            return null;
                          },
                          onSaved: (String value) {
                            password = value;
                          },

                          cursorColor: AppColors.primary,
                          controller: _passwordController,
                          obscureText: _isObscurePass,
                          maxLines: 1,
                          style: AppStyles.Roboto500(color: AppColors.black),

                          decoration: InputDecoration(
                            suffixIcon: IconButton(
                                icon: Icon(
                                  _isObscurePass ?Icons.visibility_off  : Icons.visibility,color:AppColors.primary,),
                                onPressed: () {
                                  setState(() {
                                    _isObscurePass = !_isObscurePass;
                                  });
                                }),
                            fillColor: AppColors.primary.withOpacity(0.05),
                            filled: true,
                            errorStyle: AppStyles.Roboto400(color: Colors.redAccent,fontSize: 10),
                            alignLabelWithHint: false,
                            /*contentPadding:
                            EdgeInsets.symmetric(horizontal: 12, vertical: 7),*/
                           // hintText: hintText,
                            hintStyle: AppStyles.Roboto400(color: AppColors.black),
                            labelStyle: AppStyles.Roboto400(color: AppColors.black),
                            border: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            disabledBorder: InputBorder.none,
                            errorBorder: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            focusedErrorBorder: InputBorder.none,
                            labelText: 'Пароль',


                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),



            const SizedBox(height: 10.0),

            Container(
              //constraints: BoxConstraints(maxHeight: height),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6),
                //  color: AppColors.primary.withOpacity(0.05)
              ),
              child: Row(
                children: [
                  Expanded(
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(6),
                      child: Container(
                        child: TextFormField(
                          validator: (String value) {
                            if (!(value == _passwordController.text)) {
                              print(value);
                              return 'Пароли не совпадают';
                            }
                            return null;
                          },
                          onSaved: (String value) {
                            password = value;
                          },

                          cursorColor: AppColors.primary,
                          controller: _confirm_passwordController,
                          obscureText: _isObscureConfirmPass,
                          maxLines: 1,
                          style: AppStyles.Roboto500(color: AppColors.black),

                          decoration: InputDecoration(
                            suffixIcon: IconButton(
                                icon: Icon(
                                  _isObscureConfirmPass ?Icons.visibility_off  : Icons.visibility,color:AppColors.primary,),
                                onPressed: () {
                                  setState(() {
                                    _isObscureConfirmPass = !_isObscureConfirmPass;
                                  });
                                }),
                            fillColor: AppColors.primary.withOpacity(0.05),
                            filled: true,
                            errorStyle: AppStyles.Roboto400(color: Colors.redAccent,fontSize: 10),
                            alignLabelWithHint: false,
                            /*contentPadding:
                            EdgeInsets.symmetric(horizontal: 12, vertical: 7),*/
                            // hintText: hintText,
                            hintStyle: AppStyles.Roboto400(color: AppColors.black),
                            labelStyle: AppStyles.Roboto400(color: AppColors.black),
                            border: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            disabledBorder: InputBorder.none,
                            errorBorder: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            focusedErrorBorder: InputBorder.none,
                            labelText: 'Повторить пароль',


                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),







            const SizedBox(height: 20.0),
            SubmitButton(
              onPressed: () {
                _onSingUpClick();
              },
              text: 'Зарегистрироваться',
              textColor: AppColors.white,
              buttonColor: AppColors.primary,
            ),
            Flexible(
              fit: FlexFit.loose,
              child: SizedBox(height: 10,),
            ),
           // Expanded(child: const SizedBox(height: 40.0)),
            Center(child: PrivacyPolicy()),
           // Expanded(child: const SizedBox(height: 40.0)),
          Flexible(
            fit: FlexFit.loose,
            child: SizedBox(height: 40,),
          ),
            SubmitButton(
              onPressed: () {
                Navigator.of(context).pushNamed(
                  SignInScreen.route,
                );
              },
              text: 'У Вас уже есть аккаунт?',
              textColor: AppColors.primary,
              buttonColor: AppColors.backgroundTransparent,
              borderColor: AppColors.primary,
            ),
            const SizedBox(
              height: 30.0,
            ),
          ],
        ),
      ),
    );
  }

  void _onSingUpClick() {
    if (_formKey.currentState.validate()) {
      final String name = _nameController.value.text;
      final String surname = _surnameController.value.text;
      final String phone = _phoneController.value.text;
      final String email = _emailController.value.text;
      final String password = _passwordController.value.text;
      final String confirm_password = _confirm_passwordController.value.text;
      String phoneMask = controller.value.text
          .replaceAll(new RegExp(r'[^0-9]'), '');
      _bloc.add(SignUpRegisterEvent(
          name: name,
          surname: surname,
          phone: phoneMask,
          email: email,
          password: password,
          confirm_password: confirm_password));
    }

    /* if (_formKey.currentState.validate()) {
      //  final String phone = _phoneController.value.text;
      // final String password = _passwordController.value.text;
      print(name);
      print(surname);
      print(phone);
      print(confirmPassword);
      _bloc.add(SignUpRegisterEvent(
          name: name,
          surname: surname,
          phone: phone,
          email: email,
          password: password,
          confirm_password: confirmPassword));
    }*/
  }
}
