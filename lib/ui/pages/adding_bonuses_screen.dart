import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:no_fine/bloc/bonuses_bloc/bonuses_state.dart';
import 'package:no_fine/ui/resoursese/app_colors.dart';
import 'package:no_fine/ui/resoursese/app_styles.dart';
import 'package:no_fine/ui/widgets/app_bar.dart';
import 'package:no_fine/ui/widgets/custom_alert.dart';
import 'package:no_fine/ui/widgets/submit_button.dart';
import 'package:no_fine/ui/widgets/text_fild.dart';
import 'package:no_fine/bloc/bonuses_bloc/bonuses_bloc.dart';
import 'package:no_fine/bloc/bonuses_bloc/bonuses_event.dart';

class AddBonusesScreen extends StatefulWidget {
  static const String route = "/addBonuses";

  @override
  _AddBonusesScreenState createState() => _AddBonusesScreenState();
}

class _AddBonusesScreenState extends State<AddBonusesScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  BonusesBloc _bloc;
  TextEditingController _autoVin = TextEditingController();
  TextEditingController _autoNumber = TextEditingController();
  TextEditingController _autoLicense = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    _bloc = BlocProvider.of<BonusesBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: CustomAppBar(
          titleText: 'Добавление бонусов',
          context: context,
        ),
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Container(
                height: 316.0,
                width: double.infinity,
                decoration: BoxDecoration(
                    border: Border.all(color: AppColors.primary, width: 1),
                    borderRadius: BorderRadius.all(Radius.circular(6.0))),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 16.0,
                      ),

                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 60.0),
                        child: Text(
                          'Модерация заявки может длиться до двух месяцев ',
                          maxLines: 2,
                          style: AppStyles.Roboto400(color: AppColors.gray),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      const SizedBox(
                        height: 12.0,
                      ),
                      Container(
                        color: AppColors.primary.withOpacity(0.05),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 60.0),
                          child: Column(
                            children: [
                              const SizedBox(
                                height: 6,
                              ),
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Text('VIN',
                                    style: AppStyles.Roboto400(
                                        color: AppColors.gray)),
                              ),



                              Container(
                                height: 50,
                                child: MyTextField(
                                  //height: 40,
                                  keyboardTypeIsInt: false,
                                  controller: _autoVin,
                                  limitNum: 17,
                                ),
                              /*  TextFormField(
                                  validator: (String value) {
                                    if (value.length < 9) {
                                      return 'Пароль от 9 цыфр';
                                    }

                                    //return null;
                                  },
                                  *//*  onSaved: (String value) {
                                    password = value;
                                  },*//*
                                  maxLength: 4,
                                  cursorColor: AppColors.primary,
                                  controller: _autoVin,
                                  obscureText: false,
                                  maxLines: 1,
                                  style: AppStyles.Roboto400(
                                      color: AppColors.black),
                                  decoration: InputDecoration(
                                   *//* suffixIcon: IconButton(
                                        icon: Icon(
                                          _isObscurePass
                                              ? Icons.visibility_off
                                              : Icons.visibility,
                                          color: AppColors.primary,
                                        ),
                                        onPressed: () {
                                          setState(() {
                                            _isObscurePass = !_isObscurePass;
                                          });
                                        }),*//*
                                    fillColor:
                                    AppColors.primary.withOpacity(0.05),
                                    filled: true,
                                    errorStyle: AppStyles.Roboto400(
                                        color: Colors.redAccent, fontSize: 10),
                                    alignLabelWithHint: false,
                                    *//*contentPadding:
                            EdgeInsets.symmetric(horizontal: 12, vertical: 7),*//*
                                    // hintText: hintText,
                                    hintStyle: AppStyles.Roboto400(
                                        color: AppColors.black),
                                    labelStyle: AppStyles.Roboto400(
                                        color: AppColors.black),
                                    border: InputBorder.none,
                                    enabledBorder: InputBorder.none,
                                    disabledBorder: InputBorder.none,
                                    errorBorder: InputBorder.none,
                                    focusedBorder: InputBorder.none,
                                    focusedErrorBorder: InputBorder.none,
                                    labelText: 'Пароль',
                                  ),
                                ),*/
                              )
                            ],
                          ),
                        ),
                      ),
                      // Center(child: const SizedBox(height: 12.0,)),

                      Container(
                        color: AppColors.primary.withOpacity(0.05),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 60.0),
                          child: Column(
                            children: [
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Text('Номер автомобиля',
                                    style: AppStyles.Roboto400(
                                        color: AppColors.gray)),
                              ),
                              Container(
                                height: 50,
                                child: MyTextField(
                                  limitNum: 10,
                                  keyboardTypeIsInt: false,
                                  controller: _autoNumber,

                                ),
                              )
                            ],
                          ),
                        ),
                      ),

                      Container(
                        color: AppColors.primary.withOpacity(0.05),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 60.0),
                          child: Column(
                            children: [
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Text('Номер удостоверения',
                                    style: AppStyles.Roboto400(
                                        color: AppColors.gray)),
                              ),
                              Container(
                                height: 50,
                                child: MyTextField(
                                  limitNum: 10,
                                  keyboardTypeIsInt: false,
                                  controller: _autoLicense,

                                ),
                              ),
                              const SizedBox(
                                height: 20,
                              ),
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 16,
                      ),

                      /*     Container(
                        height: 30,
                          color: Colors.grey.withOpacity(0.5),
                          child: TextField())*/
                    ],
                  ),
                ),
              ),
            ),
            /*  const SizedBox(
              height: 36.0,
            ),*/
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: SubmitButton(
                onPressed: () {
                  _onAddBonuseClick();
                },
                text: 'Добавить',
                buttonColor: AppColors.primary,
                textColor: AppColors.white,
              ),
            ),
            Expanded(child: SizedBox()),
            BlocListener<BonusesBloc, BonusesState>(
              child: Container(),
              listener: (context, state) {
                if (state is BonusesAddSuccess) {
                  // Navigator.of(context).pop();
                 /* _scaffoldKey.currentState.showSnackBar(
                    new SnackBar(
                      backgroundColor: AppColors.primary,
                      duration: new Duration(seconds: 6),
                      behavior: SnackBarBehavior.floating,
                      elevation: 6.0,
                      content: new Text('Бонус успешно добавлен'),
                    ),
                  );*/

                  showDialog(
                      context: context,
                      builder: (BuildContext context) {

                        return CustomAlert(
                          SubmitButton(buttonColor: AppColors.primary,text: 'Хорошо',textColor:AppColors.white,onPressed: (){

                            Navigator.pop(context);
                            Navigator.pop(context);

                          },),
                          image: 'assets/images/success.png',
                          //title: 'Успех',
                          description: state.bonusesResponse==null?'Бонус успешно добавлен':state.bonusesResponse,

                        );
                      });

                }

                if (state is BonusesFailureState) {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return CustomAlert(
                          SubmitButton(buttonColor: AppColors.primary,text: 'Хорошо',textColor:AppColors.white,onPressed: (){

                            Navigator.pop(context);

                          },),
                          image: 'assets/images/error.png',
                        //  title: 'Неудача',
                          description: state.bonusesResponse,

                        );
                      });
                }
              },
            )
          ],
        ));
  }

  void _onAddBonuseClick() {
    if (_autoVin.value.text.isEmpty) {
      _scaffoldKey.currentState.showSnackBar(
        new SnackBar(
          backgroundColor: AppColors.red,
          duration: new Duration(seconds: 3),
          behavior: SnackBarBehavior.floating,
          elevation: 6.0,
          content: new Text('Заполните поле vin'),
        ),
      );
    } else if (_autoNumber.value.text.isEmpty) {
      _scaffoldKey.currentState.showSnackBar(
        new SnackBar(
          backgroundColor: AppColors.red,
          duration: new Duration(seconds: 3),
          behavior: SnackBarBehavior.floating,
          elevation: 6.0,
          content: new Text('Заполните поле номер автомобиля'),
        ),
      );
    } else if (_autoLicense.value.text.isEmpty) {
      _scaffoldKey.currentState.showSnackBar(
        new SnackBar(
          backgroundColor: AppColors.red,
          duration: new Duration(seconds: 3),
          behavior: SnackBarBehavior.floating,
          elevation: 6.0,
          content: new Text('Заполните поле номер удостоверения'),
        ),
      );
    } else if (_formKey.currentState.validate()) {
      final String vin = _autoVin.value.text;
      final String number = _autoNumber.value.text;
      final String license = _autoLicense.value.text;

      _bloc.add(AddBonuseEvent(
          autoVin: vin, autoNumber: number, autoLicense: license));
    }
  }
}
