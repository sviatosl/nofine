import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:no_fine/bloc/settings_bloc/settings_bloc.dart';
import 'package:no_fine/bloc/settings_bloc/settings_state.dart';
import 'package:no_fine/ui/resoursese/app_colors.dart';
import 'package:no_fine/ui/resoursese/app_styles.dart';
import 'package:no_fine/ui/widgets/app_bar.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:no_fine/bloc/settings_bloc/settings_event.dart';

class CoinIndexScreen extends StatefulWidget {
  static const String route = "/coinIndex";

  @override
  _CoinIndexScreenState createState() => _CoinIndexScreenState();
}

class _CoinIndexScreenState extends State<CoinIndexScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  // List<charts.Series<Coin, String>> _seriesData;

/*  _generateData() {
    var data1 = [
      new Coin(1980, '1', 30),
      new Coin(1980, '2', 40),
      new Coin(1980, '3', 10),
    ];
    var data2 = [
      new Coin(1985, '1', 100),
      new Coin(1980, '2', 150),
      new Coin(1985, '3', 80),
    ];
    var data3 = [
      new Coin(1985, '1', 200),
      new Coin(1980, '2', 300),
      new Coin(1985, '3', 180),
    ];


    _seriesData.add(
      charts.Series(
        domainFn: (Coin pollution, _) => pollution.x,
        measureFn: (Coin pollution, _) => pollution.quantity,
        id: '2017',
        data: data1,
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (Coin pollution, _) =>
            charts.ColorUtil.fromDartColor(AppColors.primary),
      ),
    );

    _seriesData.add(
      charts.Series(
        domainFn: (Coin pollution, _) => pollution.x,
        measureFn: (Coin pollution, _) => pollution.quantity,
        id: '2018',
        data: data2,
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (Coin pollution, _) =>
            charts.ColorUtil.fromDartColor(Color(0xff109618)),
      ),
    );

    _seriesData.add(
      charts.Series(
        domainFn: (Coin pollution, _) => pollution.x,
        measureFn: (Coin pollution, _) => pollution.quantity,
        id: '2019',
        data: data3,
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (Coin pollution, _) =>
            charts.ColorUtil.fromDartColor(Color(0xff00A97F)),

      ),
    );




  }*/
  SettingsBloc _bloc;

  @override
  void initState() {
    _bloc = BlocProvider.of<SettingsBloc>(context);
    _bloc.add(UpdateSettingsEvent());
    super.initState();
    //_seriesData = List<charts.Series<Coin, String>>();
    // _generateData();
  }

  @override
  Widget build(BuildContext context) {
    _bloc.add(UpdateSettingsEvent());
    return Scaffold(
        key: _scaffoldKey,
        appBar: CustomAppBar(
          titleText: 'Индекс монет',
          context: context,
        ),
        body:
            /* Column(
        children: [
        const  SizedBox(
            height: 30.0,
          ),
          Text(
            '1 монета = 250 руб',
            style: AppStyles.Roboto400(color: AppColors.primary),
          ),
          const  SizedBox(
            height: 30.0,
          ),
          Container(
           // color: Colors.blue,
            height: MediaQuery.of(context).size.height * 0.5,
            child: charts.BarChart(
              _seriesData,
              animate: true,
              barGroupingType: charts.BarGroupingType.grouped,
              //behaviors: [new charts.SeriesLegend()],
              animationDuration: Duration(seconds: 2),
            ),
          ),
          const SizedBox(
            height: 70.0,
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: SubmitButton(
              onPressed: () {},
              text: 'Перейти на биржу',
              buttonColor: AppColors.primary,
              textColor: AppColors.white,
            ),
          )
        ],
      ),*/
            BlocBuilder<SettingsBloc, SettingsState>(builder: (context, state) {
          if (state is SettingsLoadedState) {
            /*return Center(child: Text(state.settingsResponse.coinReferred));*/
            return Center(child: Text('Индекс монет = '+state.settingsResponse.coin,style: AppStyles.Roboto400(color:AppColors.primary),));
          }
          return Center(
              child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(AppColors.primary),
          ));
        }));
  }
}

/*
class Coin {
  String x;
  int y;
  int quantity;

  Coin(this.y, this.x, this.quantity);
}
*/
