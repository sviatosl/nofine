import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:no_fine/bloc/settings_bloc/settings_bloc.dart';
import 'package:no_fine/bloc/settings_bloc/settings_event.dart';
import 'package:no_fine/bloc/settings_bloc/settings_state.dart';
import 'package:no_fine/ui/resoursese/app_colors.dart';
import 'package:no_fine/ui/resoursese/app_styles.dart';
import 'package:no_fine/ui/widgets/app_bar.dart';
import 'package:no_fine/ui/widgets/custom_alert.dart';
import 'package:no_fine/ui/widgets/submit_button.dart';

class WriteUsScreen extends StatefulWidget {
  static const String route = "/writeUs";

  @override
  _WriteUsScreenState createState() => _WriteUsScreenState();
}

class _WriteUsScreenState extends State<WriteUsScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController _description = TextEditingController();
  SettingsBloc _bloc;

  @override
  void initState() {
    _bloc = BlocProvider.of<SettingsBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: CustomAppBar(
          titleText: 'Напишите нам',
          context: context,
        ),
        body: Column(
          children: [
            BlocListener<SettingsBloc, SettingsState>(
              listener: (context, state) {
                if (state is FitbackSuccessState) {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return CustomAlert(
                          SubmitButton(
                            buttonColor: AppColors.primary,
                            text: 'Закрыть',
                            textColor: AppColors.white,
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          ),
                          image: 'assets/images/success.png',
                          //title: 'Успех',
                          description: state.fitbackResponse,
                        );
                      });
                }
              },
              child: Container(),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Container(
                height: 160.0,
                decoration: BoxDecoration(
                    color: AppColors.primary.withOpacity(0.06),
                    borderRadius: BorderRadius.all(Radius.circular(6.0))),
                child: TextField(
                  //  validator: fieldValidator,

                  cursorColor: AppColors.primary,
                  controller: _description,

                  maxLines: 12,
                  style: AppStyles.Roboto500(color: AppColors.black),

                  decoration: InputDecoration(
                    fillColor: AppColors.backgroundTransparent,
                    filled: true,
                    errorStyle: AppStyles.Roboto400(
                        color: Colors.redAccent, fontSize: 10),
                    alignLabelWithHint: false,
                    /*contentPadding:
                                        EdgeInsets.symmetric(horizontal: 12, vertical: 7),*/
                    hintText: 'Пишите Ваше сообщение здесь...',
                    hintStyle: AppStyles.Roboto400(color: AppColors.black.withOpacity(0.6)),
                    labelStyle: AppStyles.Roboto400(color: AppColors.black),
                    border: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    focusedErrorBorder: InputBorder.none,
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: SubmitButton(
                text: 'Отправить',
                buttonColor: AppColors.primary,
                textColor: AppColors.white,
                onPressed: () async {
                  if(_description.value.text.isNotEmpty){
                  final String description = _description.value.text;
                  _bloc.add(AddFitbackEvent(description: description));}
                },
              ),
            )
          ],
        ));
  }
}
