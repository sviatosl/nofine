import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:no_fine/bloc/bonuses_bloc/bonuses_bloc.dart';
import 'package:no_fine/bloc/bonuses_bloc/bonuses_event.dart';
import 'package:no_fine/bloc/bonuses_bloc/bonuses_state.dart';
import 'package:no_fine/bloc/news_bloc/news_bloc.dart';
import 'package:no_fine/bloc/news_bloc/news_event.dart';
import 'package:no_fine/bloc/news_bloc/news_state.dart';
import 'package:no_fine/bloc/settings_bloc/settings_bloc.dart';
import 'package:no_fine/bloc/settings_bloc/settings_event.dart';
import 'package:no_fine/bloc/settings_bloc/settings_state.dart';
import 'package:no_fine/data/api_models/bonuse_response.dart';
import 'package:no_fine/data/api_models/news_response.dart';
import 'package:no_fine/ui/pages/news_detail_screen.dart';
import 'package:no_fine/ui/resoursese/app_colors.dart';
import 'package:no_fine/ui/resoursese/app_styles.dart';
import 'package:no_fine/ui/widgets/app_bar.dart';
import 'package:no_fine/ui/widgets/submit_button.dart';

import 'adding_bonuses_screen.dart';

class NewsScreen extends StatefulWidget {
  static const String route = "/newsScreen";

  @override
  _NewsScreenState createState() => _NewsScreenState();
}

class _NewsScreenState extends State<NewsScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  NewsBloc _blocNews;
  SettingsBloc _blocSettings;

  @override
  void initState() {
    _blocNews = BlocProvider.of<NewsBloc>(context);
    _blocSettings = BlocProvider.of<SettingsBloc>(context);
    _blocNews.add(GetAllNewsEvent());
    _blocSettings.add(UpdateSettingsEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
     /*      appBar: CustomAppBar(
          titleText: 'Главная',
          context: context,
          isExistClouseIcon: false,
        ),*/
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: SingleChildScrollView(
            child: Column(
              children: [
                const SizedBox(height: 50),
                Container(
                  height: 184,
                  decoration: BoxDecoration(
                      border: Border.all(color: AppColors.primary, width: 3),
                      borderRadius: BorderRadius.all(Radius.circular(6.0))),
                  child: Container(
                    margin: const EdgeInsets.symmetric(
                        horizontal: 14.0, vertical: 20.0),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Text(
                              'AВТО',
                              style: AppStyles.Roboto700(
                                  fontSize: 18, color: AppColors.blackk),
                            ),
                            Text(
                              'ТОКЕН',
                              style: AppStyles.Roboto700(
                                  fontSize: 18, color: AppColors.primary),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        Text(
                          'Это внутренняя монета проекта, посредством которой Вы будете получать компенсации штрафов.',
                          style: AppStyles.Roboto400(color: AppColors.blackk),
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        /*    Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              'Биржа.AutoToken = ',
                              style: AppStyles.Roboto500(color: AppColors.blackk),
                            )),
                        const SizedBox(
                          height: 16,
                        ),
                        Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              'Внутренний Индекс = ',
                              style: AppStyles.Roboto500(color: AppColors.blackk),
                            )),*/

                        BlocBuilder<SettingsBloc, SettingsState>(
                            builder: (context, state) {
                              if (state is SettingsLoadedState) {
                                return Column(
                                  children: [

                                    Align(
                                        alignment: Alignment.centerLeft,
                                        child: Row(
                                          children: [
                                            Text(
                                              'Биржа.AutoToken = ',
                                              style: AppStyles.Roboto500(
                                                  color: AppColors.blackk),
                                            ),
                                            Text(
                                              state.settingsResponse.wavesCoin,
                                              style: AppStyles.Roboto500(
                                                  color: AppColors.primary),
                                            ),
                                          ],
                                        )),

                                    const SizedBox(
                                      height: 10,
                                    ),
                                    Align(
                                        alignment: Alignment.centerLeft,
                                        child: Row(
                                          children: [
                                            Text(
                                              'Внутренний Индекс = ',
                                              style: AppStyles.Roboto500(
                                                  color: AppColors.blackk),
                                            ),
                                            Text(
                                              state.settingsResponse.indexCoin,
                                              style: AppStyles.Roboto500(
                                                  color: AppColors.primary),
                                            )
                                          ],
                                        )),

                                  ],
                                );
                              }

                              /* return Center(
                              child: CircularProgressIndicator(
                                valueColor: AlwaysStoppedAnimation<Color>(AppColors.primary),
                              ));*/
                              return Container();
                            }),
                      ],
                    ),
                  ),
                ),

                const SizedBox(
                  height: 30,
                ),
                Padding(
                  padding: const EdgeInsets.only(left:16.0),
                  child: Align(
                      alignment: Alignment.centerLeft,

                      child: Text('Новости',style: AppStyles.Roboto500(color: AppColors.primary,fontSize: 20),)),
                ),

                BlocBuilder<NewsBloc, NewsState>(builder: (context, state) {
                  if (state is NewsLoadedState) {
                    return newsList(state.newsResponse);
                  }

                  return Center(
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(
                            AppColors.primary),
                      ));
                }),


              ],
            ),
          ),
        ));
  }

  newsList(List<NewsResponse> newsList) =>
      Container(
       // color: Colors.amberAccent,
        child: ListView.builder(
           // scrollDirection: Axis.vertical,
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: newsList.length,
            itemBuilder: (BuildContext context, int index) {
              return Padding(
                padding: const EdgeInsets.only(bottom:16.0),
                child: Container(
                    height: 160,
                    decoration: BoxDecoration(
                        border: Border.all(color: AppColors.primary, width: 1),
                        borderRadius: BorderRadius.all(Radius.circular(6.0))),
                    child: Container(
                      margin: const EdgeInsets.all(14.0),
                      child: Column(
                        children: [
                          Text(newsList[index].subject,
                              style: AppStyles.Roboto500(
                                  color: AppColors.blackk,
                                  fontWeight: FontWeight.w700
                              )),
                          const SizedBox(
                            height: 12,
                          ),
                          Expanded(
                            child: Text(
                              newsList[index].text,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 4,
                              style: AppStyles.Roboto400(color: AppColors.blackk),
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (_) =>
                                        NewsDetailScreen(
                                          title: newsList[index].subject,
                                          text: newsList[index].text,
                                          data: DateFormat('dd.MM.yyyy')
                                              .format(newsList[index].createdAt),
                                        ),
                                  ));
                            },
                            child: Container(
                              height: 30,
                              child: Row(
                                children: [
                                  Text(
                                    DateFormat('dd.MM.yyyy')
                                        .format(newsList[index].createdAt),
                                    style: AppStyles.Roboto500(
                                        color: AppColors.blackk,
                                        fontSize: 12,
                                        fontWeight: FontWeight.w700),
                                  ),
                                  Expanded(child: SizedBox()),
                                  Padding(
                                    padding: const EdgeInsets.only(right: 16.0),
                                    child: Image.asset(
                                      'assets/icons/right_arrow.png',
                                      scale: 2,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    )),
              );
            }),
      );
}
