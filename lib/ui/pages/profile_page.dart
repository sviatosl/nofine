import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_exif_rotation/flutter_exif_rotation.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';

import 'package:no_fine/bloc/auth/main_screen_bloc/main_screen_bloc.dart';
import 'package:no_fine/bloc/auth/main_screen_bloc/main_screen_event.dart';
import 'package:no_fine/bloc/auth/main_screen_bloc/main_screen_state.dart';
import 'package:no_fine/ui/resoursese/app_colors.dart';
import 'package:no_fine/ui/resoursese/app_styles.dart';
import 'package:no_fine/ui/widgets/app_bar.dart';
import 'package:no_fine/ui/widgets/profile_bottom_sheet.dart';
import 'package:no_fine/ui/widgets/social_button.dart';
import 'package:no_fine/ui/widgets/submit_button.dart';

class ProfileScreen extends StatefulWidget {
  static const String route = "/myprofile";
 final bool isVisibleAppBar;

  const ProfileScreen({Key key, this.isVisibleAppBar}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

//  BonusesBloc _bloc;
  MainScreenBloc _bloc;
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _confirm_passwordController = TextEditingController();
  bool _isObscurePass = true;

  bool _isObscureConfirmPass = true;

  TextEditingController _nameController = TextEditingController();
  TextEditingController _surnameController = TextEditingController();

  TextEditingController _emailController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();
  final controller = MaskedTextController(mask: "+7(000) 000-00-00");
  File imageFile;
  final picker = ImagePicker();
  static const BASE_URL = "https://api-fines.profdecor.com.ua";
  static const String apiEndpoint = BASE_URL + "/api/v1/";

  @override
  void initState() {
    _bloc = BlocProvider.of<MainScreenBloc>(context);
    _bloc.add(GetCurrentUserEvent());
    super.initState();
  }

  @override
  void dispose() {
    _passwordController.dispose();
    _confirm_passwordController.dispose();
    super.dispose();
  }

  Future getImageFromGallery() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        imageFile = File(pickedFile.path);
        _bloc.add(UploadProfileImageEvent(imageFile));

        //  _upload(imageFile);

      } else {
        print('No image selected.');
      }
    });
  }

  Future getImageFromCamera() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);
    File rotatedImage =
        await FlutterExifRotation.rotateAndSaveImage(path: pickedFile.path);
    setState(() {
      if (pickedFile != null) {
        imageFile = File(pickedFile.path);

        _bloc.add(UploadProfileImageEvent(rotatedImage));
        // _upload(imageFile);
      } else {
        print('No image selected.');
      }
    });
  }

/*
  void _upload(File file) async {
    String fileName = file.path.split('/').last;

    FormData data = FormData.fromMap({
      "image": await MultipartFile.fromFile(
        file.path,
        filename: fileName,
      ),
    });

    Dio dio = new Dio();

    await dio.post(apiEndpoint + "users/avatar",
        options: Options(headers: {
          "Authorization":
              'vAntDh9UTxZH3n7MsJATqMtI5Wwp0WflEpLN2fCKVsKf2yMoGAqFNfSKYCPa'
        }),
        data: data);
  }
*/

  @override
  Widget build(BuildContext context) {
    return /*Scaffold(
        key: _scaffoldKey,
        appBar: CustomAppBar(
          titleText: 'Профиль',
          context: context,
        ));*/

        BlocConsumer<MainScreenBloc, MainScreenState>(
            listener: (context, state) {
      if (state is SucessffulUpdatePassState) {
        _scaffoldKey.currentState.showSnackBar(
          new SnackBar(
            backgroundColor: AppColors.primary,
            duration: new Duration(seconds: 6),
            behavior: SnackBarBehavior.floating,
            elevation: 6.0,
            //content: new Text('Можете добавить еще штраф.'),
            content: new Text('Пароль обновлен!'),
          ),
        );
      }

      if (state is UpdatePassFailureState) {
        _scaffoldKey.currentState.showSnackBar(
          new SnackBar(
            backgroundColor: AppColors.red,
            duration: new Duration(seconds: 6),
            behavior: SnackBarBehavior.floating,
            elevation: 6.0,
            content: new Text(
              state.failureResponse,
            ),
          ),
        );
      }

      if (state is SucessffulUpdateUserState) {
        _scaffoldKey.currentState.showSnackBar(
          new SnackBar(
            backgroundColor: AppColors.primary,
            duration: new Duration(seconds: 6),
            behavior: SnackBarBehavior.floating,
            elevation: 6.0,
            //content: new Text('Можете добавить еще штраф.'),
            content: new Text('Данные изменены успешно!'),
          ),
        );
      }
    }, builder: (context, state) {
      if (state is GetUserSucessfful) {
        return Scaffold(
          key: _scaffoldKey,
          appBar:CustomAppBar(
            titleText:
                state.userResponse.name + " " + state.userResponse.surname,
            context: context,
            isExistClouseIcon: widget.isVisibleAppBar,
          ),
          body: Center(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: GestureDetector(
                      onTap: () async {
                        _scaffoldKey.currentState.showBottomSheet(
                            (context) => Container(
                                color: AppColors.backgroundTransparent,
                                child: ClipRRect(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(20),
                                      topRight: Radius.circular(20)),
                                  child: Container(
                                      color: AppColors.white,
                                      height: 400,
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 20),
                                      child: Column(children: <Widget>[
                                        Container(
                                          height: 4,
                                          width: 100,
                                          margin: EdgeInsets.only(
                                              bottom: 19.5, top: 5),
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(99),
                                              color: AppColors.primary),
                                        ),
                                        const SizedBox(
                                          height: 16.0,
                                        ),
                                        SubmitButton(
                                          onPressed: () async {
                                            await getImageFromGallery();
                                            Navigator.pop(context);
                                            _bloc.add(GetCurrentUserEvent());
                                          },
                                          text: 'ГАЛЕРЕЯ',
                                          textColor: AppColors.white,
                                          buttonColor: AppColors.primary,
                                          borderColor: AppColors.primary,
                                        ),
                                        const SizedBox(
                                          height: 16.0,
                                        ),
                                        SocialButton(
                                          onPressed: () async {
                                            await getImageFromCamera();
                                            Navigator.pop(context);
                                            _bloc.add(GetCurrentUserEvent());
                                          },
                                          text: 'СДЕЛАТЬ ФОТО',
                                          textColor: AppColors.primary,
                                        ),
                                        const SizedBox(
                                          height: 46.0,
                                        ),
                                        SocialButton(
                                          onPressed: () {
                                            Navigator.pop(context);
                                          },
                                          text: 'ОТМЕНИТЬ',
                                          textColor: AppColors.primary,
                                        ),
                                        const SizedBox(
                                          height: 30.0,
                                        ),
                                      ])),
                                )),
                            backgroundColor: AppColors.backgroundTransparent);

                        _bloc.add(GetCurrentUserEvent());
                      },
                      child: Center(
                        child: state.userResponse.image != null
                            ? ClipOval(
                                child: Image.network(
                                'https://api-fines.profdecor.com.ua/public/upload/' +
                                    state.userResponse.image,
                                fit: BoxFit.cover,
                                height: 66,
                                width: 66,
                              ))
                            : Image.asset('assets/icons/profile_icon.png',
                                scale: 2),
                      ),
                    ),
                  ),
                  Text(
                    'Зарегистрирован в системе: ' +
                        DateFormat('dd.MM.yyyy')
                            .format(state.userResponse.createdAt),
                    style: AppStyles.Roboto500(
                        fontSize: 12, color: AppColors.gray),
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  profileItem(
                      'assets/icons/copy_icon.png',
                      'Реферальная ссылка',
                     'https://shtrafam-net.ru/register/?ref=' + state.userResponse.affiliateId),
                  const SizedBox(
                    height: 12,
                  ),
                  profileItem('assets/icons/status_icon.png', 'Статус',
                      _getType(state.userResponse.packageId)),
                  const SizedBox(
                    height: 12,
                  ),
                  profileItem('assets/icons/coin_prof_icon.png', 'Баланс',
                      state.userResponse.balance),
                  const SizedBox(
                    height: 12,
                  ),
                  /*     profileItem('assets/icons/coin_prof_icon.png',
                      'Загружено штрафов', '?'),*/
                  const SizedBox(
                    height: 12,
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: Text(
                        'БАЗОВАЯ ИНФОРМАЦИЯ',
                        style: AppStyles.Roboto500(
                            color: AppColors.gray, fontSize: 12),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  profileUpdatedItem('assets/icons/profile.png', 'Имя',
                      state.userResponse.name, _nameController),
                  const SizedBox(
                    height: 12,
                  ),
                  profileUpdatedItem(
                      'assets/icons/profile.png',
                      'Фамилия',
                      state.userResponse.surname,
                      _surnameController),
                  const SizedBox(
                    height: 12,
                  ),
                  /*    profileItem('assets/icons/info_icon.png', 'Информация',
                      'Вот немного обо мне...'),
                  const SizedBox(
                    height: 12,
                  ),*/
                  profileUpdatedItem('assets/icons/messages_icon.png', 'Email',
                      state.userResponse.email, _emailController),
                  /* const SizedBox(
                    height: 12,
                  ),
                  profileUpdatedItem(
                      'assets/icons/phone_icon.png',
                      'Номер телефона',
                      '+' + state.userResponse.phone,
                      _phoneController),*/
                  const SizedBox(
                    height: 12,
                  ),
                  /*   profileItem('assets/icons/sex_icon.png', 'Пол', 'пол'),
                  const SizedBox(
                    height: 12,
                  ),*/
                  const SizedBox(
                    height: 12,
                  ),
                  profileUpdatedItem(
                      'assets/icons/phone_icon.png',
                      'Номер телефона',
                     (state.userResponse.phone ?? 'Нет'),
                      controller..text = state.userResponse.phone??''),

                  /*  TextField(
                    controller: controller..text = state.userResponse.phone,
                    keyboardType: TextInputType.number,
                    autofocus: true,
                  ),
*/
                  const SizedBox(
                    height: 12,
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: SubmitButton(
                      text: 'Изменить данные',
                      buttonColor: AppColors.primary,
                      textColor: AppColors.white,
                      onPressed: () {
                        String name = _nameController.value.text;
                        String surname = _surnameController.value.text;
                        String email = _emailController.value.text;
                        String phone = controller.value.text;
                        String phoneMask = controller.value.text
                            .replaceAll(new RegExp(r'[^0-9]'), '');
                        //    if(list[0])

                        _bloc.add(UpdateUserEvent(
                            name: name,
                            surname: surname,
                            email: email,
                            phone: phoneMask));
                        _bloc.add(GetCurrentUserEvent());

                        /*    if(_passwordController.value.text.length>=9 && _confirm_passwordController.value.text.length>=9) {
                            _bloc.add(UpdatePasswordUserEvent(
                              pass: _passwordController.value.text,
                              confirmPass: _confirm_passwordController.value
                                  .text,
                            ));
                            _bloc.add(GetCurrentUserEvent());
                            //_bloc.add(GetCurrentUserEvent());
                          }*/
                      },
                    ),
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: Text(
                        'МОЙ КОШЕЛЕЛЕК',
                        style: AppStyles.Roboto500(
                            color: AppColors.gray, fontSize: 12),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  profileItem('assets/icons/copy_icon.png', 'Номер кошелька',
                      state.userResponse.bill),
                  const SizedBox(
                    height: 12,
                  ),
                  profileItem('assets/icons/copy_icon.png', 'Приватный ключ',
                      state.userResponse.privateKey),
                  const SizedBox(
                    height: 12,
                  ),

/*
                  profileItem('assets/icons/copy_icon.png', 'Ключевая фраза',
                      state.userResponse.seedBill),*/

                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: Container(
                      height: 55,
                      decoration: new BoxDecoration(
                        color: AppColors.primary.withOpacity(0.06),
                        borderRadius: BorderRadius.all(Radius.circular(6.0)),
                      ),

                      /*child:Text(state.userResponse.seedBill,
                   style:
                   AppStyles.Roboto400(color: AppColors.blak_prof)),*/
                      child: Row(
                        children: [
                          const SizedBox(
                            width: 8,
                          ),
                          Container(
                            width: 34,
                            child: GestureDetector(
                                onTap: () {
                                  Clipboard.setData(new ClipboardData(
                                      text: state.userResponse.seedBill));
                                  Fluttertoast.showToast(
                                    msg: "Скопировано",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.BOTTOM,
                                    timeInSecForIosWeb: 1,
                                    backgroundColor:
                                        AppColors.primary.withOpacity(0.8),
                                    textColor: Colors.white,
                                    fontSize: 12.0,
                                  );
                                },
                                child: Image.asset('assets/icons/copy_icon.png',
                                    scale: 2)),
                          ),
                          const SizedBox(
                            width: 12,
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  'Ключевая фраза',
                                  style: AppStyles.Roboto400(
                                      color: AppColors.gray),
                                ),
                                Text(state.userResponse.seedBill,
                                    style: AppStyles.Roboto400(
                                        color: AppColors.blak_prof)),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: Text(
                        'ИЗМЕНИТЬ ПАРОЛЬ',
                        style: AppStyles.Roboto500(
                            color: AppColors.gray, fontSize: 12),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: Container(
                      height: 50,
                      //constraints: BoxConstraints(maxHeight: height),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        //  color: AppColors.primary.withOpacity(0.05)
                      ),
                      child: Row(
                        children: [
                          Expanded(
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(6),
                              child: Container(
                                child: TextFormField(
                                  validator: (String value) {
                                    if (value.length < 9) {
                                      return 'Пароль от 9 цыфр';
                                    }

                                    //return null;
                                  },
                                  /*  onSaved: (String value) {
                                    password = value;
                                  },*/
                                  cursorColor: AppColors.primary,
                                  controller: _passwordController,
                                  obscureText: _isObscurePass,
                                  maxLines: 1,
                                  style: AppStyles.Roboto400(
                                      color: AppColors.black),
                                  decoration: InputDecoration(
                                    suffixIcon: IconButton(
                                        icon: Icon(
                                          _isObscurePass
                                              ? Icons.visibility_off
                                              : Icons.visibility,
                                          color: AppColors.primary,
                                        ),
                                        onPressed: () {
                                          setState(() {
                                            _isObscurePass = !_isObscurePass;
                                          });
                                        }),
                                    fillColor:
                                        AppColors.primary.withOpacity(0.05),
                                    filled: true,
                                    errorStyle: AppStyles.Roboto400(
                                        color: Colors.redAccent, fontSize: 10),
                                    alignLabelWithHint: false,
                                    /*contentPadding:
                            EdgeInsets.symmetric(horizontal: 12, vertical: 7),*/
                                    // hintText: hintText,
                                    hintStyle: AppStyles.Roboto400(
                                        color: AppColors.black),
                                    labelStyle: AppStyles.Roboto400(
                                        color: AppColors.black),
                                    border: InputBorder.none,
                                    enabledBorder: InputBorder.none,
                                    disabledBorder: InputBorder.none,
                                    errorBorder: InputBorder.none,
                                    focusedBorder: InputBorder.none,
                                    focusedErrorBorder: InputBorder.none,
                                    labelText: 'Пароль',
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: Container(
                      height: 50,
                      //constraints: BoxConstraints(maxHeight: height),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        //  color: AppColors.primary.withOpacity(0.05)
                      ),
                      child: Row(
                        children: [
                          Expanded(
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(6),
                              child: Container(
                                child: TextFormField(
                                  validator: (String value) {
                                    if (!(value == _passwordController.text)) {
                                      print(value);
                                      return 'Пароли не совпадают';
                                    }
                                    return null;
                                  },
                                  cursorColor: AppColors.primary,
                                  controller: _confirm_passwordController,
                                  obscureText: _isObscureConfirmPass,
                                  maxLines: 1,
                                  style: AppStyles.Roboto400(
                                      color: AppColors.black),
                                  decoration: InputDecoration(
                                    suffixIcon: IconButton(
                                        icon: Icon(
                                          _isObscureConfirmPass
                                              ? Icons.visibility_off
                                              : Icons.visibility,
                                          color: AppColors.primary,
                                        ),
                                        onPressed: () {
                                          setState(() {
                                            _isObscureConfirmPass =
                                                !_isObscureConfirmPass;
                                          });
                                        }),
                                    fillColor:
                                        AppColors.primary.withOpacity(0.05),
                                    filled: true,
                                    errorStyle: AppStyles.Roboto400(
                                        color: Colors.redAccent, fontSize: 10),

                                    alignLabelWithHint: false,
                                    /*contentPadding:
                            EdgeInsets.symmetric(horizontal: 12, vertical: 7),*/
                                    // hintText: hintText,
                                    hintStyle: AppStyles.Roboto400(
                                        color: AppColors.black),
                                    labelStyle: AppStyles.Roboto400(
                                        color: AppColors.black),
                                    border: InputBorder.none,
                                    enabledBorder: InputBorder.none,
                                    disabledBorder: InputBorder.none,
                                    errorBorder: InputBorder.none,
                                    focusedBorder: InputBorder.none,
                                    focusedErrorBorder: InputBorder.none,
                                    labelText: 'Повторить пароль',
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 28,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: SubmitButton(
                      text: 'Изменить пароль',
                      buttonColor: AppColors.primary,
                      textColor: AppColors.white,
                      onPressed: () async {
                        if (_passwordController.value.text.length < 9) {
                          _scaffoldKey.currentState.showSnackBar(
                            new SnackBar(
                              backgroundColor: AppColors.red,
                              duration: new Duration(seconds: 3),
                              behavior: SnackBarBehavior.floating,
                              elevation: 6.0,
                              content: new Text('Пароль от 9 цыфр!'),
                            ),
                          );
                        } else if (_passwordController.value.text !=
                            _confirm_passwordController.value.text) {
                          _scaffoldKey.currentState.showSnackBar(
                            new SnackBar(
                              backgroundColor: AppColors.red,
                              duration: new Duration(seconds: 3),
                              behavior: SnackBarBehavior.floating,
                              elevation: 6.0,
                              content: new Text('Пароли не совпадают!'),
                            ),
                          );
                        } else if (_passwordController.value.text.length >= 9 &&
                            _confirm_passwordController.value.text.length >=
                                9) {
                          _bloc.add(UpdatePasswordUserEvent(
                            pass: _passwordController.value.text,
                            confirmPass: _confirm_passwordController.value.text,
                          ));
                          //  _bloc.add(GetCurrentUserEvent());

                          //_bloc.add(GetCurrentUserEvent());
                        }
                      },
                    ),
                  ),
                  const SizedBox(
                    height: 28,
                  ),

                  /* TextField(
                    controller: controller..text = state.userResponse.phone,
                    keyboardType: TextInputType.number,
                    autofocus: true,
                  ),

                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: SubmitButton(
                      text: 'print',
                      buttonColor: AppColors.primary,
                      textColor: AppColors.white,
                      onPressed: () {
                      String  res =controller.value.text.replaceAll(new RegExp(r'[^0-9]'),'');

                      print(res);
                      },
                    ),
                  ),*/
                ],
              ),
            ),
          ),
        );
      }

      return Container(
        height: 1,
        width: 1,
        color: Colors.white,
        child: Center(
            child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(AppColors.primary),
        )),
      );
    });
  }

  String _getType(int type) {
    switch (type) {
      case 1:
        return 'Бронзовый';
        break;
      case 2:
        return 'Серебряный';
        break;
      case 3:
        return 'Золотой';
        break;
      case 3:
        return 'Платиновый';
        break;
    }
  }

  Widget profileItem(String icon, String firstText, String secondText) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Container(
        height: 40,
        decoration: new BoxDecoration(
          color: AppColors.primary.withOpacity(0.06),
          borderRadius: BorderRadius.all(Radius.circular(6.0)),
        ),
        child: Row(
          children: [
            const SizedBox(
              width: 8,
            ),
            Container(
              width: 34,
              child: GestureDetector(
                  onTap: () {
                    Clipboard.setData(new ClipboardData(text: secondText));
                    Fluttertoast.showToast(
                      msg: "Скопировано",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      timeInSecForIosWeb: 1,
                      backgroundColor: AppColors.primary.withOpacity(0.8),
                      textColor: Colors.white,
                      fontSize: 12.0,
                    );
                  },
                  child: Image.asset(icon, scale: 2)),
            ),
            const SizedBox(
              width: 12,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    firstText,
                    style: AppStyles.Roboto400(color: AppColors.gray),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 3.0),
                    child: Text(secondText,
                        overflow: TextOverflow.ellipsis,
                        style: AppStyles.Roboto400(color: AppColors.blak_prof)),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget profileUpdatedItem(String icon, String firstText, String secondText,
      TextEditingController edit) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Container(
        height: 40,
        decoration: new BoxDecoration(
          color: AppColors.primary.withOpacity(0.06),
          borderRadius: BorderRadius.all(Radius.circular(6.0)),
        ),
        child: Row(
          children: [
            const SizedBox(
              width: 8,
            ),
            Container(
              width: 34,
              child: GestureDetector(
                  onTap: () {
                    Clipboard.setData(new ClipboardData(text: secondText));
                    Fluttertoast.showToast(
                      msg: "Скопировано",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      timeInSecForIosWeb: 1,
                      backgroundColor: AppColors.primary.withOpacity(0.8),
                      textColor: Colors.white,
                      fontSize: 12.0,
                    );
                  },
                  child: Image.asset(icon, scale: 2)),
            ),
            const SizedBox(
              width: 12,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    height: 16,
                    child: Text(
                      firstText,
                      style: AppStyles.Roboto400(color: AppColors.gray),
                    ),
                  ),
                  Container(
                    height: 16,
                    child: TextFormField(
                      controller: edit..text = secondText,

                      // initialValue: secondText,
                      cursorColor: AppColors.primary,
                      style: AppStyles.Roboto400(color: AppColors.blak_prof),
                      decoration: InputDecoration(
                        // fillColor: AppColors.primary.withOpacity(0.05),
                        // fillColor: AppColors.primary.withOpacity(0.05),
                        // filled: true,

                        alignLabelWithHint: false,
                        /*contentPadding:
                        EdgeInsets.symmetric(horizontal: 12, vertical: 7),*/
                        // hintText: hintText,
                        hintStyle: AppStyles.Roboto400(color: AppColors.black),
                        labelStyle: AppStyles.Roboto400(color: AppColors.black),
                        border: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        focusedErrorBorder: InputBorder.none,
                        // hintText: secondText
                        //  labelText: labelText,
                      ),
                      //initialValue: '111',
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
