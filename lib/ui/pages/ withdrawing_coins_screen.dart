import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:no_fine/bloc/auth/translations_bloc/translations_bloc.dart';
import 'package:no_fine/bloc/auth/translations_bloc/translations_event.dart';
import 'package:no_fine/bloc/auth/translations_bloc/translations_state.dart';
import 'package:no_fine/ui/resoursese/app_colors.dart';
import 'package:no_fine/ui/resoursese/app_styles.dart';
import 'package:no_fine/ui/widgets/app_bar.dart';
import 'package:no_fine/ui/widgets/custom_alert.dart';
import 'package:no_fine/ui/widgets/submit_button.dart';
import 'package:shared_preferences/shared_preferences.dart';


class WithdrawingCoinsScreen extends StatefulWidget {
  static const String route = "/withdrawingConis";

  @override
  _WithdrawingCoinsScreenState createState() => _WithdrawingCoinsScreenState();
}

class _WithdrawingCoinsScreenState extends State<WithdrawingCoinsScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  TransferBloc _bloc;
  TextEditingController _coins = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    _bloc = BlocProvider.of<TransferBloc>(context);
    _bloc.add(UpdateTransScreenEvent());
    super.initState();
  }
  _getPackageId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt('packageId');
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: CustomAppBar(
          titleText: 'Вывод монет на биржу',
          context: context,
        ),
        body:

        FutureBuilder(
            future: _getPackageId(),
            builder: (context, AsyncSnapshot snapshot) {
              if (snapshot.data ==1) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'Вам нужно преобрести пакет,чтобы иметь возможность делать переводы',textAlign: TextAlign.center, style: AppStyles.Roboto400(color: AppColors.gray),),
                );
              } else {
               return BlocConsumer<TransferBloc, TransferState>(

                  listener: (context, state) {
                    if (state is TransferToStockExchangeSucessfful) {

                      showDialog(
                          context: context,
                          builder: (BuildContext context) {

                            return CustomAlert(
                              SubmitButton(buttonColor: AppColors.primary,text: 'Хорошо',textColor:AppColors.white,onPressed: (){

                                Navigator.pop(context);
                                Navigator.pop(context);

                              },),
                              image: 'assets/images/success.png',
                              // title: 'Успех',
                              description: state.transferResponse,
                            );
                          });


                    }

                    if (state is TransferFailureState) {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return CustomAlert(
                              SubmitButton(buttonColor: AppColors.primary,text: 'Хорошо',textColor:AppColors.white,onPressed: (){

                                Navigator.pop(context);

                              },),
                              image: 'assets/images/error.png',
                              //title: 'Неудача',
                              description: state.transferResponse,

                            );
                          });
                    }


                  },

                  builder: (context, state) {
                    if (state is TransferLoadedState) {
                      return Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: Container(
                              height: 200.0,
                              width: double.infinity,
                              decoration: BoxDecoration(
                                  border:
                                  Border.all(color: AppColors.primary, width: 1),
                                  borderRadius: BorderRadius.all(Radius.circular(6.0))),
                              child: Column(
                                children: [
                                  const SizedBox(
                                    height: 45.0,
                                  ),
                                  const SizedBox(
                                    height: 14.0,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Container(
                                        width: 75,
                                        child: Center(
                                          child: Text('Минимум',
                                              style: AppStyles.Roboto400(
                                                  color: AppColors.gray)),
                                        ),
                                      ),
                                      Container(
                                        width: 75,
                                        child: Center(
                                          child: Text('Доступно',
                                              style: AppStyles.Roboto400(
                                                  color: AppColors.gray)),
                                        ),
                                      ),
                                      Container(
                                        width: 75,
                                        child: Center(
                                          child: Text('Сумма',
                                              style: AppStyles.Roboto400(
                                                  color: AppColors.gray)),
                                        ),
                                      )
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 14.0,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                    children: [
                                      ClipRRect(
                                        borderRadius: BorderRadius.all(Radius.circular(6.0)),
                                        child: Container(
                                          color: AppColors.primary.withOpacity(0.05),
                                          height: 40,
                                          width: 75,
                                          child: Center(child: Text('100',style: AppStyles.Roboto500(color: AppColors.gray),)),
                                        ),
                                      ),
                                      ClipRRect(
                                        borderRadius: BorderRadius.all(Radius.circular(6.0)),
                                        child: Container(
                                          color: AppColors.primary.withOpacity(0.05),
                                          height: 40,
                                          width: 75,
                                          child: Center(
                                              child: Text(state.userResponse.balance,style: AppStyles.Roboto500(color: AppColors.gray))),
                                        ),
                                      ),

                                      Center(
                                        child: Container(
                                          //color: AppColors.primary.withOpacity(0.05),
                                          height: 40,
                                          width: 75,
                                          decoration: BoxDecoration(
                                              border: Border.all(
                                                  color: AppColors.primary, width: 1),
                                              borderRadius: BorderRadius.all(Radius.circular(6.0))
                                          ),
                                          child: TextField(
                                            controller: _coins,
                                            keyboardType: TextInputType.number,
                                            style: AppStyles.Roboto500(color: AppColors.primary),
                                            cursorColor: AppColors.primary,
                                            textAlign: TextAlign.center,
                                            decoration: InputDecoration(
                                              fillColor:
                                              AppColors.backgroundTransparent,
                                              filled: true,
                                              errorStyle: AppStyles.Roboto400(
                                                  color: Colors.redAccent,
                                                  fontSize: 10),
                                              alignLabelWithHint: false,
                                              contentPadding: EdgeInsets.only(
                                                  bottom: 8, left: 8, right: 8),
                                              // hintText: hintText,
                                              hintStyle: AppStyles.Roboto400(
                                                  color: AppColors.black),
                                              labelStyle: AppStyles.Roboto400(
                                                  color: AppColors.black),
                                              border: InputBorder.none,
                                              enabledBorder: InputBorder.none,
                                              disabledBorder: InputBorder.none,
                                              errorBorder: InputBorder.none,
                                              focusedBorder: InputBorder.none,
                                              focusedErrorBorder: InputBorder.none,
                                              //   labelText: labelText,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),

                          Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: SubmitButton(
                              onPressed: () {
                                _onTransferClick();
                              },
                              text: 'Перевести',
                              buttonColor: AppColors.primary,
                              textColor: AppColors.white,
                            ),
                          ),

                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 34.0),
                            child: Text(
                              'Обратите внимание на Ваш текущий баланс, введите разрешенное для вывода количество монет.',
                              maxLines: 2,
                              style: AppStyles.Roboto400(color: AppColors.gray),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          Expanded(child: SizedBox())
                        ],
                      );
                    }

                    return Center(
                        child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation<Color>(AppColors.primary),
                        ));
                  },
                );

              }
            })


    );
  }

  _showDialog() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int packageId = prefs.getInt('packageId');

    if (packageId == 1) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return CustomAlert(
              SubmitButton(
                buttonColor: AppColors.primary,
                text: 'Продолжить',
                textColor: AppColors.white,
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              image: 'assets/images/success.png',
              //title: 'Успех',
              description: 'Вы не можете купить пакет,у вас Бронзоавй пакет',
            );
          });
    }
  }

  void _onTransferClick() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int packageId = prefs.getInt('packageId');

    if (packageId == 1) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return CustomAlert(
              SubmitButton(
                buttonColor: AppColors.primary,
                text: 'Продолжить',
                textColor: AppColors.white,
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              image: 'assets/images/success.png',
              //title: 'Успех',
              description: 'Вы не можете сделать перевод,у вас Бронзоавй пакет!',
            );
          });
    }


    else if (_coins.value.text.isEmpty) {
      _scaffoldKey.currentState.showSnackBar(
        new SnackBar(
          backgroundColor: AppColors.red,
          duration: new Duration(seconds: 3),
          behavior: SnackBarBehavior.floating,
          elevation: 6.0,
          content: new Text('Введите сумму'),
        ),
      );
    }

    final int coins = int.parse(_coins.value.text);

    _bloc.add(TransferCoinsToStockExchangeEvent(coin: coins));
    _bloc.add(UpdateTransScreenEvent());
  }
}
