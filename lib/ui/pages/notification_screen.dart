import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:no_fine/bloc/notificatios_bloc/notifications_bloc.dart';
import 'package:no_fine/bloc/notificatios_bloc/notifications_event.dart';
import 'package:no_fine/bloc/notificatios_bloc/notifications_state.dart';
import 'package:no_fine/data/api_models/notifications_response.dart';
import 'package:no_fine/ui/resoursese/app_colors.dart';
import 'package:no_fine/ui/resoursese/app_styles.dart';
import 'package:no_fine/ui/widgets/app_bar.dart';
import 'package:no_fine/ui/widgets/submit_button.dart';

class NotificationScreen extends StatefulWidget {
  static const String route = "/notificaton";
  final bool isVisibleAppBar;

  const NotificationScreen({Key key, this.isVisibleAppBar}) : super(key: key);
  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  NotificationsBloc _bloc;

  @override
  void initState() {
    _bloc = BlocProvider.of<NotificationsBloc>(context);
    _bloc.add(UpdateNotificationsEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: CustomAppBar(
          titleText: 'Уведомления',
          isExistClouseIcon: widget.isVisibleAppBar,
          context: context,
        ),
        body: BlocBuilder<NotificationsBloc, NotificationsState>(
            builder: (context, state) {
          if (state is NotificationsLoadedState) {
            return Column(children: [
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: SubmitButton(
                    text: 'Прочитать все',
                    textColor: AppColors.white,
                    buttonColor: AppColors.primary,
                    onPressed: () {
                      _bloc.add(ReadAllNotificationsEvent());
                      _bloc.add(UpdateNotificationsEvent());
                    }),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    child: Center(
                      child: Text(
                        'Входящие',
                        style: AppStyles.Roboto400(color: AppColors.gray),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Center(
                      child: Text('Дата',
                          style: AppStyles.Roboto400(color: AppColors.gray)),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: Divider(
                  color: AppColors.gray,
                ),
              ),
              notificationsList(state.notificationsResponse),
            ]);
          }
          return Center(
              child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(AppColors.primary),
          ));
        }));
  }

  notificationsList(List<NotificationsResponse> notificationsList) => Expanded(
        child: ListView.builder(
            itemCount: notificationsList.length ?? 0,
            scrollDirection: Axis.vertical,
            itemBuilder: (BuildContext context, int index) {
              return Visibility(
                visible: notificationsList[index].readAt == null,
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 16.0, vertical: 5),
                  child: Container(
                    height: 40.0,
                    decoration: new BoxDecoration(
                      color: AppColors.primary.withOpacity(0.06),
                      borderRadius: BorderRadius.all(Radius.circular(6.0)),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Container(
                          // height: 20,
                          width: 50,
                          child: Image.asset(
                              'assets/icons/notification_item_icon.png',
                              scale: 2),
                        ),
                        Expanded(
                            child: Center(
                          child: Text(
                            notificationsList[index].name,
                            style: AppStyles.Roboto400(color: AppColors.gray),
                          ),
                        )),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16.0),
                          child: Text(
                            notificationsList[index].createdAt.day.toString() +
                                '.' +
                                notificationsList[index]
                                    .createdAt
                                    .month
                                    .toString() +
                                '.' +
                                notificationsList[index]
                                    .createdAt
                                    .year
                                    .toString(),
                            style: AppStyles.Roboto400(color: AppColors.gray),
                          ),
                        ),
                        IconButton(
                          onPressed: () {
                            setState(() {
                              _bloc.add(ReadNotificationsEvent(
                                  id: notificationsList[index].id));
                              _bloc.add(UpdateNotificationsEvent());
                            });
                          },
                          icon: Icon(
                            Icons.close,
                            color: AppColors.gray,
                          ),
                          iconSize: 20.0,
                        ),
                      ],
                    ),
                  ),
                ),
              );
            }),
      );
}
