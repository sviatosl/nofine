import 'package:flutter/material.dart';
import 'package:no_fine/ui/resoursese/app_colors.dart';
import 'package:no_fine/ui/resoursese/app_styles.dart';
import 'package:no_fine/ui/widgets/app_bar.dart';
import 'package:no_fine/ui/widgets/submit_button.dart';

class InfoScreen extends StatefulWidget {
  static const String route = "/info";

  @override
  _InfoScreenState createState() => _InfoScreenState();
}

class _InfoScreenState extends State<InfoScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: CustomAppBar(
          titleText: 'О сервисе',
          context: context,
        ),
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: [
              Text(
                'Главная задача проекта способствовать развитию и интеграции цифровых технологий в повседневный быт автомобилиста. Приобщить и научить современного водителя с выгодой использовать цифровые продукты. Мы меняем подход и отношение к штрафам, показывая выгоду, которую можно получить используя компенсацию. Наша монета направлена на развитие дохода автомобилиста. Будущее проекта направлено на помощь автомобилистам.',
                style: AppStyles.Roboto400(color: AppColors.blak_prof),
              ),

              //const SizedBox(height: 56  ,),
            //  SubmitButton(text: 'Написать нам',buttonColor: AppColors.primary,textColor: AppColors.white,onPressed: (){},)
            ],
          ),
        ));
  }


}
