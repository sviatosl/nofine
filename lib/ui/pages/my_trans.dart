import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:no_fine/bloc/auth/translations_bloc/translations_bloc.dart';
import 'package:no_fine/bloc/auth/translations_bloc/translations_event.dart';
import 'package:no_fine/bloc/auth/translations_bloc/translations_state.dart';
import 'package:no_fine/data/api_models/transfers_response.dart';
import 'package:no_fine/ui/pages/transfer_of_coins_screen_to_person.dart';
import 'package:no_fine/ui/resoursese/app_colors.dart';
import 'package:no_fine/ui/resoursese/app_styles.dart';
import 'package:no_fine/ui/widgets/app_bar.dart';
import 'package:no_fine/ui/widgets/submit_button.dart';

import ' withdrawing_coins_screen.dart';

class MyTransScreen extends StatefulWidget {
  static const String route = "/mytrans";

  @override
  _MyTransScreenState createState() => _MyTransScreenState();
}

const BorderSide noBorder = BorderSide(width: 0.0, color: Colors.transparent);
const BorderSide activeBorder = BorderSide(width: 2.0, color: Colors.green);

class _MyTransScreenState extends State<MyTransScreen>
    with SingleTickerProviderStateMixin {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  // TabController controller;
  TransferBloc _bloc;

  TabController _tabController;
  int _selectedTab = 0;

  static const tab_items = [
    Text(
      'Получено',
      style: TextStyle(
          fontSize: 12,
          fontWeight: FontWeight.w400,
          fontFamily: 'RobotoMedium',
          color: AppColors.gray),
    ),
    Text(
      'Отправленно',
      style: TextStyle(
          fontSize: 12,
          fontWeight: FontWeight.w400,
          fontFamily: 'RobotoMedium',
          color: AppColors.gray),
    ),
  ];

  @override
  void initState() {
    _bloc = BlocProvider.of<TransferBloc>(context);
    _bloc.add(UpdateAllTransfersEvent());
    super.initState();

    _tabController = TabController(vsync: this, length: tab_items.length);

    _tabController.addListener(() {
      if (!_tabController.indexIsChanging) {
        setState(() {
          _selectedTab = _tabController.index;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: CustomAppBar(
        titleText: 'Мои переводы',
        context: context,
      ),
      body: Column(
        children: [
          const SizedBox(
            height: 16,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                  child: SubmitButton(
                    onPressed: () async{
                    await  Navigator.of(context).pushNamed(
                        TransferCoinsToPersonScreen.route,
                      );
                      _bloc.add(UpdateAllTransfersEvent());
                    },
                    text: 'Перевод монет',
                    textColor: AppColors.white,
                    buttonColor: AppColors.primary,
                  ),
                ),
                const SizedBox(
                  width: 30.0,
                ),
                Expanded(
                  child: SubmitButton(
                    onPressed: () async {
                      await Navigator.of(context).pushNamed(
                        WithdrawingCoinsScreen.route,
                      );
                      _bloc.add(UpdateAllTransfersEvent());
                    },
                    text: 'Вывод на биржу',
                    textColor: AppColors.primary,
                    buttonColor: AppColors.backgroundTransparent,
                    borderColor: AppColors.primary,
                  ),
                )
              ],
            ),
          ),
          Expanded(
            child: DefaultTabController(
                length: tab_items.length,
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 16.0),
                    Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Container(
                        height: 46,
                        child: Stack(
                          overflow: Overflow.visible,
                          children: [
                            Positioned(
                              left: 0,
                              right: 0,
                              bottom: 0,
                              top: 0,
                              child: CustomPaint(
                                painter: TabbarPainter(
                                    _selectedTab / tab_items.length,
                                    tab_items.length,
                                    AppColors.primary),
                                child: Container(
                                  height: 75.0,
                                ),
                              ),
                            ),
                            Positioned(
                              left: 0,
                              right: 0,
                              bottom: 0,
                              top: 0,
                              child: SizedBox(
                                height: 64,
                                width: 300,
                                child: Row(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.stretch,
                                  children: tab_items
                                      .asMap()
                                      .entries
                                      .map(
                                        (item) => Expanded(
                                          child: GestureDetector(
                                              behavior: HitTestBehavior.opaque,
                                              onTap: () {
                                                _tabController.index = item.key;
                                              },
                                              child: Center(
                                                child: Container(
                                                  child: item.value,
                                                ),
                                              )),
                                        ),
                                      )
                                      .toList(),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      child: TabBarView(
                        physics: NeverScrollableScrollPhysics(),
                        controller: _tabController,
                        children: [
                          BlocBuilder<TransferBloc, TransferState>(
                              builder: (context, state) {
                            if (state is TransferAllLoadedState) {
                              return transfersListRes(
                                  state.transferResponse.recipientUser);
                            }
                            return Container(
                              height: 1,
                              width: 1,
                            );
                          }),
                          BlocBuilder<TransferBloc, TransferState>(
                              builder: (context, state) {
                            if (state is TransferAllLoadedState) {
                              return transfersListSen(
                                  state.transferResponse.senderUser);
                            }
                            return Center(
                                child: CircularProgressIndicator(
                                  valueColor: AlwaysStoppedAnimation<Color>(AppColors.primary),
                                ));
                          })
                        ],
                      ),
                    ),
                  ],
                )),
          ),
        ],
      ),
    );
  }

  transfersListRes(List<User> sList) => Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              children: [
                Expanded(
                    child: Center(
                        child: Text(
                  'Получатель',
                  style: AppStyles.Roboto400(color: AppColors.gray),
                ))),
                Expanded(
                    child: Center(
                        child: Text(
                  'Монет',
                  style: AppStyles.Roboto400(color: AppColors.gray),
                ))),
                Expanded(
                    child: Center(
                        child: Text(
                  'Статус',
                  style: AppStyles.Roboto400(color: AppColors.gray),
                ))),
                Expanded(
                    child: Center(
                        child: Text(
                  'Дата',
                  style: AppStyles.Roboto400(color: AppColors.gray),
                )))
              ],
            ),
          ),
          Expanded(
            child: ListView.builder(
                itemCount: sList.length ?? 0,
                scrollDirection: Axis.vertical,
                itemBuilder: (BuildContext context, int index) {
                  return Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 3.0, horizontal: 16.0),
                    child: Container(
                      height: 40.0,
                      decoration: new BoxDecoration(
                        color: AppColors.primary.withOpacity(0.06),
                        borderRadius: BorderRadius.all(Radius.circular(6.0)),
                      ),
                      child: Row(
                        children: [
                          Expanded(
                              child: Center(
                                  child: Text(
                            sList[index].recipientUser,
                            style: AppStyles.Roboto400(color: AppColors.gray),
                          ))),
                          Expanded(
                              child: Center(
                                  child: Text(
                            sList[index].coin.toString(),
                            style: AppStyles.Roboto400(color: AppColors.gray),
                          ))),
                          Expanded(
                              child: Center(
                                  child: Text(
                            sList[index].status,
                            style: AppStyles.Roboto400(color: AppColors.gray),
                          ))),
                          Expanded(
                              child: Center(
                                  child: Text(
                                    DateFormat('dd.MM.yyyy').format(sList[index].createdAt),
                                      style: AppStyles.Roboto400(color: AppColors.gray),
                          )))
                        ],
                      ),
                    ),
                  );
                }),
          ),
          const SizedBox(
            height: 12.0,
          )
        ],
      );

  transfersListSen(List<User> sList) => Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              children: [
                Expanded(
                    child: Center(
                        child: Text(
                  'Отправитель',
                  style: AppStyles.Roboto400(color: AppColors.gray),
                ))),
                Expanded(
                    child: Center(
                        child: Text(
                  'Монет',
                  style: AppStyles.Roboto400(color: AppColors.gray),
                ))),
                Expanded(
                    child: Center(
                        child: Text(
                  'Статус',
                  style: AppStyles.Roboto400(color: AppColors.gray),
                ))),
                Expanded(
                    child: Center(
                        child: Text(
                  'Дата',
                  style: AppStyles.Roboto400(color: AppColors.gray),
                )))
              ],
            ),
          ),
          Expanded(
            child: ListView.builder(
                itemCount: sList.length ?? 0,
                scrollDirection: Axis.vertical,
                itemBuilder: (BuildContext context, int index) {
                  return Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 3.0, horizontal: 16.0),
                    child: Container(
                      height: 40.0,
                      decoration: new BoxDecoration(
                        color: AppColors.primary.withOpacity(0.06),
                        borderRadius: BorderRadius.all(Radius.circular(6.0)),
                      ),
                      child: Row(
                        children: [
                          Expanded(
                              child: Center(
                                  child: Text(
                            sList[index].senderUser,
                            style: AppStyles.Roboto400(color: AppColors.gray),
                          ))),
                          Expanded(
                              child: Center(
                                  child: Text(
                            sList[index].coin.toString(),
                            style: AppStyles.Roboto400(color: AppColors.gray),
                          ))),
                          Expanded(
                              child: Center(
                                  child: Text(
                            sList[index].status,
                            style: AppStyles.Roboto400(color: AppColors.gray),
                          ))),
                          Expanded(
                              child: Center(
                                  child: Text(
                                    DateFormat('dd.MM.yyyy').format(sList[index].createdAt),
                            style: AppStyles.Roboto400(color: AppColors.gray),
                          )))
                        ],
                      ),
                    ),
                  );
                }),
          ),
          const SizedBox(
            height: 12.0,
          )
        ],
      );
}

class TabbarPainter extends CustomPainter {
  double loc;
  int length;
  Color color;

  TabbarPainter(double startingLoc, int itemsLength, this.color) {
    loc = startingLoc;
    length = itemsLength;
  }

  @override
  void paint(Canvas canvas, Size size) {
    double radius = 10;

    final paint = Paint()
      ..color = color
      ..style = PaintingStyle.stroke
      ..strokeWidth = 1;

    final path = Path();
    if (loc > 0.0) {
      path.moveTo(0, size.height);
      path.lineTo(loc * size.width - radius, size.height);
      path.arcToPoint(Offset((loc * size.width), size.height - radius),
          radius: Radius.circular(radius), clockwise: false);
      path.lineTo(loc * size.width, 0 + radius);
      path.arcToPoint(
        Offset((loc * size.width) + radius, 0),
        radius: Radius.circular(radius),
      );
    }

    if (((loc * size.width) + size.width / length) < size.width) {
      path.lineTo((loc * size.width) + (size.width / length) - radius, 0);
      path.arcToPoint(
        Offset((loc * size.width) + size.width / length, 0 + radius),
        radius: Radius.circular(radius),
      );
      path.lineTo(
          (loc * size.width) + size.width / length, size.height - radius);
      path.arcToPoint(
          Offset(
              (loc * size.width) + (size.width / length) + radius, size.height),
          radius: Radius.circular(radius),
          clockwise: false);
      path.lineTo(size.width, size.height);
    } else {
      path.lineTo((loc * size.width) + (size.width / length), 0);
    }
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return this != oldDelegate;
  }
}

/*child: Column(
          children: [
            const SizedBox(
              height: 8.0,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    child: SubmitButton(
                      onPressed: () {
                        Navigator.of(context).pushNamed(
                          TransferCoinsToPersonScreen.route,
                        );
                        _bloc.add(UpdateAllTransfersEvent());
                      },
                      text: 'Перевод монет',
                      textColor: AppColors.white,
                      buttonColor: AppColors.primary,
                    ),
                  ),
                  const SizedBox(
                    width: 30.0,
                  ),
                  Expanded(
                    child: SubmitButton(
                      onPressed: () {
                        Navigator.of(context).pushNamed(
                          WithdrawingCoinsScreen.route,
                        );
                        _bloc.add(UpdateAllTransfersEvent());
                      },
                      text: 'Вывод на биржу',
                      textColor: AppColors.primary,
                      buttonColor: AppColors.backgroundTransparent,
                      borderColor: AppColors.primary,
                    ),
                  )
                ],
              ),
            ),
            BlocBuilder<TransferBloc, TransferState>(
                builder: (context, state) {

                  if (state is TransferAllLoadedState) {

                    return transfersList(state.transferResponse.recipientUser);







                  }
                  return Container(height: 1,width: 1,);


                })
          ],
        ),*/
