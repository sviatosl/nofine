import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:no_fine/bloc/bonuses_bloc/bonuses_bloc.dart';
import 'package:no_fine/bloc/bonuses_bloc/bonuses_event.dart';
import 'package:no_fine/bloc/bonuses_bloc/bonuses_state.dart';
import 'package:no_fine/data/api_models/bonuse_response.dart';
import 'package:no_fine/ui/resoursese/app_colors.dart';
import 'package:no_fine/ui/resoursese/app_styles.dart';
import 'package:no_fine/ui/widgets/app_bar.dart';
import 'package:no_fine/ui/widgets/submit_button.dart';

import 'adding_bonuses_screen.dart';

class MyBonusesScreen extends StatefulWidget {
  static const String route = "/myBonuses";

  @override
  _MyBonusesScreenState createState() => _MyBonusesScreenState();
}

class _MyBonusesScreenState extends State<MyBonusesScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  BonusesBloc _bloc;

  @override
  void initState() {
    _bloc = BlocProvider.of<BonusesBloc>(context);
    _bloc.add(UpdateBonusesEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: CustomAppBar(
          titleText: 'Мои бонусы',
          context: context,
        ),
        body: BlocBuilder<BonusesBloc, BonusesState>(builder: (context, state) {
          if (state is BonusesLoadedState) {
            return Column(
              children: [
                const SizedBox(
                  height: 8.0,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Expanded(
                        child: Row(
                          //  mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Image.asset(
                              'assets/icons/coins_icon.png',
                              scale: 2,
                            ),
                            const SizedBox(
                              width: 8.0,
                            ),
                            Center(
                              child: Text(
                                'Загружено: ' +
                                        state.bonuseResponce.length
                                            .toString() ??
                                    '',
                                style: AppStyles.Roboto400(
                                    fontSize: 14, color: AppColors.primary),
                              ),
                            )
                          ],
                        ),
                      ),
                      Expanded(
                        child: SubmitButton(
                          onPressed: () async {
                            await Navigator.of(context).pushNamed(
                              AddBonusesScreen.route,
                            );
                             _bloc.add(UpdateBonusesEvent());
                          },
                          text: 'Добавить',
                          textColor: AppColors.white,
                          buttonColor: AppColors.primary,
                        ),
                      )
                    ],
                  ),
                ),
                const SizedBox(
                  height: 22.0,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Expanded(
                        flex: 1,
                        child: Center(
                          child: Text(
                            'VIN',
                            style: AppStyles.Roboto400(color: AppColors.gray),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Center(
                          child: Text('Номер',
                              style:
                                  AppStyles.Roboto400(color: AppColors.gray)),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Center(
                          child: Text('Удостоверение',
                              style:
                                  AppStyles.Roboto400(color: AppColors.gray)),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Divider(
                    color: AppColors.gray,
                  ),
                ),
                bonusesList(state.bonuseResponce)
              ],
            );
          }
          return Center(
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(AppColors.primary),
              ));
        }));
  }

  bonusesList(List<BonuseResponce> bonusesList) => Expanded(
        child: ListView.builder(
            itemCount: bonusesList.length ?? 0,
            scrollDirection: Axis.vertical,
            itemBuilder: (BuildContext context, int index) {
              return Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16.0, vertical: 5),
                child: Container(
                  decoration: new BoxDecoration(
                    color: AppColors.primary.withOpacity(0.06),
                    borderRadius: BorderRadius.all(Radius.circular(6.0)),
                  ),
                  height: 40.0,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Expanded(
                        flex: 1,
                        child: Center(
                          child: Text(
                            bonusesList[index].autoVin,
                            style: AppStyles.Roboto400(color: AppColors.gray),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Center(
                          child: Text(
                              /*bonusesList[index].type.toString()*/
                              bonusesList[index].autoNumber,
                              style:
                                  AppStyles.Roboto400(color: AppColors.gray)),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Center(
                          child: Text(bonusesList[index].autoLicense,
                              style:
                                  AppStyles.Roboto400(color: AppColors.gray)),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }),
      );

/*Widget function12(String vin, String number, String id) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 5),
      child: Container(
        height: 40.0,
        color: AppColors.primary.withOpacity(0.06),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Text(
              vin,
              style: AppStyles.Roboto400(color: AppColors.gray),
            ),
            Text(number, style: AppStyles.Roboto400(color: AppColors.gray)),
            Text(id, style: AppStyles.Roboto400(color: AppColors.gray)),
          ],
        ),
      ),
    );
  }*/
}
