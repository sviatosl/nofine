import 'package:flutter/material.dart';
import 'package:no_fine/ui/resoursese/app_colors.dart';
import 'package:no_fine/ui/resoursese/app_styles.dart';
import 'package:no_fine/ui/widgets/app_bar.dart';

class NewsDetailScreen extends StatefulWidget {
  static const String route = "/newsDetailScreen";
  final String title;
  final String text;
  final String data;

  const NewsDetailScreen({Key key, this.title,  this.text, this.data})
      : super(key: key);

  @override
  _NewsDetailScreenState createState() => _NewsDetailScreenState();
}

class _NewsDetailScreenState extends State<NewsDetailScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

//  BonusesBloc _bloc;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: CustomAppBar(
          titleText: 'Новости',
          context: context,
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Container(
           height: 342.0,
            //  margin: const EdgeInsets.all(14.0),
            decoration: BoxDecoration(
                border: Border.all(color: AppColors.primary, width: 1),
                borderRadius: BorderRadius.all(Radius.circular(6.0))),
            child: Container(
              margin: const EdgeInsets.all(14.0),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 40.0),
                    child: Text(
                      widget.title,
                      textAlign: TextAlign.center,
                      style: AppStyles.Roboto500(color: AppColors.blackk,fontSize: 14,fontWeight: FontWeight.w700),
                    ),
                  ),
                  const SizedBox(
                    height: 14,
                  ),
                  Container(
                    child: Text(
                      widget.text,
                      style: AppStyles.Roboto400(color: AppColors.blackk),
                    ),
                  ),
                  Expanded(child: const SizedBox(height: 16,)),
                  Container(
                    child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          widget.data,
                          style: AppStyles.Roboto500(
                              fontSize: 12, color: AppColors.blackk,fontWeight: FontWeight.w700),
                        )),
                  )
                ],
              ),
            ),
          ),
        ));
  }
}
