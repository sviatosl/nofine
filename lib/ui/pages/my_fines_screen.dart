import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:no_fine/bloc/fines_bloc/fines_bloc.dart';
import 'package:no_fine/bloc/fines_bloc/fines_event.dart';
import 'package:no_fine/bloc/fines_bloc/fines_state.dart';
import 'package:no_fine/data/api_models/fines_response.dart';
import 'package:no_fine/ui/resoursese/app_colors.dart';
import 'package:no_fine/ui/resoursese/app_styles.dart';
import 'package:no_fine/ui/widgets/app_bar.dart';
import 'package:no_fine/ui/widgets/submit_button.dart';
import 'package:no_fine/ui/widgets/text_fild.dart';

import 'add_fine_screen.dart';

class MyFinesScreen extends StatefulWidget {
  static const String route = "/myFines";

  @override
  _MyFinesScreenState createState() => _MyFinesScreenState();
}

class _MyFinesScreenState extends State<MyFinesScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  FinesBloc _bloc;
  TextEditingController _number = TextEditingController();

  @override
  void initState() {
    _bloc = BlocProvider.of<FinesBloc>(context);
    _bloc.add(UpdateFinesEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: CustomAppBar(
          titleText: 'Мои штрафы',
          context: context,
        ),
        body:
            BlocBuilder<FinesBloc, FinesState>(builder: (context, state) {
          if (state is FinesLoadedState) {

            return Column(
              children: [
                const SizedBox(
                  height: 8.0,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Expanded(
                        child: Row(
                          //  mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Image.asset(
                              'assets/icons/coins_icon.png',
                              scale: 2,
                            ),
                            const SizedBox(
                              width: 8.0,
                            ),
                            Center(
                              child: Text(
                                'Загружено: ' +
                                    state.fineResponse.length.toString(),
                                style: AppStyles.Roboto400(
                                    fontSize: 14, color: AppColors.primary),
                              ),
                            )
                          ],
                        ),
                      ),
                      Expanded(
                        child: SubmitButton(
                          onPressed: () async {
                            await Navigator.of(context).pushNamed(
                              AddFineScreen.route,
                            );
                            _bloc.add(UpdateFinesEvent());
                          },
                          text: 'Добавить',
                          textColor: AppColors.white,
                          buttonColor: AppColors.primary,
                        ),
                      )
                    ],
                  ),
                ),
                const SizedBox(
                  height: 22.0,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Expanded(
                        flex: 2,
                        child: Center(
                          child: Text(
                            'Номер постановления',
                            style: AppStyles.Roboto400(color: AppColors.gray),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Center(

                          child: Text('Служба',
                              style: AppStyles.Roboto400(color: AppColors.gray)),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Center(
                          child: Text('Дата',
                              style: AppStyles.Roboto400(color: AppColors.gray)),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Divider(
                    color: AppColors.gray,
                  ),
                ),
                bonusesList(state.fineResponse)
              ],
            );
          }
          return Center(
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(AppColors.primary),
              ));
        }


        ));
  }

  bonusesList(List<FineResponse> bonusesList) => Expanded(
        child: ListView.builder(
            itemCount: bonusesList.length ?? 0,
            scrollDirection: Axis.vertical,
            itemBuilder: (BuildContext context, int index) {
              return Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16.0, vertical: 5),
                child: Container(
                  decoration: new BoxDecoration(
                    color: AppColors.primary.withOpacity(0.06),
                    borderRadius: BorderRadius.all(Radius.circular(6.0)),
                  ),
                  height: 40.0,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Expanded(
                        flex: 2,
                        child: Center(
                          child: Text(
                            bonusesList[index].uin,
                            style: AppStyles.Roboto400(color: AppColors.gray),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Center(
                          child: Text(
                              /*bonusesList[index].type.toString()*/
                              getType(bonusesList[index].type),
                              style: AppStyles.Roboto400(color: AppColors.gray)),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Center(
                          child: Text(DateFormat('dd.MM.yyyy').format(bonusesList[index].createdAt)   ,
                              style: AppStyles.Roboto400(color: AppColors.gray)),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }),
      );

  String getType(int type) {
    switch (type) {
      case 1:
        return 'ГИБДД';
        break;
      case 2:
        return 'МАДИ';
        break;
      case 3:
        return 'АМПП';
        break;
    }
  }
}
