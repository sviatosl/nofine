import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:no_fine/bloc/fines_bloc/fines_bloc.dart';
import 'package:no_fine/bloc/fines_bloc/fines_event.dart';
import 'package:no_fine/bloc/fines_bloc/fines_state.dart';
import 'package:no_fine/ui/resoursese/app_colors.dart';
import 'package:no_fine/ui/resoursese/app_styles.dart';
import 'package:no_fine/ui/widgets/app_bar.dart';
import 'package:no_fine/ui/widgets/custom_alert.dart';
import 'package:no_fine/ui/widgets/submit_button.dart';
import 'package:no_fine/ui/widgets/text_fild.dart';

class AddFineScreen extends StatefulWidget {
  static const String route = "/addFines";

  @override
  _AddFineScreenState createState() => _AddFineScreenState();
}

class _AddFineScreenState extends State<AddFineScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  FinesBloc _bloc;
  TextEditingController _uin = TextEditingController();
  int type = 1;

  // Color bulbColor = AppColors.primary;
  int index = 1;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    _bloc = BlocProvider.of<FinesBloc>(context);
    //  _bloc.add(UpdateTransScreenEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: CustomAppBar(
          titleText: 'Мои штрафы',
          context: context,
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Container(
                  height: 250.0,
                  width: double.infinity,
                  decoration: BoxDecoration(
                      border: Border.all(color: AppColors.primary, width: 1),
                      borderRadius: BorderRadius.all(Radius.circular(6.0))),
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 36.0,
                      ),
                      Text(
                        'Пожалуйста, выберите тип службы:*',
                        style: AppStyles.Roboto400(color: AppColors.gray),
                      ),
                      const SizedBox(
                        height: 14.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: Row(
                          children: [
                            Expanded(
                              child: Row(children: [
                                Radio(
                                    activeColor: AppColors.primary,
                                    value: 1,
                                    groupValue: index,
                                    onChanged: (val) {
                                      setState(() {
                                        index = 1;
                                        type = 1;
                                        print('1');
                                      });
                                    }),
                                new Text(
                                  'ГИБДД',
                                  style: AppStyles.Roboto400(
                                      fontSize: 14, color: AppColors.blackk),
                                ),
                              ]),
                            ),
                            Expanded(
                              child: Row(children: [
                                Radio(
                                    activeColor: AppColors.primary,
                                    value: 2,
                                    groupValue: index,
                                    onChanged: (val) {
                                      setState(() {
                                        index = 2;
                                        type = 2;
                                        print('2');
                                      });
                                    }),
                                new Text(
                                  'МАДИ',
                                  style: AppStyles.Roboto400(
                                      fontSize: 14, color: AppColors.blackk),
                                ),
                              ]),
                            ),
                            Expanded(
                              child: Row(children: [
                                Radio(
                                    activeColor: AppColors.primary,
                                    value: 3,
                                    groupValue: index,
                                    onChanged: (val) {
                                      setState(() {
                                        index = 3;
                                        type = 3;
                                        print('3');
                                      });
                                    }),
                                new Text(
                                  'АМПП',
                                  style: AppStyles.Roboto400(
                                      fontSize: 14, color: AppColors.blackk),
                                ),
                              ]),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 10.0,
                      ),
                      Container(
                        color: AppColors.primary.withOpacity(0.06),
                        child: Padding(
                          padding: const EdgeInsets.all(2.0),
                          child: Column(
                            children: [
                              const SizedBox(
                                height: 4.0,
                              ),
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 40.0),
                                  child: Text('Номер УИН ',
                                      style: AppStyles.Roboto400(
                                          color: AppColors.gray)),
                                ),
                              ),
                              const SizedBox(
                                height: 4.0,
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 40.0),
                                child: Form(
                                  key: _formKey,
                                  child: MyTextField(
                                    limitNum: 25,
                                    keyboardTypeIsInt: true,
                                    controller: _uin,
                                    /* fieldValidator: (String value) {
                                      if (value.length < 20) {
                                        return 'УИН не менее 20 цыфр';
                                      }
                                      return null;
                                    },*/
                                  ),
                                ),
                              ),


                              const SizedBox(
                                height: 16,
                              ),
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 14.0,
                      ),
                    ],
                  ),
                ),
              ),

              Padding(
                padding: const EdgeInsets.all(16.0),
                child: SubmitButton(
                  onPressed: () {
                    _onTransferClick();

                  },
                  text: 'Добавить',
                  buttonColor: AppColors.primary,
                  textColor: AppColors.white,
                ),
              ),
              const SizedBox(
                height: 6,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 36.0),
                child: Column(
                  children: [
                    Text(
                      'ГИБДД - Государственная инспекция безопасного дорожного движения',
                      style: AppStyles.Roboto400(color: AppColors.gray),
                      maxLines: 2,
                    ),
                    const SizedBox(
                      height: 6,
                    ),
                    Text(
                      'МАДИ - Ммосковская административная дорожная инспекция',
                      style: AppStyles.Roboto400(color: AppColors.gray),
                      maxLines: 2,
                    ),
                    const SizedBox(
                      height: 6,
                    ),
                    Text(
                      'АМПП - администратор Московского парковочного пространства',
                      style: AppStyles.Roboto400(color: AppColors.gray),
                      maxLines: 2,
                    ),
                  ],
                ),
              ),
            //  Expanded(child: SizedBox()),
              BlocListener<FinesBloc, FinesState>(
                listener: (context, state) {
                  if (state is FinesAddSuccessState) {
                    /*_scaffoldKey.currentState.showSnackBar(
                      new SnackBar(
                        backgroundColor: AppColors.primary,
                        duration: new Duration(seconds: 6),
                        behavior: SnackBarBehavior.floating,
                        elevation: 6.0,
                        //content: new Text('Можете добавить еще штраф.'),
                        content: new Text(state.fineResponse),
                      ),
                    );*/
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {

                          return CustomAlert(
                            SubmitButton(buttonColor: AppColors.primary,text: 'Хорошо',textColor:AppColors.white,onPressed: (){

                              Navigator.pop(context);
                              Navigator.pop(context);

                            },),
                            image: 'assets/images/success.png',
                           //title: 'Успех',
                            description: state.fineResponse,

                          );
                        });
                  }

                  if (state is FinesFailureState) {

                   showDialog(
                   context: context,
                   builder: (BuildContext context) {
                    return CustomAlert(
                      SubmitButton(buttonColor: AppColors.primary,text: 'Хорошо',textColor:AppColors.white,onPressed: (){

                        Navigator.pop(context);

                      },),
                      image: 'assets/images/error.png',
                    //  title: 'Неудача',
                      description: state.fineResponse,

                    );
                   });
                  }
                },
                child: Container(
                  height: 1,
                  width: 1,
                ),
              )
            ],
          ),
        ));
  }

  void _onTransferClick() {
    if (_uin.value.text.length < 20 || _uin.value.text.length > 25) {
      _scaffoldKey.currentState.showSnackBar(
        new SnackBar(
          backgroundColor: AppColors.red,
          duration: new Duration(seconds: 3),
          behavior: SnackBarBehavior.floating,
          elevation: 6.0,
          content: new Text('Поле uin должно быть от 20 до 25 цыфр!'),
        ),
      );
    } else {
      final String uin = _uin.value.text;
      _bloc.add(AddFineEvent(uin: uin, type: type));
    }
    /*    if(_formKey.currentState.validate()) {
      final String uin = _uin.value.text;
      _bloc.add(AddFineEvent(uin: uin, type: type));
    }
*/
  }
}
