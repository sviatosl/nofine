import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:no_fine/ui/widgets/app_bar.dart';
import 'package:webview_flutter/webview_flutter.dart';

class PaymantScreen extends StatefulWidget {
  static const String route = "/paymant";

  /* final String title;
  final String text;*/

  @override
  _PaymantScreenState createState() => _PaymantScreenState();
}

class _PaymantScreenState extends State<PaymantScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

//  BonusesBloc _bloc;

  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    /* return Scaffold(
        key: _scaffoldKey,
        appBar: CustomAppBar(
          titleText:'Покупка пакета ',
          context: context,
        ),
      body: ,
    );*/
    return Padding(
      padding: const EdgeInsets.only(top:40.0),
      child: WebView(
        initialUrl: 'https://paymo.ru/rest/merchant/invoice/',
      ),
    );
  }
}
