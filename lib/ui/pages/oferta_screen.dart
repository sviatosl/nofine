import 'package:flutter/material.dart';
import 'package:no_fine/ui/resoursese/app_colors.dart';
import 'package:no_fine/ui/resoursese/app_styles.dart';
import 'package:no_fine/ui/widgets/app_bar.dart';
import 'package:no_fine/ui/widgets/submit_button.dart';

class OfertaScreen extends StatefulWidget {
  static const String route = "/oferta";
  /* final String title;
  final String text;*/



  @override
  _OfertaScreenState createState() => _OfertaScreenState();
}

class _OfertaScreenState extends State<OfertaScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

//  BonusesBloc _bloc;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: CustomAppBar(
          titleText:'Договор оферты',
          context: context,
        ),
        body: Container(
          // height: MediaQuery.of(context).size.height,
          child: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,//.horizontal
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Text('Общество с ограниченной ответственностью «Реар Метал Трейд» (ОГРН 1127746481048, место нахождения: 109428, РФ, г. Москва, проспект Рязанский, дом 8а, строение 19, Э 2 помещение III К 2), созданное в соответствии с законодательством Российской Федерации (далее – Администрация сайта), выражает намерение заключить договор по использованию Сайта с доменным именем https://shtrafam-net.ru/  и предоставлению услуг на этом сайте на условиях, содержащихся в настоящей Публичной оферте, со всяким физическим лицом, зарегистрированным на сайте (Пользователю)  и предоставившим Акцепт настоящей Оферты1. Основные понятияЕсли иное не вытекает из настоящей оферты, следующие слова и выражения будут иметь следующее значение1.1. «Администрация сайта» (Администрация)- ООО «Реар Метал Трейд» (ОГРН 1127746481048), сторона в Договоре Публичной Оферты, реализующая Монеты/Автотокены, предоставляющая возможность Пользователю заключить договор с Исполнителем на специальных условиях.1.2. Сайт – совокупность размещенных в сети электронных документов (файлов), объединенных единой темой, дизайном и единым адресным пространством домена https://shtrafam-net.ru/.1.3. «Пользовательское соглашение» (далее по тексту – Оферта, Публичная оферта, Договор) — данный документ, размещенный по адресу https://shtrafam-net.ru/. Оферта становится действительной с момента ее размещения на Сайте Администрацией сайта и действует до момента ее отзыва Администрацией сайта.В соответствии с пунктом 2 статьи 437 Гражданского кодекса Российской Федерации (далее – ГК РФ) данный документ является публичной Офертой (далее по тексту Оферта, Договор) для Сайта. В соответствии с пунктом 3 статьи 438 ГК РФ акцепт Оферты равносилен заключению договора на условиях, изложенных в Оферте, которые безоговорочно принимаются сторонами).',
                        style: AppStyles.Roboto400(color: AppColors.blackk),),
                    )),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: SubmitButton(text: 'Соглашаюсь',buttonColor: AppColors.primary,textColor: AppColors.white, onPressed: (){
                  Navigator.pop(context);
                },),
              )
            ],
          ),
        ));
  }
}
