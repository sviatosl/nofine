import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:no_fine/bloc/packages_bloc/packages_bloc.dart';
import 'package:no_fine/bloc/packages_bloc/packages_event.dart';
import 'package:no_fine/bloc/packages_bloc/packages_state.dart';
import 'package:no_fine/data/api_models/packages_response.dart';
import 'package:no_fine/ui/pages/payment_screen.dart';
import 'package:no_fine/ui/resoursese/app_colors.dart';
import 'package:no_fine/ui/resoursese/app_styles.dart';
import 'package:no_fine/ui/widgets/app_bar.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:no_fine/ui/widgets/pa%D1%81kage_widget.dart';
import 'package:no_fine/ui/widgets/submit_button.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MyPakagesScreen extends StatefulWidget {
  static const String route = "/mypakages";

  @override
  _MyPakagesScreenState createState() => _MyPakagesScreenState();
}

class _MyPakagesScreenState extends State<MyPakagesScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  PackagesBloc _bloc;
  int _packID;

  @override
  void initState() {
    _bloc = BlocProvider.of<PackagesBloc>(context);
    _bloc.add(UpdatePackagesEvent());
    getPackageId();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: CustomAppBar(
        titleText: 'Пакеты',
        context: context,
      ),
      body: BlocBuilder<PackagesBloc, PackagesState>(builder: (context, state) {
        if (state is PackagesLoadedState) {
          return packagesList(state.packagesResponse);
        }
        return Center(
            child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(AppColors.primary),
        ));
      }

      ),
    );
  }

  packagesList(List<PackagesResponse> packagesList) => ListView.builder(
      itemCount: packagesList.length,
      itemBuilder: (BuildContext context, int index) {
        return Padding(
          padding: const EdgeInsets.all(16.0),
          child:   PackageWidget(
            title: packagesList[index].name,
            id: _packID == packagesList[index].id,
            price: packagesList[index].price,
            description: packagesList[index].description,
            index: index,
            tap: (){

             setState(() {

               Navigator.of(context).pushNamed(
                 PaymantScreen.route,
               );

             //  _packID = packagesList[index].id;
              // setPackageId(_packID);
             });

            },
          ),
        );
      });

  getPackageId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print('10101001010101010');
     print(prefs.getInt('packageId'));
    _packID = prefs.getInt('packageId');
    //return  prefs.getInt('packageId');
  }
  setPackageId(int id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print('10101001010101010');
    print(prefs.getInt('packageId'));
    prefs.setInt('packageId', id);
    //return  prefs.getInt('packageId');
  }


}
