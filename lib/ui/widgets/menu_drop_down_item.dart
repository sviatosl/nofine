import 'package:flutter/material.dart';
import 'package:no_fine/ui/resoursese/app_colors.dart';
import 'package:no_fine/ui/resoursese/app_styles.dart';

class DropDownMenuItemWidget extends StatelessWidget {

  final int index;
 // final int isSelected;
  final String title;
  final String icon;
  final VoidCallback onTap;

  DropDownMenuItemWidget({
    this.index,
  //  this.isSelected,
    this.title,
    this.icon,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        color: AppColors.primary.withOpacity(0.06),
        height: 40,
        child: Padding(
          padding: const EdgeInsets.only(left:20.0),
          child: Row(
            // mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                // height: 20,
                width: 40,
                child: Image.asset(
                  icon??'',
                //  color: isSelected ==index? AppColors.primary : AppColors.gray,
                ),
              ),
              const SizedBox(width: 4,),

              Container(
                child: Text(
                  title,
                  style: AppStyles.Roboto500(
                      color:  AppColors.gray
                    ),
                ),
              ),
              // const SizedBox(width: 60,),
              /* Visibility(
                visible: isSelected==4,
                child: Icon(
                  isSelected ==index? Icons.arrow_drop_down_circle_outlined : Icons.arrow_forward_ios_outlined ,
                  color: isSelected ==index? AppColors.primary : AppColors.gray,
                  size: 30.0,
                ),
              ),*/
            ],
          ),
        ),
      ),
    );
  }
}
