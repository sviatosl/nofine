import 'package:flutter/material.dart';
import 'package:no_fine/ui/resoursese/app_colors.dart';

class SocialButton extends StatelessWidget {
  final VoidCallback onPressed;
  final String text;
  final Color textColor;
  final String image;

  const SocialButton({
    Key key,
    this.onPressed,
    @required this.text,
    this.textColor,
    this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 48,
      child: Container(
        decoration: BoxDecoration(
            borderRadius: const BorderRadius.all(
              Radius.circular(6.0),
            ),
            boxShadow: [
              BoxShadow(
                color: Color(0xff026853).withOpacity(0.15),
                spreadRadius: 2,
                blurRadius: 3,
                offset: Offset(0, 4),
              )
            ]),
        child: FlatButton(
          onPressed: (onPressed),
          color: AppColors.white,
          // disabledColor: AppColors.primary3Color,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(6.0),
          ),
          child: Center(
            child: Row(
              children: [
                Container(
                  height: 22,
                  width: 22,
                  alignment: Alignment.center,
                  child: Image.asset(
                        image??'',
                        // color: Colors.red,
                      ) ,
                ),
                Spacer(),
                Text(
                  text,
                  style: TextStyle(
                      color: textColor,
                      fontFamily: 'RobotoMedium',
                      fontSize: 14,
                      fontWeight: FontWeight.w500),
                ),
                Spacer(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
