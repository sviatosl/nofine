import 'package:flutter/material.dart';
import 'package:no_fine/ui/resoursese/app_colors.dart';

class SubmitButton extends StatelessWidget {
  final VoidCallback onPressed;
  final String text;
  final Color buttonColor;
  final Color textColor;
  final Color borderColor;

  const SubmitButton({
    Key key,
    this.onPressed,
    @required this.text,
    this.buttonColor,
    this.textColor,
    this.borderColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 48,
      child: FlatButton(
        onPressed: (onPressed),
        color: buttonColor,
        // disabledColor: AppColors.primary3Color,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
            side: BorderSide(
                color: borderColor ?? AppColors.backgroundTransparent,
                width: 1.6)),
        child: Center(
          child: Text(
            text,
            textAlign: TextAlign.center,
            style: TextStyle(
                color: textColor,
                fontFamily: 'RobotoMedium',
                fontSize: 14,
                fontWeight: FontWeight.w500),
          ),
        ),
      ),
    );
  }
}
