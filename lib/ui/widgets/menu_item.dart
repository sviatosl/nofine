import 'package:flutter/material.dart';
import 'package:no_fine/ui/resoursese/app_colors.dart';
import 'package:no_fine/ui/resoursese/app_styles.dart';

class SelectableMenuItemWidget extends StatelessWidget {

  final int index;
  final int isSelected;
  final String title;
  final String icon;
  final VoidCallback onTap;

  SelectableMenuItemWidget({
    this.index,
    this.isSelected,
    this.title,
    this.icon,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return Container(

      child: InkWell(
        onTap: onTap,
        child: Container(

          height: 46,
          child: Row(
           // mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const SizedBox(width: 6,),
              Container(
               // height: 20,
                width: 40,
                child: Image.asset(
                  icon,
                  color: isSelected ==index? AppColors.primary : AppColors.gray,
                    scale: 1.6
                ),
              ),
              const SizedBox(width: 18,),

              Container(
                child: Text(
                  title,
                  style: AppStyles.Roboto500(
                    fontSize: 15,
                      color: isSelected ==index? AppColors.primary : AppColors.gray),
                ),
              ),
             // const SizedBox(width: 60,),
             /* Visibility(
                visible: isSelected==4,
                child: Icon(
                  isSelected ==index? Icons.arrow_drop_down_circle_outlined : Icons.arrow_forward_ios_outlined ,
                  color: isSelected ==index? AppColors.primary : AppColors.gray,
                  size: 30.0,
                ),
              ),*/
            ],
          ),
        ),
      ),
    );
  }
}
