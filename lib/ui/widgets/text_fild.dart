import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:no_fine/ui/resoursese/app_colors.dart';
import 'package:no_fine/ui/resoursese/app_styles.dart';

class MyTextField extends StatelessWidget {
  final VoidCallback onButtonPressed;
  final String buttonText;
  final String hintText;
  final String labelText;
  final bool obscureText;
  final Color lightBackground;
  final bool border;
  final bool keyboardTypeIsInt;
  final FormFieldValidator<String> fieldValidator;
  final String initVal;
  final TextEditingController controller;
  final FormFieldSetter<String> onSaved;
final int limitNum;
  const MyTextField({
    Key key,
    this.onButtonPressed,
    this.buttonText = "",
    this.hintText,
    this.labelText,
    this.obscureText = false,
    this.lightBackground,
    this.border = true,
    // this.height = 40,
    this.controller,
    this.keyboardTypeIsInt,
    this.fieldValidator,
    this.onSaved,
    this.initVal,
    this.limitNum,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      //constraints: BoxConstraints(maxHeight: height),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6),
        //  color: AppColors.primary.withOpacity(0.05)
      ),
      child: Row(
        children: [
          Expanded(
            child: ClipRRect(
              borderRadius: BorderRadius.circular(6),
              child: Container(

                child: TextFormField(
                  //maxLength: limitNum,
                //  maxLengthEnforcement: false,
                 // inputFormatters: [LengthLimitingTextInputFormatter(limitNum)],
                  //buildCounter: (BuildContext context, { int currentLength, int maxLength, bool isFocused }) => null,
                  //inputFormatters: [LengthLimitingTextInputFormatter(limitNum)],
                  initialValue: initVal,
                  validator: fieldValidator,
                  keyboardType: keyboardTypeIsInt
                      ? TextInputType.number
                      : TextInputType.text,
                  cursorColor: AppColors.primary,
                  controller: controller,
                  obscureText: obscureText,
                  maxLines: 1,
                  style: AppStyles.Roboto500(color: AppColors.black),
                  decoration: InputDecoration(
                     fillColor: AppColors.primary.withOpacity(0.05),
                    //fillColor: AppColors.primary.withOpacity(0.05),
                    filled: true,
                    errorStyle: AppStyles.Roboto400(
                        color: Colors.redAccent, fontSize: 10),
                    alignLabelWithHint: false,
                    /*contentPadding:
                          EdgeInsets.symmetric(horizontal: 12, vertical: 7),*/
                    hintText: hintText,
                    hintStyle: AppStyles.Roboto400(color: AppColors.black),
                    labelStyle: AppStyles.Roboto400(color: AppColors.black),
                    border: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    focusedErrorBorder: InputBorder.none,
                    labelText: labelText,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
