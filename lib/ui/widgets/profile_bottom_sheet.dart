import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http_parser/http_parser.dart';
import 'package:image_picker/image_picker.dart';
import 'package:no_fine/bloc/auth/main_screen_bloc/main_screen_bloc.dart';
import 'package:no_fine/ui/resoursese/app_colors.dart';
import 'package:no_fine/ui/widgets/social_button.dart';
import 'package:no_fine/ui/widgets/submit_button.dart';
import 'package:no_fine/bloc/auth/main_screen_bloc/main_screen_event.dart';

class ProfileBottomSheet extends StatefulWidget {
  const ProfileBottomSheet({
    Key key,
  }) : super(key: key);

  @override
  _ProfileBottomSheetState createState() => _ProfileBottomSheetState();
}

class _ProfileBottomSheetState extends State<ProfileBottomSheet> {
  static File imageFile;
  final picker = ImagePicker();
  MainScreenBloc _bloc;
  static const BASE_URL = "https://api-fines.profdecor.com.ua";
  static const String apiEndpoint = BASE_URL + "/api/v1/";

  Future getImageFromGallery() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        imageFile = File(pickedFile.path);
        // _bloc.add(UploadProfileImageEvent(imageFile));

        _upload(imageFile);

      } else {
        print('No image selected.');
      }
    });
  }

  Future getImageFromCamera() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        imageFile = File(pickedFile.path);
       // _bloc.add(UploadProfileImageEvent(imageFile));
        _upload(imageFile);
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  void initState() {
    _bloc = BlocProvider.of<MainScreenBloc>(context);
    super.initState();
  }

  void _upload(File file) async {
    String fileName = file.path.split('/').last;

    FormData data = FormData.fromMap({
      "image": await MultipartFile.fromFile(file.path,
          filename: fileName, ),

    });

    Dio dio = new Dio();

    await dio.post(apiEndpoint + "users/avatar",
        options: Options(headers: {
          "Authorization":
              'Gs4NppG5v7nmNZcrGbgJ5oTR4GaRpk3O7685bNRYnc32xtG3Nmbtlodp0qw0'
        }),
        data:data);
  }

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20), topRight: Radius.circular(20)),
      child: Container(
          color: AppColors.white,
          height: 400,
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Column(children: <Widget>[
            Container(
              height: 4,
              width: 100,
              margin: EdgeInsets.only(bottom: 19.5, top: 5),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(99),
                  color: AppColors.primary),
            ),
            const SizedBox(
              height: 16.0,
            ),
            SubmitButton(
              onPressed: () {
                getImageFromGallery();
                Navigator.pop(context);
                // _bloc.add(GetCurrentUserEvent());
              },
              text: 'ГАЛЕРЕЯ',
              textColor: AppColors.white,
              buttonColor: AppColors.primary,
              borderColor: AppColors.primary,
            ),
            const SizedBox(
              height: 16.0,
            ),
            SocialButton(
              onPressed: getImageFromCamera,
              text: 'СДЕЛАТЬ ФОТО',
              textColor: AppColors.primary,
            ),
            const SizedBox(
              height: 46.0,
            ),
            SocialButton(
              onPressed: () {
                Navigator.pop(context);
              },
              text: 'ОТМЕНИТЬ',
              textColor: AppColors.primary,
            ),
            const SizedBox(
              height: 30.0,
            ),
          ])),
    );
  }
}
