import 'package:flutter/material.dart';
import 'package:no_fine/ui/resoursese/app_colors.dart';

class ContainerBorder extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return GestureDetector(

      child: Container(
        height: 220.0,
        decoration: BoxDecoration(
          border:  Border.all(color: AppColors.primary)

        ),
      ),
    );
  }
}
