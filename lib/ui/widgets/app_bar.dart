import 'package:flutter/material.dart';
import 'package:no_fine/ui/resoursese/app_colors.dart';
import 'package:no_fine/ui/resoursese/app_styles.dart';

class CustomAppBar extends AppBar {
  CustomAppBar({
    String titleText,
    bool isExistClouseIcon = true,
    BuildContext context,
  }) : super(
          backgroundColor: AppColors.white,
          leading: IconButton(
            icon: Icon(
              isExistClouseIcon==true?  Icons.close:null,
              color: AppColors.primary,
              size: 26.0,
            ),
            onPressed: () => Navigator.of(context).pop(),
          ),
          elevation: 0,
          title: Text(
            titleText,
            style: AppStyles.Roboto500(
                fontSize: 18,
                height: 1.27,
                color: AppColors.primary,
                fontWeight: FontWeight.w500,
                fontFamily: 'RobotoMedium'),
          ),
        );
}
