import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:no_fine/bloc/auth/sign_in_bloc/sign_in_bloc.dart';
import 'package:no_fine/bloc/auth/sign_in_bloc/sign_in_events.dart';
import 'package:no_fine/data/api_provider.dart';
import 'package:no_fine/data/repository/auth_repo.dart';
import 'package:no_fine/ui/resoursese/app_colors.dart';
import 'package:no_fine/ui/resoursese/app_styles.dart';
import 'package:no_fine/ui/sign_in/sign_in.dart';
import 'package:no_fine/ui/widgets/privacy_policy.dart';
import 'package:no_fine/ui/widgets/social_button.dart';
import 'package:no_fine/ui/widgets/submit_button.dart';

class CustomBottomSheet extends StatefulWidget {
  const CustomBottomSheet({
    Key key,
  }) : super(key: key);

  @override
  _CustomBottomSheetState createState() => _CustomBottomSheetState();
}

class _CustomBottomSheetState extends State<CustomBottomSheet> {
  SignInBloc _bloc;
  @override
  void initState() {
    _bloc = BlocProvider.of<SignInBloc>(context);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create:  (_) => SignInBloc(AuthRepositoryImpl(ApiProvider(Dio()))),
      child: ClipRRect(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20), topRight: Radius.circular(20)),
        child: Container(
            color: AppColors.white,
            height: 400,
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Column(children: <Widget>[
              Container(
                height: 4,
                width: 100,
                margin: EdgeInsets.only(bottom: 19.5, top: 5),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(99),
                    color: AppColors.primary),
              ),
              const SizedBox(
                height: 16.0,
              ),
              SocialButton(
                onPressed: () {

                  _onLogInFaceBookClick();

                },
                text: 'Войти с помощью Facebook',
                textColor: AppColors.black,
                image: 'assets/icons/facebook.png',
              ),
              const SizedBox(
                height: 16.0,
              ),
              SocialButton(
                onPressed: () {
                  _onLogInGoogleClick();


                },
                text: 'Войти с помощью Google',
                textColor: AppColors.black,
                image: 'assets/icons/google.png',
              ),
              const SizedBox(
                height: 40.0,
              ),
              SubmitButton(
                onPressed: () {
                  Navigator.of(context).pushNamed(
                    SignInScreen.route,
                  );
                },
                text: 'Войти с помощью телефона',
                textColor: AppColors.primary,
                buttonColor: AppColors.backgroundTransparent,
                borderColor: AppColors.primary,
              ),
              Expanded(
                child: const SizedBox(
                  height: 12.0,
                ),
              ),
              /*Center(
                  child: Text(
                'Уже есть аккаунт?',
                style: AppStyles.Roboto400(color: AppColors.black),
              )),*/
              Expanded(
                child: const SizedBox(
                  height: 12.0,
                ),
              ),
              Center(child: PrivacyPolicy()),
              const SizedBox(
                height: 30.0,
              ),
            ])),
      ),
    );
  }


  void _onLogInGoogleClick() {
    _bloc.add(SignInLoginGoogleEvent());
  }
  void _onLogInFaceBookClick() {
    _bloc.add(SignInLoginFacebookEvent());
  }

}
