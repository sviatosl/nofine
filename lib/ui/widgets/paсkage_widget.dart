import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:no_fine/ui/resoursese/app_colors.dart';
import 'package:no_fine/ui/resoursese/app_styles.dart';
import 'package:no_fine/ui/widgets/submit_button.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PackageWidget extends StatelessWidget {
  final bool id;
  final int index;
  final String title;
  final String description;

  final String price;
  final int limit;
  final VoidCallback tap;

  const PackageWidget(
      {Key key,
      this.id,
      this.title,
      this.description,
      this.price,
      this.limit,
      this.tap,
      this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      //  height: 188.0,
      decoration: BoxDecoration(
          border: Border.all(color: id ? AppColors.gray : AppColors.primary),
          borderRadius: BorderRadius.all(Radius.circular(6.0))),
      child: Column(
        children: [
          Container(
            height: 156,
            child: Column(
              children: [
                const SizedBox(
                  height: 12.0,
                ),
                Container(
                    // color: Colors.green,
                    height: 26,
                    child: Text(
                      title,
                      style: AppStyles.Roboto700(
                          color: id ? AppColors.gray : AppColors.primary),
                    )),
                Container(
                  height: 106,
                  // color: Colors.grey,
                  child: Padding(
                    padding: const EdgeInsets.only(
                      left: 26.0,
                      right: 30.0,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                            child: Text(
                          description.split('<br>')[0].toString(),
                          style: AppStyles.Roboto400(
                              color: id ? AppColors.gray : AppColors.primary),
                        )),
                        Container(
                            child: Text(
                          getDescription(description, 1),
                          style: AppStyles.Roboto400(
                              color: id ? AppColors.gray : AppColors.primary),
                        )),
                        Container(
                            height: 36,
                            child: Expanded(
                              child: Text(getDescription(description, 2),
                                  style: AppStyles.Roboto400(
                                      color: id
                                          ? AppColors.gray
                                          : AppColors.primary)),
                            )),
                      ],
                    ),
                  ),
                ),
                Container(
                  color: Colors.redAccent,
                  child: const SizedBox(
                    height: 12.0,
                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: id ? AppColors.gray : AppColors.primary,
            height: 1,
          ),
          Container(
            height: 60,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24.0),
              child: Center(
                child: Container(
                  height: 40,
                  child: Row(
                    //crossAxisAlignment: CrossAxisAlignment.center,
                    // mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                          child: Center(
                              child: Text(
                        index == 0 ? 'Бесплатно' : price.toString(),
                        style: AppStyles.Roboto400(
                            fontSize: 20,
                            color: id ? AppColors.gray : AppColors.primary),
                      ))),
                      Expanded(
                        child: SubmitButton(
                          text: id ? 'Текущий' : 'Получить',
                          textColor: id ? AppColors.gray : AppColors.primary,
                          borderColor: id ? AppColors.gray : AppColors.primary,
                          onPressed: () {
                            id ? null : tap();
                          },
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  getPackageId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print('10101001010101010');
    print(prefs.getInt('packageId'));
    return prefs.getInt('packageId');
  }

  String getDescription(String text, int index) {
    List<String> arrDescription = text.split('<br>');
    return arrDescription[index].toString();
  }
}
