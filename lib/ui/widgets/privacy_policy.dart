import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:no_fine/ui/pages/agreement_creen.dart';
import 'package:no_fine/ui/pages/oferta_screen.dart';
import 'package:no_fine/ui/pages/policy_screen.dart';
import 'package:no_fine/ui/resoursese/app_colors.dart';
import 'package:no_fine/ui/resoursese/app_styles.dart';

class PrivacyPolicy extends StatelessWidget {
  const PrivacyPolicy({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 300.0,
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          style: AppStyles.Roboto400(color: AppColors.black),
          children: <TextSpan>[
            TextSpan(text: 'Регистрируясь, Вы соглашаетесь с '),
            TextSpan(
                text: 'Политикой конфиденциальности',
                style: TextStyle(
                  fontFamily: 'RobotoMedium',
                  fontWeight: FontWeight.w400,
                  fontSize: 12,
                  color: AppColors.primary,
                  decoration: TextDecoration.underline,
                ),
                recognizer: TapGestureRecognizer()
                  ..onTap = () {
                    Navigator.of(context).pushNamed(
                      PolicyScreen.route,
                    );

                  }),
            TextSpan(text: ' и '),
            TextSpan(
                text: 'Договор оферты',
                style: TextStyle(
                  fontFamily: 'RobotoMedium',
                  fontWeight: FontWeight.w400,
                  fontSize: 12,
                  color: AppColors.primary,
                  decoration: TextDecoration.underline,
                ),
                recognizer: TapGestureRecognizer()
                  ..onTap = () {
                    Navigator.of(context).pushNamed(
                      OfertaScreen.route,
                    );

                  }),
            TextSpan(text: ' и '),
            TextSpan(
                text: 'Соглашение',
                style: TextStyle(
                  fontFamily: 'RobotoMedium',
                  fontWeight: FontWeight.w400,
                  fontSize: 12,
                  color: AppColors.primary,
                  decoration: TextDecoration.underline,
                ),
                recognizer: TapGestureRecognizer()
                  ..onTap = () {
                    Navigator.of(context).pushNamed(
                      AgreementScreen.route,
                    );

                  }),
          ],
        ),
      ),
    );

  }

}
