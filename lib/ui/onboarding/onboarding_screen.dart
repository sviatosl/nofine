import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:no_fine/bloc/auth/sign_in_bloc/sign_in_bloc.dart';
import 'package:no_fine/bloc/auth/sign_in_bloc/sign_in_events.dart';
import 'package:no_fine/bloc/auth/sign_in_bloc/sign_in_state.dart';
import 'package:no_fine/ui/base_screen.dart';
import 'package:no_fine/ui/main_screen/main_screen.dart';
import 'package:no_fine/ui/resoursese/app_colors.dart';
import 'package:no_fine/ui/resoursese/app_strinds.dart';
import 'package:no_fine/ui/resoursese/app_styles.dart';
import 'package:no_fine/ui/sign_in/sign_in.dart';
import 'package:no_fine/ui/sign_up/sign_up.dart';
import 'package:no_fine/ui/widgets/bottom_sheet.dart';
import 'package:no_fine/ui/widgets/privacy_policy.dart';
import 'package:no_fine/ui/widgets/social_button.dart';
import 'package:no_fine/ui/widgets/submit_button.dart';

class OnboardingScreen extends StatefulWidget {
  static const String route = "/";

  @override
  _OnboardingScreenState createState() => _OnboardingScreenState();
}

class _OnboardingScreenState extends State<OnboardingScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
 // int index = 2;
  int indexColorButton = 1;

  bool _isHidden = true;
  SignInBloc _bloc;

  void _toggleVisibility() {
    setState(() {
      _isHidden = !_isHidden;
    });
  }

  @override
  void initState() {
    _bloc = BlocProvider.of<SignInBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/images/auth_background.png'),
                fit: BoxFit.fitHeight),
          ),
        ),
        Container(color: AppColors.primary.withOpacity(0.8)),
        Scaffold(
          key: _scaffoldKey,
          backgroundColor: AppColors.backgroundTransparent,
          body: Column(
            children: [
              BlocListener<SignInBloc, SignInState>(

                  listener: (context, state) {
                    if (state is SuccessSignInState) {
                      WidgetsBinding.instance.addPostFrameCallback(
                              (_) => Navigator.of(context).pushReplacementNamed(
                                BaseScreen.route,
                          ));
                    }
                  },
                  child:Container(),
              ),
              Expanded(child: const SizedBox(height: 140.0)),
              Center(
                child: Container(
                  child: Text(
                    AppStrings.onboardingTitle,
                    style: AppStyles.Montserrat700(),
                  ),
                ),
              ),
              Text(
                AppStrings.onboardingSubitle,
                style: AppStyles.Montserrat500(),
              ),
              Expanded(
                child: const SizedBox(
                  height: 140,
                ),
              ),
              /*   Container(
                width: 180.0,
                height: 40.0,
                child: Text(
                  AppStrings.onboardingText,
                  style: AppStyles.Roboto400(
                      height: 1.66, fontWeight: FontWeight.w400),
                  textAlign: TextAlign.center,
                ),
              ),*/
              const SizedBox(
                height: 40,
              ),
           /*   Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    height: 15,
                    width: 15,
                    decoration: BoxDecoration(
                      // borderRadius: BorderRadius.circular(20),
                      border: Border.all(color: AppColors.white),
                      color: index == 1
                          ? AppColors.white
                          : AppColors.backgroundTransparent,
                      shape: BoxShape.circle,
                    ),
                  ),
                  const SizedBox(
                    width: 4.0,
                  ),
                  Container(
                    height: 15,
                    width: 15,
                    decoration: BoxDecoration(
                      // borderRadius: BorderRadius.circular(20),
                      border: Border.all(color: AppColors.white),
                      color: index == 2
                          ? AppColors.white
                          : AppColors.backgroundTransparent,
                      shape: BoxShape.circle,
                    ),
                  ),
                ],
              ),*/
              Expanded(
                child: const SizedBox(
                  height: 40,
                ),
              ),
              /*Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Уже с нами? ',
                    style: AppStyles.Roboto400(),
                  ),
                  CupertinoButton(
                      minSize: 20,
                      padding: EdgeInsets.zero,
                      child: Text(
                        'Войти',
                        style: TextStyle(
                          fontFamily: 'RobotoMedium',
                          fontWeight: FontWeight.w400,
                          fontSize: 12,
                          color: AppColors.white,
                          decoration: TextDecoration.underline,
                        ),
                      ),
                      onPressed: () {
                        */ /* _scaffoldKey.currentState.showBottomSheet((context) =>
                            Container(
                                color: AppColors.backgroundTransparent,
                                child: CustomBottomSheet()),
                          backgroundColor: AppColors.backgroundTransparent

                        );*/ /*
                      })
                ],
              ),*/
              /*  const SizedBox(
                height: 40,
              ),*/
            /*  Visibility(
                visible: index == 1,
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: SubmitButton(
                    onPressed: () {
                      setState(() {
                        index = 2;
                      });
                    },
                    text: 'Пропустить',
                    textColor: AppColors.primary,
                    buttonColor: AppColors.white,
                    //   borderColor: Colors.red,
                  ),
                ),
              ),*/
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Expanded(
                      child: SubmitButton(
                        onPressed: () {
                          setState(() {
                            indexColorButton = 1;
                          });
                          /*Navigator.of(context).pushNamed(
                            SignInScreen.route,
                          );*/

                          _scaffoldKey.currentState.showBottomSheet(
                              (context) => Container(
                                    color: AppColors.backgroundTransparent,
                                    child:
                                        /*CustomBottomSheet()
                              */

                                        ClipRRect(
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(20),
                                          topRight: Radius.circular(20)),
                                      child: Container(
                                          color: AppColors.white,
                                          height: 400,
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 20),
                                          child: Column(children: <Widget>[
                                            Container(
                                              height: 4,
                                              width: 100,
                                              margin: EdgeInsets.only(
                                                  bottom: 19.5, top: 5),
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          99),
                                                  color: AppColors.primary),
                                            ),
                                            const SizedBox(
                                              height: 16.0,
                                            ),
                                            SocialButton(
                                              onPressed: () {
                                                _onLogInFaceBookClick();
                                              },
                                              text:
                                                  'Войти с помощью Facebook',
                                              textColor: AppColors.black,
                                              image:
                                                  'assets/icons/facebook.png',
                                            ),
                                            const SizedBox(
                                              height: 16.0,
                                            ),
                                            SocialButton(
                                              onPressed: () {
                                                _onLogInGoogleClick();
                                              },
                                              text: 'Войти с помощью Google',
                                              textColor: AppColors.black,
                                              image:
                                                  'assets/icons/google.png',
                                            ),
                                            const SizedBox(
                                              height: 40.0,
                                            ),
                                            SubmitButton(
                                              onPressed: () {
                                                Navigator.of(context)
                                                    .pushNamed(
                                                  SignInScreen.route,
                                                );
                                              },
                                              text:
                                                  'Войти с помощью телефона',
                                              textColor: AppColors.primary,
                                              buttonColor: AppColors
                                                  .backgroundTransparent,
                                              borderColor: AppColors.primary,
                                            ),

                                           /* Center(
                                                child: Text(
                                              'Уже есть аккаунт?',
                                              style: AppStyles.Roboto400(
                                                  color: AppColors.black),
                                            )),*/
                                            Expanded(
                                              child: const SizedBox(
                                                height: 12.0,
                                              ),
                                            ),
                                            Center(child: PrivacyPolicy()),
                                            Expanded(
                                              child: const SizedBox(
                                                height: 30.0,
                                              ),
                                            ),
                                          ])),
                                    ),
                                  ),
                              backgroundColor:
                                  AppColors.backgroundTransparent);
                        },
                        text: 'Войти',
                        textColor: indexColorButton == 1
                            ? AppColors.primary
                            : AppColors.white,
                        buttonColor: indexColorButton == 1
                            ? AppColors.white
                            : AppColors.backgroundTransparent,
                        borderColor: AppColors.white,
                      ),
                    ),
                    const SizedBox(
                      width: 4,
                    ),
                    Expanded(
                      child: SubmitButton(
                        onPressed: () {
                          setState(() {
                            indexColorButton = 2;
                          });
                          Navigator.of(context).pushNamed(
                            SignUpScreen.route,
                          );
                        },
                        text: 'Регистрация',
                        textColor: indexColorButton == 2
                            ? AppColors.primary
                            : AppColors.white,
                        buttonColor: indexColorButton == 2
                            ? AppColors.white
                            : AppColors.backgroundTransparent,
                        borderColor: AppColors.white,
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 30.0,
              )
            ],
          ),
        ),
     /*   Padding(
          padding: const EdgeInsets.only(bottom: 120.0),
          child: GestureDetector(onPanUpdate: (details) {
            if (details.delta.dx < 0) {
              setState(() {
                index = 2;
              });
            }
            if (details.delta.dx > 0) {
              setState(() {
                index = 1;
              });
            }
          }),
        )*/
      ],
    );
  }

  void _onLogInGoogleClick() {
    _bloc.add(SignInLoginGoogleEvent());
  }

  void _onLogInFaceBookClick() {
    _bloc.add(SignInLoginFacebookEvent());
  }
}
