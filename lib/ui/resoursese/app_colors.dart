
import 'package:flutter/material.dart';

class AppColors {


  /// All COLORS
  /// static const Color background = Color(0xff100026);
  static const Color backgroundTransparent = Colors.transparent;
  static const Color primary = Color(0xFF026853);

  static const Color primaryDarker = Color(0xFF7542BB);
  static const Color light = Color(0xFFF6F6F6);

  static const Color white = Color(0xFFFfffff);
  static const Color black = Color(0xFF666666);
  static const Color blackk = Color(0xFF000000);

  static const Color gray = Color(0xFF797979);
  static const Color blak_prof = Color(0xFF061D28);

  static const Color red = Colors.redAccent;


  //static const Color rt = Color(0xffF6F6F6);


}