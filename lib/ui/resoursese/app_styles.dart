import 'package:flutter/material.dart';
import 'package:no_fine/ui/resoursese/app_colors.dart';

class AppStyles {
  static TextStyle Montserrat700({double fontSize = 18.0,
    Color color = AppColors.white,
    double height,
    FontWeight fontWeight = FontWeight.w700,
    fontFamily: 'MontserratMedium'}) {
    return TextStyle(
        color: color,
        fontSize: fontSize,
        height: height,
        fontWeight: fontWeight,
        fontFamily: fontFamily);
  }

  static TextStyle Montserrat500({double fontSize = 14.0,
    Color color = AppColors.white,
    double height,
    FontWeight fontWeight = FontWeight.w500,
    fontFamily: 'MontserratMedium'}) {
    return TextStyle(
        color: color,
        fontSize: fontSize,
        height: height,
        fontWeight: fontWeight,
        fontFamily: fontFamily);
  }

  static  TextStyle Roboto400({double fontSize = 12.0,
    Color color = AppColors.white,
    double height,
    FontWeight fontWeight = FontWeight.w400,
    fontFamily: 'RobotoMedium'}) {
    return TextStyle(
        color: color,
        fontSize: fontSize,
        height: height,
        fontWeight: fontWeight,
        fontFamily: fontFamily);
  }

  static TextStyle Roboto500({double fontSize = 14.0,
    Color color = AppColors.white,
    double height,
    FontWeight fontWeight = FontWeight.w500,
    fontFamily: 'RobotoMedium'}) {
    return TextStyle(
        color: color,
        fontSize: fontSize,
        height: height,
        fontWeight: fontWeight,
        fontFamily: fontFamily);
  }


  static TextStyle Roboto700({double fontSize = 14.0,
    Color color = AppColors.primary,
    double height,
    FontWeight fontWeight = FontWeight.w700,
    fontFamily: 'RobotoMedium'}) {
    return TextStyle(
        color: color,
        fontSize: fontSize,
        height: height,
        fontWeight: fontWeight,
        fontFamily: fontFamily);
  }
}